﻿Imports System
Imports System.Security.Cryptography
Imports System.Text

Namespace MD5Hash
    Class MD5Hashing
        'Private Shared Sub Main(ByVal args As String())
        '    Dim source As String = "Hello World!"

        '    Using md5Hash As MD5 = MD5.Create()
        '        Dim hash As String = GetMd5Hash(md5Hash, source)
        '        Console.WriteLine("The MD5 hash of " & source & " is: " & hash & ".")
        '        Console.WriteLine("Verifying the hash...")

        '        If VerifyMd5Hash(md5Hash, source, hash) Then
        '            Console.WriteLine("The hashes are the same.")
        '        Else
        '            Console.WriteLine("The hashes are not same.")
        '        End If
        '    End Using
        'End Sub

        Public Function GetMd5Hash(ByVal md5Hash As MD5, ByVal input As String) As String
            Dim data As Byte() = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input))
            Dim sBuilder As StringBuilder = New StringBuilder()

            For i As Integer = 0 To data.Length - 1
                sBuilder.Append(data(i).ToString("x2"))
            Next

            Return sBuilder.ToString()
        End Function

        Public Function VerifyMd5Hash(ByVal md5Hash As MD5, ByVal input As String, ByVal hash As String) As Boolean
            Dim hashOfInput As String = GetMd5Hash(md5Hash, input)
            Dim comparer As StringComparer = StringComparer.OrdinalIgnoreCase

            If 0 = comparer.Compare(hashOfInput, hash) Then
                Return True
            Else
                Return False
            End If
        End Function
    End Class
End Namespace

