﻿Public Class UserLogonModel
    Property UserID As Integer
    Property UserCode As String
    Property MD5Password As String
    Property PlainTextPassword As String
    Property FirstName As String
    Property LastName As String
    Property FullName As String
    Property Title As String
    Property FileName As String
    Property ByteImage As Byte()
    Property Email As String

    Property Tel As String

    Property RoleID As Integer
    Property RoleName As String
    Property RoleCode As String
    Property BranchReponsibility As List(Of BranchModel)
    Property UserRole As List(Of UserRoleModel)

    Property IsExpired As Boolean
End Class

Public Class BranchModel
    Property StoreID As Integer
    Property BranchID As Integer
    Property BranchName As String
    Property Address As String
    Property Region As String
    Property Province As String
    Property SCGLSoldTo As String
    Property SCGLLocation As String
    Property CustCode As String

    Property Region7SM As String
    Property ShipTo As String
End Class

Public Class UserRoleModel
    Property RoleID As Integer
    Property MenuID As String
    Property MenuName As String
    Property IsVisibled As Boolean
End Class

Public Class StoreModel
    Property StoreID As Integer
    Property StoreName As String
End Class

Public Class CompetitorModel
    Property ROWID As Int64
    Property StoreID As Integer
    Property StoreName As String
    Property Province As String
    Property ProductNo As String
    Property ProductName As String
    Property UnitCode As String
    Property Barcode As String
    Property CategoryID As Integer
    Property CategoryName As String
    Property BrandID As Integer
    Property BrandName As String
    Property Model As String
    Property NormalPrice As Decimal
    Property ToleranceMin As Decimal
    Property ToleranceMax As Decimal
    Property Remark As String

    Property PromotionPrice As Decimal
    Property PromotionDate As String

    Property LastNormalPrice As Decimal

    Property AlreadyInCart As Boolean

    Property AlreadyInCartNormal As Boolean

    Property ActivePrice As Decimal
    Property ActivePriceDescription As String

    Property CalculationBarcode As String
End Class

Public Class UpdatePricingModel
    Property ROWID As Int64
    Property BranchID As Integer
    Property TransactDate As String

    Property CompetitorID As Int64
    Property ProductNo As String
    Property ProductName As String
    Property UnitCode As String
    Property Barcode As String

    Property NormalPrice As Decimal
    Property LastNormalPrice As Decimal
    Property NewPrice As Decimal
    Property NewDate As String
    Property PromotionPrice As Decimal
    Property PromotionDate As String

    Property Remark As String
    Property IsApproved As Boolean
    Property ApprovedDate As String
    Property ApprovedBy As String
    Property IsRejected As Boolean
    Property RejectedDate As String
    Property RejectedBy As String

    Property ToleranceReason As String

    Property IsActive As Boolean
    Property CreateBy As String
End Class

Public Class UpdatePricingNotificationSummaryModel
    Property BranchID As Integer
    Property CreateDate As String
    Property TotalNotify As Integer
End Class

Public Class UpdatePricingNotificationModel
    Property ROWID As Int64
    Property BranchID As Integer

    Property CompetitorID As Int64
    Property ProductNo As String
    Property ProductName As String
    Property UnitCode As String
    Property Barcode As String

    Property NormalPrice As Decimal
    Property RetailPrice As Decimal
    Property PromotionPrice As Decimal
    Property ExpireDate As String
    Property NewPromotionPrice As Decimal

    Property Action As String
    Property ActionCode As String
    Property Subsidized As Decimal

    Property EffectiveDate As String
    Property NewExpireDate As String

    Property IsAddToCart As Boolean
    Property AddToCartDate As String
    Property AddToCartBy As String
    Property AddToCartRowId As Int64

    Property CreateBy As String
    Property CreateDate As String
    Property LastUpdateBy As String
    Property LastUpdateDate As String
End Class

Public Class SaveUpdatePricingModel
    Property DeviceID As String
    Property UserID As Int64
    Property BranchID As Integer
    Property TransactDate As String
    Property CompetitorID As Int64
    Property NormalPrice As Decimal
    Property LastNormalPrice As Decimal
    Property NewPrice As Decimal
    Property NewDate As String
    Property PromotionPrice As Decimal
    Property PromotionDate As String
    Property Reason As String

    Property ReferNotificationRowId As Int64 '-- update from mobile for add to cart from notification function
    Property NotificationActionCode As String '-- notification
End Class

Public Class ApproveListUserModel
    Property UserID As Integer
    Property UserCode As String
    Property FirstName As String
    Property LastName As String
    Property FullName As String
    Property Title As String

    Property StoreID As Integer
    Property StoreName As String

    Property BranchID As Integer
    Property BrandName As String

    Property TotalItems As Integer

    Property IsFlag As Boolean

End Class

Public Class NotificationModel
    Property ROWID As Int64
    Property NotifyDate As String
    Property NotifyType As String
    Property NotifySubject As String
    Property NotifyDescription As String
    Property NotifyToUserID As Integer
    Property NotifyToBranchID As Integer
    Property NotifyToStoreID As Integer
    Property NotifyNumber As Integer
    Property IsRead As Boolean
    Property ReadDate As String
    Property CreateDate As String
    Property CreateBy As String
    Property LastUpdateDate As String
    Property LastUpdateBy As String
End Class

Public Class PocketShareHeaderModel
    Property ROWID As Int64
    Property BranchID As Int64
    Property OrderNumber As String
    Property OrderDate As String
    Property OrderDescription As String
    Property Refer1Number As String
    Property Refer2Number As String
    Property BuyerName As String
    Property BuyerTel As String
    Property IsActive As Boolean
    Property Remark As String
    Property CreateDate As String
    Property CreateBy As Int64
    Property LastUpdateDate As String
    Property LastUpdateBy As Int64
    Property TotalLine As Integer

    Property IsApproved As Boolean
    Property ApprovedBy As String
    Property ApprovedDate As String
    Property IsRejected As String
    Property RejectedBy As String
    Property RejectedDate As String
    Property IsSend As Boolean
    Property SendBy As String
    Property SendDate As String

    Property PocketShareDetails As List(Of PocketShareDeatailModel)

End Class

Public Class PocketShareDeatailModel
    Property ROWID As Int64
    Property OrderID As Int64
    Property OrderNumber As String
    Property LineNumber As Integer

    Property CompetitorID As Int64
    Property ProductNo As String
    Property ProductName As String
    Property UnitCode As String
    Property Barcode As String

    Property ActualQty As Decimal
    Property IsActive As Boolean

    Property CreateBy As Int64
    Property CreateDate As String
    Property LastUpdateBy As Int64
    Property LastUpdateDate As String
End Class

Public Class CheckInOutModel
    Property ROWID As Int64
    Property StampDate As String
    Property UserId As Int64
    Property IsCheckIn As Boolean
    Property CheckInDateTime As String
    Property IsCheckOut As Boolean
    Property CheckOutDateTime As String
End Class

Public Class QuotationHeaderModel
    Property ROWID As Int64
    Property BranchID As Int64
    Property OrderNumber As String
    Property OrderDate As String
    Property OrderDescription As String
    Property Refer1Number As String
    Property Refer2Number As String
    Property BuyerName As String
    Property BuyerTel As String
    Property BuyerLineID As String
    Property IsActive As Boolean
    Property IsCompleted As Boolean
    Property CancellationReasons As String
    Property Remark As String

    Property CreateDate As String
    Property CreateBy As Int64
    Property LastUpdateDate As String
    Property LastUpdateBy As Int64
    Property TotalItem As Integer
    Property TotalPrice As Decimal

    Property QuotationDetailModel As List(Of QuotationDetailModel)
End Class

Public Class QuotationDetailModel
    Property ROWID As Int64
    Property OrderID As Int64
    Property OrderNumber As String
    Property LineNumber As Integer

    Property CompetitorID As Int64
    Property ProductNo As String
    Property ProductName As String
    Property UnitCode As String
    Property Barcode As String
    Property UnitPrice As Decimal
    Property PriceDescription As String

    Property Quantity As Decimal
    Property Remark As String
    Property IsActive As Boolean
    Property IsCompleted As Boolean

    Property CreateBy As Int64
    Property CreateDate As String
    Property LastUpdateBy As Int64
    Property LastUpdateDate As String
End Class


'---- CALCULATION -----

Public Class CalFormulaVariableModel
    Property VarKey As String
    Property VarName As String
    Property VarUnit As String
End Class
Public Class CalProjectTypeModel
    Property ProjectID As Integer
    Property ProjectName As String
    Property ImageUrl As String
    Property Remark As String

End Class

Public Class CalSubProjectModel
    Property ProjectID As Integer
    Property ProjectName As String
    Property SubProjectID As Integer
    Property SubProjectName As String
    Property ImageUrl As String
    Property Remark As String

    Property Main1Formula As String
    Property Main2Formula As String

    Property Main1Input As String
    Property Main2Input As String
End Class

Public Class CalMaterialGroupModel
    Property MatGroupID As Integer
    Property MatGroupName As String
    Property Remark As String
End Class


Public Class CalMaterialModel
    Property MatID As Integer
    Property MatGroupID As Integer
    Property MatGroupName As String
    Property Formula As String
    Property MatName As String
    Property Barcode As String
    Property CalType As String

    Property CalX As Decimal
    Property CalPcsSQM As Decimal
    Property CalPcsM As Decimal

    Property CategoryID As Integer
    Property CategoryName As String

    Property Category3 As String
    Property Category4 As String

    Property Remark As String

    Property Quantity As Integer

    Property ValidateStatus As String

    Property ValidateMessage As String

End Class


'---- CONTENT DOWNLOAD (CATALOG)
Public Class ContentDownloadModel
    Property ROWID As Decimal
    Property ContentType As String
    Property ContentSubject As String
    Property ContentCategory As String
    Property ProductDescription As String
    Property ContentTag As String
    Property ContentDate As String
    Property ContentDescription As String
    Property DisplayImageName As String
    Property IsActive As Boolean
    Property IsPinned As Boolean

    Property StartDate As String
    Property ExpireDate As String

    Property CreateDate As String
    Property CreateBy As String
    Property LastUpdateDate As String
    Property LastUpdateBy As String

    Property ImageUrl As String
End Class

Public Class ContentFilesModel
    Property ROWID As Decimal
    Property Ref_ROWID As Decimal
    Property FileName As String
    Property FileType As String
    Property Remark As String
    Property IsActive As Boolean
    Property CreateDate As String
    Property CreateBy As String
    Property LastUpdateDate As String
    Property LastUpdateBy As String

    Property FileUrl As String
End Class

Public Class ContentRolesModel
    Property ROWID As Decimal
    Property ContentID As Decimal
    Property Province As String
    Property StoreID As Integer
    Property BranchID As Integer
    Property LastUpdateDate As String
    Property LasteUpdateBy As String
End Class

'### SMART POCKET SHARE REPORT ##
Public Class SPRHeaderModel
    Property YearValue As Integer
    Property MonthValue As Integer
    Property Region7SM As String

    Property BranchCustCode As String '-- legion level set nothing
    Property BranchID As Integer '-- legion level set 0
    Property BranchName As String '-- legion level set nothing

    Property ProductGroup4SubCate As String

    Property DURANetValue As Decimal
    Property DURAPercentage As Decimal
    Property OTHERNetValue As Decimal
    Property OTHERPercentage As Decimal

End Class
Public Class SPRDetailModel
    Property YearValue As Integer
    Property MonthValue As Integer

    Property Region7SM As String
    Property BranchCustCode As String
    Property BranchID As Integer
    Property BranchName As String

    Property ProductGroup4SubCate As String
    Property NetValue As Decimal

    Property Percentage As Decimal

    Property Brand As String
End Class

'--- Sale Out --
Public Class SaleOutAndCommissionModel
    Property Region7SM As String
    Property BranchCustCode As String '-- legion level set nothing
    Property BranchID As Integer '-- legion level set 0
    Property BranchName As String '-- legion level set nothing
    'Property ProductGroup3 As String
    'Property ProductGroup3Name As String

    Property ProductGroup4 As String
    Property ProductGroup4Name As String

    Property FinalForecast As Decimal
    Property CurrentSalesOut As Decimal
    Property SalesOutPercent As Decimal
    Property SaleOutDiff As Decimal
    Property SaleOutEstActual As Decimal
    Property SaleOutEstDiff As Decimal

    Property CommissionRate As Decimal
    Property CommissionAmount As Decimal
End Class



'--- New Tracking  --
Public Class TrackingBranchDao
    Property Region7SM As String
    Property BranchCustCode As String
    Property BranchID As Integer
    Property BranchName As String
    Property BranchShipTo As String
    Property TrackingShipTo As String '-- without 000 lead
    Property BranchAddress As String
    Property BranchProvince As String
    Property BranchSCGLocation As String
    Property BranchSCGSoldTo As String
    Property CountPO As Integer
End Class

Public Class TrackingPOModel
    Property BranchName As String
    Property PONumber As String
    Property CountSO As Integer
End Class

Public Class TrackingSOModel
    Property BranchName As String
    Property PONumber As String
    Property SONumber As String
    Property CountItem As Integer
End Class

Public Class TrackingItemModel
    Property BranchName As String
    Property PONumber As String
    Property SONumber As String
    Property DNNumber As String
    Property MatCode As String
    Property MatName As String
    Property ProductUnit As String
    Property OrderQty As Integer
    Property FillQty As Integer
    Property BackQty As Integer
End Class