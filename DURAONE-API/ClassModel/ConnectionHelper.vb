﻿Imports System.Data.Entity.Core.EntityClient

Public Class ConnectionHelper
    Public Shared Function CreateConnectionString(metaData As String, dataSource As String, initialCatalog As String,
                                                      UserId As String, Password As String) As String
        Dim appName As String = "EntityFramework"
        Dim providerName As String = "System.Data.SqlClient"

        Dim connectionString As String = New EntityConnectionStringBuilder() _
                    With {.Metadata = metaData,
                            .Provider = providerName,
                            .ProviderConnectionString = New System.Data.SqlClient.SqlConnectionStringBuilder() _
                            With {.InitialCatalog = initialCatalog,
                                    .DataSource = dataSource,
                                    .IntegratedSecurity = Convert.ToBoolean(0),
                                    .UserID = UserId,
                                    .Password = Password}.ConnectionString}.ConnectionString

        Return connectionString
    End Function

    Public Shared Function CreateDURAConnection(metaData As String, dataSource As String, initialCatalog As String, UserId As String, Password As String) As DURAONEEntities
        Return New DURAONEEntities(CreateConnectionString(metaData, dataSource, initialCatalog, UserId, Password))
    End Function
End Class
Partial Public Class DURAONEEntities
    Inherits System.Data.Entity.DbContext

    Public Sub New(ConnectionString As String)
        MyBase.New(ConnectionString)
    End Sub
    Public Sub New(ExistingConnection As Common.DbConnection, OwnContext As Boolean)
        MyBase.New(ExistingConnection, OwnContext)
    End Sub
End Class