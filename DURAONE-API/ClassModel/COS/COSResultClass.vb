﻿Public Class APIResult
    Property success As Boolean
    Property message As String
    Property data As Object

    Sub New(pSuccess As Boolean, pMessage As String, pData As Object)
        success = pSuccess
        message = pMessage
        data = pData
    End Sub
    Sub New()

    End Sub
End Class


Public Class UserResult
    Property CompanyCode As String
    Property CompanyName As String
    Property UserCode As String
    Property FirstName As String
    Property LastName As String
    Property APIKey As String
    Property OriginIdDefault As Integer
    Property SoldToGroupDefault As String
End Class

Public Class OriginResult
    Property OriginId As Integer
    Property OriginCode As String
    Property OriginName As String
    Property Address As String
    Property Refer1 As String
    Property Refer2 As String
    Property Remark As String
    Property IsDefault As Boolean
End Class

Public Class SoldToGroupResult
    Property CompanyCode As String
    Property SoldToGroupCode As String
    Property SoldToGroupName As String
    Property Refer1 As String
    Property Refer2 As String
    Property Remark As String
    Property IsDefault As Boolean
End Class

Public Class SoldToResult
    Property CompanyCode As String
    Property SoldToGroupCode As String
    Property SoldToCode As String
    Property SoldToName As String
    Property Address As String
    Property Refer1 As String
    Property Refer2 As String
    Property Remark As String
End Class

Public Class OriginSoldToResult
    Property CompanyCode As String
    Property OriginId As Integer
    Property SoldToGroupCode As String
    Property SoldToSubCode As String
End Class

Public Class ServiceTypeResult
    Property ServiceTypeCode As String
    Property ServiceTypeName As String
End Class

Public Class PatternResult
    Property PatternCode As String
    Property PatternName As String
End Class

Public Class RouteResult
    Property RowID As Integer
    Property SAPShiptoCode As String
    Property RouteCode As String
    Property RegionCode As String
    Property RegionName As String
    Property ProvinceCode As String
    Property ProvinceName As String
    Property AmphurCode As String
    Property AmphurName As String
End Class

Public Class ProductResult
    Property CompanyCode As String
    Property ProductCode As String
    Property ProductGroupCode As String
    Property ProductName As String
    Property ProductDescription As String
    Property MaterialCode As String
    Property Barcode As String
    Property Volumn As Decimal
    Property Weight As Decimal
    Property Width As Decimal
    Property Length As Decimal
    Property Height As Decimal
    Property Remark As String
    Property IsActive As Boolean

    Property SoldToSubCode As String
    Property SoldToSubName As String
    Property SoldToGroupCode As String
    Property SoldToGroupName As String
    Property IsShareSoldToGroup As Boolean

End Class

Public Class CustomerResult
    Property CompanyCode As String
    Property CustomerCode As String
    Property CustomerName As String
    Property SAPShiptoCode As String
    Property Address1 As String
    Property Address2 As String
    Property Telephone As String
    Property Contact As String
    Property SoldToSubCode As String
    Property SoldToSubName As String
    Property SoldToSubGroupCode As String
    Property SoldToSubGroupName As String
    Property IsShareSoldToGroup As Boolean
    Property Remark As String

    '------ route ------
    Property RouteCode As String
    Property RegionCode As String
    Property RegionName As String
    Property ProvinceCode As String
    Property ProvinceName As String
    Property AmphurCode As String
    Property AmphurName As String
End Class

Public Class TransactionHeaderResult
    Property CompanyCode As String
    Property DocumentNo As String
    Property OriginId As Integer
    Property DocumentDate As String
    Property ServiceTypeCode As String
    Property ServiceTypeName As String
    Property DeliveryDate As String
    Property CustomerCode As String
    Property CustomerName As String
    Property SAPShiptoCode As String
    Property SoldToGroupCode As String
    Property SoldToSubCode As String
    Property PatternCode As String
    Property PatternName As String
    Property IsPrint As Boolean
    Property Remark As String

    '------ route ------
    Property RouteCode As String
    Property RegionCode As String
    Property RegionName As String
    Property ProvinceCode As String
    Property ProvinceName As String
    Property AmphurCode As String
    Property AmphurName As String
    Property HubCode As String
    Property HubName As String

    Property ActualBy As String

End Class

Public Class TransactionDetailResult
    Property CompanyCode As String
    Property DocumentNo As String
    Property OriginId As Integer
    Property SoldToGroupCode As String
    Property SoldToSubCode As String
    Property SAPShiptoCode As String
    Property RouteCode As String
    Property HubCode As String
    Property HubName As String

    Property ProductCode As String
    Property ProductName As String
    Property ProductDescription As String
    Property MaterialCode As String
    Property ProductBarcode As String
    Property Width As Decimal
    Property Length As Decimal
    Property Height As Decimal
    Property Weight As Decimal
    Property Volumn As Decimal
    Property Quantity As Decimal

    Property ActualBy As String
End Class

Public Class TransactionShippingMarkResult
    Property ROWID As Long
    Property CompanyCode As String
    Property OriginId As Integer
    Property DocumentNo As String

    Property SoldToGroup As String
    Property SoldTo As String

    Property ProductCode As String
    Property ProductName As String
    Property ProductDescription As String
    Property MaterialCode As String
    Property RouteCode As String
    Property HubCode As String
    Property HubName As String

    Property Width As Decimal
    Property Length As Decimal
    Property Height As Decimal
    Property Weight As Decimal
    Property Volumn As Decimal
    Property Quantity As Decimal

    Property ShippingMarkNo As String
    Property BarcodeText As String

    Property CheckedStatus As String
    Property CheckedBy As String
    Property CheckedFrom As String
    Property CheckedDate As String

    Property PickedStatus As Boolean
    Property PickedPalletNo As String
    Property PickedPalletStatus As String
    Property PickedDate As String
    Property PickedBy As String

    Property ActualBy As String
    Property IsDummy As Boolean
End Class


Public Class PalletItemResult
    Property ROWID As Long
    Property CompanyCode As String
    Property PickedPalletNo As String
    Property PickedPalletStatus As String
    Property PickedDate As String
    Property PickedBy As String

    Property ShippingMarkNo As String
    Property BarcodeText As String
    Property DocumentNo As String
    Property ProductCode As String
    Property ProductName As String

    Property ActualBy As String
End Class