﻿
Module Util
    Public g_platform_device As String = "Mobile"
    Public g_culture_en As New System.Globalization.CultureInfo("en-US")
    Public g_dummy_docno As String = "DUMMY"
    Public g_pattern_code_normal As String = "1"
    Public g_allow_unknown_barcode As Boolean = True

    Public Pub_metaData As String = ConfigurationManager.AppSettings("Metadata")
    Public Pub_dataSource As String = ConfigurationManager.AppSettings("DataSource")
    Public Pub_initialCatalog As String = ConfigurationManager.AppSettings("initialCatalog")
    Public Pub_UserId As String = ConfigurationManager.AppSettings("User")
    Public Pub_Password As String = ConfigurationManager.AppSettings("Password")
    Public Pub_OMS_URI As String = ConfigurationManager.AppSettings("OMS_URI")

    Public g_notifyType_PRICING As String = "PRICING"
    Public g_notifyType_NORMAL_PRICE As String = "NORMAL_PRICE"
    Public g_notifyType_POCKETSHARE As String = "POCKETSHARE"
    Public g_notifyType_APPORVE_PRICING As String = "APPORVE_PRICING"
    Public g_notifyType_APPORVE_NORMAL_PRICE As String = "APPORVE_NORMAL_PRICE"
    Public g_notifyType_APPORVE_POCKETSHARE As String = "APPORVE_POCKETSHARE"
    Public g_notifyType_REJECT_PRICING As String = "REJECT_PRICING"
    Public g_notifyType_REJECT_NORMAL_PRICE As String = "REJECT_NORMAL_PRICE"
    Public g_notifyType_REJECT_POCKETSHARE As String = "REJECT_POCKETSHARE"

    Public Const g_status_success As String = "100"
    Public Const g_status_fail As String = "200"
    Public Const g_status_userdevice_fail As String = "201"

    Public Const g_checkin_clocktype As String = "in"
    Public Const g_checkout_clocktype As String = "out"


    Public Const g_noti_action_promotion As String = "PROMOTION"
    Public Const g_noti_action_retail As String = "RETAIL"

    Public Const g_system_user As Integer = 1 ' String = "system"

    Public Function BkkNow() As Date
        Try
            Dim UTCDateTime = DateTime.UtcNow
            Dim BKKDateTime = UTCDateTime.AddHours(7)

            Return BKKDateTime
        Catch ex As Exception
            Return Now
        End Try
    End Function

    Public Function CheckUserDevice(pDeviceId As String, pUserId As Integer) As ActionResultModel
        Try
            Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                             Pub_UserId, Pub_Password)

                Dim UserDevice = (From e In dbContext.Users
                                  Where e.UserID = pUserId
                                  Select e).FirstOrDefault
                If UserDevice Is Nothing Then
                    Return New ActionResultModel With {.resultcode = g_status_userdevice_fail, .message = "ไม่พบรหัสผู้ใช้งานนี้ในระบบ!"}
                End If



                If UserDevice.DeviceID Is Nothing Or UserDevice.DeviceID = pDeviceId Then
                    Return New ActionResultModel With {.resultcode = g_status_success}
                Else

                    Return New ActionResultModel With {.resultcode = g_status_userdevice_fail, .message = "พบการ Login ล่าสุดที่เครื่อง " & UserDevice.DeviceName}
                End If

            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function DateToStringFormat(value As Date?) As String
        Try
            If value Is Nothing Then
                Return Nothing
            End If

            If value = Date.MinValue Then
                Return Nothing
            End If

            Return value.GetValueOrDefault(Date.MinValue).ToString("dd/MM/yyyy", Util.g_culture_en)

        Catch ex As Exception
            Throw ex
        End Try

    End Function


    Public Function convertStringToDate(stringDate As String) As Date
        Try
            Return Date.ParseExact(stringDate, "dd/MM/yyyy", Util.g_culture_en)
        Catch ex As Exception
            Throw ex
        End Try
    End Function


End Module
