﻿Imports System.IO
Imports System.Net
Imports System.Web.Hosting
Imports System.Web.Http
Imports System.Web.Http.Cors
Imports System.Web.Http.Description
Imports System.Security.Cryptography

Namespace Controllers.DURAONE
    Public Class ProductController
        Inherits ApiController

        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/GetProductCompetitors")>
        <EnableCors("*", "*", "*")>
        Public Function GetProductCompetitors(DeviceID As String, UserID As Integer, StoreID As Integer, Province As String) As IHttpActionResult
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                If Not ModelState.IsValid Then
                    Return BadRequest(ModelState)
                End If

                If StoreID = 0 Then
                    Return Ok(New ActionResultModel(g_status_fail, "StoreID is Required", Nothing))
                End If

                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                               Pub_UserId, Pub_Password)


                    Dim CompetitorsInfo = (From elm In dbContext.Competitors
                                           Let ToleranceValue As Decimal? = (If(elm.NormalPrice, 0) * If(elm.FaultTolerance, 0))
                                           Where elm.IsActive = True And elm.StoreID = StoreID And elm.Province = Province
                                           Select New CompetitorModel With {
                                         .ROWID = elm.ROWID,
                                         .StoreID = elm.StoreID,
                                         .StoreName = elm.Stores.StoreName,
                                         .ProductNo = elm.ProductNo,
                                         .ProductName = elm.ProductName,
                                         .UnitCode = elm.UnitCode,
                                         .Barcode = elm.Barcode,
                                         .CategoryID = If(elm.MatCategoryID, 0),
                                         .CategoryName = If(elm.Categories Is Nothing, "-", elm.Categories.CategoryName),
                                         .BrandID = If(elm.BrandID, 0),
                                         .BrandName = If(elm.Brands Is Nothing, "-", elm.Brands.BrandName),
                                         .Model = elm.Model,
                                         .NormalPrice = If(elm.NormalPrice, 0),
                                         .ToleranceMin = If(elm.NormalPrice, 0) - If(ToleranceValue, 0),
                                         .ToleranceMax = If(elm.NormalPrice, 0) + If(ToleranceValue, 0),
                                         .Province = elm.Province,
                                         .Remark = elm.Remark}).ToList

                    If CompetitorsInfo.Count = 0 Then
                        Return Ok(New ActionResultModel(g_status_fail, "Product not found", Nothing))
                    Else
                        Return Ok(New ActionResultModel(g_status_success, "success", CompetitorsInfo))
                    End If

                End Using

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function




        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/GetProductCompetitorsByBarcodeList")>
        <EnableCors("*", "*", "*")>
        Public Function GetProductCompetitorsByBarcodeList(DeviceID As String,
                                                       UserID As Integer,
                                                       StoreID As Integer,
                                                       Province As String,
                                                       Barcodes As List(Of String)) As IHttpActionResult
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                If Not ModelState.IsValid Then
                    Return BadRequest(ModelState)
                End If


                Dim compets As New List(Of CompetitorModel)

                For Each barcode In Barcodes
                    Dim res = localGetProductCompetitor(UserID, StoreID, Province, barcode, 0)
                    If res.resultcode = g_status_success Then
                        If res.data Is Nothing Then
                            Return Ok(New ActionResultModel(g_status_fail, "ไม่พบข้อมูลบาร์โค้ดสินค้า " & barcode, Nothing))
                        Else

                            'update Calcualtion Barcode (ใช้สำหรับ re-check ค่ากับการคำนวณ เพราะ Barcode จาก Compet อาจะไม่ตรงกับ Material ตอน return)
                            Dim obj As CompetitorModel = res.data
                            obj.CalculationBarcode = barcode
                            compets.Add(obj) 'success
                        End If

                    Else
                        Return Ok(New ActionResultModel(g_status_fail, "Barcode (" & barcode & ") " & res.message, Nothing))
                    End If
                Next

                Return Ok(New ActionResultModel(g_status_success, "success", compets))

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function


        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/GetProductCompetitorsByBarcode")>
        <EnableCors("*", "*", "*")>
        Public Function GetProductCompetitorsByBarcode(DeviceID As String,
                                                       UserID As Integer,
                                                       StoreID As Integer,
                                                       Province As String,
                                                       Barcode As String,
                                                       ProductId As Long) As IHttpActionResult
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                If Not ModelState.IsValid Then
                    Return BadRequest(ModelState)
                End If

                Dim res = localGetProductCompetitor(UserID, StoreID, Province, Barcode, ProductId)
                Return Ok(res)

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function


        Public Function localGetProductCompetitor(UserID As Integer,
                                                   StoreID As Integer,
                                                       Province As String,
                                                       Barcode As String,
                                                       ProductId As Long) As ActionResultModel
            Try
                Dim _CurDate As Date = Util.BkkNow.Date

                If StoreID = 0 Then
                    Return New ActionResultModel(g_status_fail, "StoreID is Required", Nothing)
                End If

                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                         Pub_UserId, Pub_Password)


                    Dim CompetitorsInfo As CompetitorModel = Nothing

                    CompetitorsInfo = (From elm In dbContext.Competitors
                                       Let ToleranceValue As Decimal? = (If(elm.NormalPrice, 0) * If(elm.FaultTolerance, 0))
                                       Where elm.IsActive = True _
                                               And elm.StoreID = StoreID _
                                               And elm.Province = Province _
                                               And elm.Barcode = Barcode _
                                               And If(ProductId = 0, True, ProductId = elm.ROWID)
                                       Select New CompetitorModel With {
                                         .ROWID = elm.ROWID,
                                         .StoreID = elm.StoreID,
                                         .StoreName = elm.Stores.StoreName,
                                         .ProductNo = elm.ProductNo,
                                         .ProductName = elm.ProductName,
                                         .UnitCode = elm.UnitCode,
                                         .Barcode = elm.Barcode,
                                         .CategoryID = If(elm.MatCategoryID, 0),
                                         .CategoryName = If(elm.Categories Is Nothing, "-", elm.Categories.CategoryName),
                                         .BrandID = If(elm.BrandID, 0),
                                         .BrandName = If(elm.Brands Is Nothing, "-", elm.Brands.BrandName),
                                         .Model = elm.Model,
                                         .NormalPrice = If(elm.NormalPrice, 0),
                                         .LastNormalPrice = If(elm.NormalPrice, 0),
                                         .ToleranceMin = If(elm.NormalPrice, 0) - If(ToleranceValue, 0),
                                         .ToleranceMax = If(elm.NormalPrice, 0) + If(ToleranceValue, 0),
                                         .Province = elm.Province,
                                         .Remark = elm.Remark}).FirstOrDefault

                    If CompetitorsInfo Is Nothing Then
                        're-check with product number
                        CompetitorsInfo = (From elm In dbContext.Competitors
                                           Let ToleranceValue As Decimal? = (If(elm.NormalPrice, 0) * If(elm.FaultTolerance, 0))
                                           Where elm.IsActive = True _
                                                   And elm.StoreID = StoreID _
                                                   And elm.Province = Province _
                                                   And elm.ProductNo = Barcode _
                                                   And If(ProductId = 0, True, ProductId = elm.ROWID)
                                           Select New CompetitorModel With {
                                             .ROWID = elm.ROWID,
                                             .StoreID = elm.StoreID,
                                             .StoreName = elm.Stores.StoreName,
                                             .ProductNo = elm.ProductNo,
                                             .ProductName = elm.ProductName,
                                             .UnitCode = elm.UnitCode,
                                             .Barcode = elm.Barcode,
                                             .CategoryID = If(elm.MatCategoryID, 0),
                                             .CategoryName = If(elm.Categories Is Nothing, "-", elm.Categories.CategoryName),
                                             .BrandID = If(elm.BrandID, 0),
                                             .BrandName = If(elm.Brands Is Nothing, "-", elm.Brands.BrandName),
                                             .Model = elm.Model,
                                             .NormalPrice = If(elm.NormalPrice, 0),
                                             .LastNormalPrice = If(elm.NormalPrice, 0),
                                             .ToleranceMin = If(elm.NormalPrice, 0) - If(ToleranceValue, 0),
                                             .ToleranceMax = If(elm.NormalPrice, 0) + If(ToleranceValue, 0),
                                             .Province = elm.Province,
                                             .Remark = elm.Remark}).FirstOrDefault
                    End If


                    '---- nothing
                    If CompetitorsInfo Is Nothing Then
                        Return New ActionResultModel(g_status_fail, "Product not found", Nothing)
                    Else

                        ' ------- get last promotion price (Approved) -----------

                        Dim getLastPromotion = (From elm In dbContext.UpdatePricing
                                                Let branch = (From br In dbContext.Branches Where br.BranchID = elm.BranchID Select br).FirstOrDefault
                                                Where branch IsNot Nothing _
                                                  And branch.StoreID = CompetitorsInfo.StoreID _
                                                  And elm.CompetitorID = CompetitorsInfo.ROWID _
                                                  And branch.Province = CompetitorsInfo.Province _
                                                  And elm.IsApproved = True
                                                Select elm Order By elm.CreateDate Descending).FirstOrDefault
                        If getLastPromotion IsNot Nothing Then
                            CompetitorsInfo.PromotionPrice = getLastPromotion.NewPrice.GetValueOrDefault(0)
                            CompetitorsInfo.PromotionDate = If(getLastPromotion.NewDate Is Nothing, Nothing, getLastPromotion.NewDate.Value.ToString("dd/MM/yyyy", g_culture_en))
                        Else
                            CompetitorsInfo.PromotionPrice = 0
                            CompetitorsInfo.PromotionDate = Nothing
                        End If


                        ' ------- get last normal price (Approved) -----------

                        Dim getLastNormalPrice = (From elm In dbContext.UpdateNormalPrice
                                                  Let branch = (From br In dbContext.Branches Where br.BranchID = elm.BranchID Select br).FirstOrDefault
                                                  Where branch IsNot Nothing _
                                                  And branch.StoreID = CompetitorsInfo.StoreID _
                                                  And elm.CompetitorID = CompetitorsInfo.ROWID _
                                                  And branch.Province = CompetitorsInfo.Province _
                                                  And elm.IsApproved = True
                                                  Select elm Order By elm.CreateDate Descending).FirstOrDefault
                        If getLastNormalPrice IsNot Nothing Then
                            CompetitorsInfo.LastNormalPrice = getLastNormalPrice.NewPrice.GetValueOrDefault(0)
                        Else
                            CompetitorsInfo.LastNormalPrice = 0
                        End If

                        ' ------- check alredy in cart (Promotion Price) -----------
                        Dim CheckAlreadyInCart = (From elm In dbContext.UpdatePricing
                                                  Let branch = (From br In dbContext.Branches Where br.BranchID = elm.BranchID Select br).FirstOrDefault
                                                  Where branch IsNot Nothing _
                                          And branch.StoreID = CompetitorsInfo.StoreID _
                                          And elm.CompetitorID = CompetitorsInfo.ROWID _
                                          And elm.TransactDate = _CurDate _
                                          And elm.CreateBy = UserID _
                                          And elm.IsActive = True _
                                          And (elm.IsSend = False OrElse elm.IsSend Is Nothing) _
                                          And (elm.IsApproved = False OrElse elm.IsApproved Is Nothing) _
                                          And (elm.IsRejected = False OrElse elm.IsRejected Is Nothing)
                                                  Select elm).FirstOrDefault

                        If CheckAlreadyInCart IsNot Nothing Then
                            CompetitorsInfo.AlreadyInCart = True
                        Else
                            CompetitorsInfo.AlreadyInCart = False
                        End If

                        ' ------- check alredy in cart (Normal Price) -----------
                        Dim CheckAlreadyInCartNormal = (From elm In dbContext.UpdateNormalPrice
                                                        Let branch = (From br In dbContext.Branches Where br.BranchID = elm.BranchID Select br).FirstOrDefault
                                                        Where branch IsNot Nothing _
                                          And branch.StoreID = CompetitorsInfo.StoreID _
                                          And elm.CompetitorID = CompetitorsInfo.ROWID _
                                          And elm.TransactDate = _CurDate _
                                          And elm.CreateBy = UserID _
                                          And elm.IsActive = True _
                                          And (elm.IsSend = False OrElse elm.IsSend Is Nothing) _
                                          And (elm.IsApproved = False OrElse elm.IsApproved Is Nothing) _
                                          And (elm.IsRejected = False OrElse elm.IsRejected Is Nothing)
                                                        Select elm).FirstOrDefault

                        If CheckAlreadyInCartNormal IsNot Nothing Then
                            CompetitorsInfo.AlreadyInCartNormal = True
                        Else
                            CompetitorsInfo.AlreadyInCartNormal = False
                        End If


                        ' ------- ActivePrice -----------
                        Dim isSetActivePrice As Boolean = False

                        '-- เช็ควันหมด โปรโมชั่น
                        If getLastPromotion IsNot Nothing Then
                            If getLastPromotion.NewDate.Value.Date >= _CurDate.Date Then
                                CompetitorsInfo.ActivePrice = getLastPromotion.NewPrice.GetValueOrDefault(0)
                                CompetitorsInfo.ActivePriceDescription = "ราคาโปรโมชั่น"
                                isSetActivePrice = True
                            End If
                        End If

                        '-- เช็คราคาขายปลีก (กรณีไม่มี Promotion)
                        If isSetActivePrice = False And getLastNormalPrice IsNot Nothing Then
                            If getLastNormalPrice.NewPrice.GetValueOrDefault(0) > 0 Then
                                CompetitorsInfo.ActivePrice = getLastNormalPrice.NewPrice.GetValueOrDefault(0)
                                CompetitorsInfo.ActivePriceDescription = "ราคาขายปลีก"
                                isSetActivePrice = True
                            End If
                        End If

                        '-- ไม่มีราคา promotion + ขายปลีก ใช้ราคาจาก Master
                        If isSetActivePrice = False Then
                            CompetitorsInfo.ActivePrice = CompetitorsInfo.NormalPrice
                            CompetitorsInfo.ActivePriceDescription = "ราคาปกติ"
                        End If

                        Return New ActionResultModel(g_status_success, "success", CompetitorsInfo)
                    End If



                End Using
            Catch ex As Exception
                Return New ActionResultModel(g_status_fail, ex.Message, Nothing)
            End Try
        End Function

    End Class
End Namespace