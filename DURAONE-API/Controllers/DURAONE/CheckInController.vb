﻿Imports System.IO
Imports System.Net
Imports System.Web.Hosting
Imports System.Web.Http
Imports System.Web.Http.Cors
Imports System.Web.Http.Description
Imports System.Security.Cryptography
Imports System.Threading.Tasks
Imports System.Net.Http
Imports System.Web.Script.Serialization
Imports RestSharp
Imports System.Web.Management

Namespace Controllers.DURAONE
    Public Class CheckInController
        Inherits ApiController

        'Dim _CurDate As Date = Util.BkkNow
        'Dim _checkdate As Date = Date.ParseExact(CheckDate, "dd/MM/yyyy", g_culture_en)

        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/getCheckInOut")>
        <EnableCors("*", "*", "*")>
        Public Function getCheckInOut(DeviceID As String, UserId As Integer) As IHttpActionResult
            Try
                Dim _CurDate As Date = Util.BkkNow
                Dim result As CheckInOutModel = Nothing
                Dim pUserId = UserId
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, pUserId)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                If Not ModelState.IsValid Then
                    Return BadRequest(ModelState)
                End If

                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                              Pub_UserId, Pub_Password)
                    Dim obj = (From elm In dbContext.CheckInOut
                               Where elm.UserId = pUserId _
                                             And elm.StampDate = _CurDate.Date
                               Select elm).FirstOrDefault
                    If obj IsNot Nothing Then
                        result = New CheckInOutModel
                        result.ROWID = obj.ROWID
                        result.UserId = obj.UserId

                        If obj.StampDate IsNot Nothing Then
                            result.StampDate = obj.StampDate.Value.ToString("dd/MM/yyyy", g_culture_en)
                        Else
                            result.StampDate = ""
                        End If

                        result.IsCheckIn = obj.IsCheckIn.GetValueOrDefault(False)
                        If obj.CheckInDateTime IsNot Nothing Then
                            result.CheckInDateTime = obj.CheckInDateTime.Value.ToString("HH:mm:ss", g_culture_en)
                        Else
                            result.CheckInDateTime = ""
                        End If

                        result.IsCheckOut = obj.IsCheckOut.GetValueOrDefault(False)
                        If obj.CheckOutDateTime IsNot Nothing Then
                            result.CheckOutDateTime = obj.CheckOutDateTime.Value.ToString("HH:mm:ss", g_culture_en)
                        Else
                            result.CheckOutDateTime = ""
                        End If

                    Else
                        result = New CheckInOutModel
                        result.ROWID = 0
                        result.UserId = UserId
                        result.StampDate = ""
                        result.IsCheckIn = False
                        result.CheckInDateTime = ""
                        result.IsCheckOut = False
                        result.CheckOutDateTime = ""
                    End If

                End Using


                Return Ok(New ActionResultModel(g_status_success, "Success", result))

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function

#Region "API EMPEO"

        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/GetWorkInSites")>
        <EnableCors("*", "*", "*")>
        Public Function GetWorkInSites(param As EMPEORequestDao) As IHttpActionResult
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(param.DeviceID, param.UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                If Not ModelState.IsValid Then
                    Return BadRequest(ModelState)
                End If

                '----- API get access token
                Dim objAccessToken = Me.CheckInAuthorization()
                If objAccessToken.error_message IsNot Nothing Then
                    Return Ok(New ActionResultModel(g_status_fail, objAccessToken.error_message, Nothing))
                End If

                '----- API get profile for login
                Dim profile = Me.getProfile(param.Email, param.UserCode, objAccessToken.access_token)
                If profile.error_message IsNot Nothing Then
                    Return Ok(New ActionResultModel(g_status_fail, profile.error_message, Nothing))
                ElseIf profile.status.code <> 1000 Then
                    Return Ok(New ActionResultModel(g_status_fail, profile.error_message, Nothing))
                End If

                '----- API Check In - Out
                Dim workInSiteResult = Me.getWorkInSite(profile.data, objAccessToken.access_token)

                Return Ok(workInSiteResult)

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function


        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/CheckInAuthorization")>
        <EnableCors("*", "*", "*")>
        Public Function CheckInAuthorization(param As EMPEORequestDao) As IHttpActionResult
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(param.DeviceID, param.UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                If Not ModelState.IsValid Then
                    Return BadRequest(ModelState)
                End If

                '----- Validate Clock Type -----
                Dim resultValidate = validateClockType(param.UserID, param.ClockType)
                If resultValidate <> "" Then
                    Return Ok(New ActionResultModel(g_status_fail, resultValidate, Nothing))
                End If

                '----- API get access token
                Dim objAccessToken = Me.CheckInAuthorization()
                If objAccessToken.error_message IsNot Nothing Then
                    Return Ok(New ActionResultModel(g_status_fail, objAccessToken.error_message, Nothing))
                End If

                '----- API get profile for login
                Dim profile = Me.getProfile(param.Email, param.UserCode, objAccessToken.access_token)
                If profile.error_message IsNot Nothing Then
                    Return Ok(New ActionResultModel(g_status_fail, profile.error_message, Nothing))
                ElseIf profile.status.code <> 1000 Then
                    Return Ok(New ActionResultModel(g_status_fail, profile.error_message, Nothing))
                End If

                '----- API Check In - Out
                Dim objCheckInOutResult = Me.checkInOut(profile.data, param, objAccessToken.access_token)
                If objCheckInOutResult.resultcode = Util.g_status_fail Then
                    Return Ok(objCheckInOutResult)
                End If

                '====== DURAONE SAVE DB ======
                Me.saveChekInOut(param)

                Return Ok(New ActionResultModel(g_status_success, "Success", Nothing))

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function


        Private Function CheckInAuthorization() As EMPEOAccessToken
            Try
                Dim auth_url As String = ConfigurationManager.AppSettings("configEMPEOAuthUrl")
                Dim client_id As String = ConfigurationManager.AppSettings("configEMPEOClientId")
                Dim ocpApimSubscriptionKey As String = ConfigurationManager.AppSettings("configEMPEOOcpApimSubscriptionKey")


                Dim client = New RestClient(auth_url)
                client.Timeout = -1
                Dim Request = New RestRequest(Method.POST)
                Request.AddHeader("Content-Type", "application/x-www-form-urlencoded")
                Request.AddHeader("Cache-Control", "no-cache")
                Request.AddHeader("Ocp-Apim-Subscription-Key", ocpApimSubscriptionKey)
                Request.AddParameter("application/x-www-form-urlencoded", client_id, ParameterType.RequestBody)
                Dim response As IRestResponse = client.Execute(Request)
                Console.WriteLine(response.Content)

                If response.IsSuccessful Then
                    Dim jsonText = response.Content
                    Dim jss As New JavaScriptSerializer()
                    jss.MaxJsonLength = Int32.MaxValue
                    Dim dict = jss.Deserialize(Of EMPEOAccessToken)(jsonText)
                    If dict Is Nothing OrElse dict.access_token.Length = 0 Then
                        Return New EMPEOAccessToken With {.error_message = "access_token is empty!"}
                    Else
                        Return dict
                    End If
                Else
                    Return New EMPEOAccessToken With {.error_message = "cannot get access_token. Error message is " & response.Content.ToString}
                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Function getProfile(EmailAddress As String,
                                        EmployeeNo As String,
                                        AccessToken As String) As EMPEOProfile
            Try
                Dim getProfile_url As String = ConfigurationManager.AppSettings("configEMPEOProfileUrl")
                Dim ocpApimSubscriptionKey As String = ConfigurationManager.AppSettings("configEMPEOOcpApimSubscriptionKey")
                Dim paramVersion As String = ConfigurationManager.AppSettings("configEMPEOProfileParamVersion")


                Dim client = New RestClient(getProfile_url &
                                            "?EmailAddress=" & EmailAddress &
                                            "&EmployeeNo=" & EmployeeNo &
                                            "&version=" & paramVersion)
                client.Timeout = -1
                Dim Request = New RestRequest(Method.GET)
                Request.AddHeader("Cache-Control", "no-cache")
                Request.AddHeader("Ocp-Apim-Subscription-Key", ocpApimSubscriptionKey)
                Request.AddHeader("Authorization", "Bearer " & AccessToken)
                Dim response As IRestResponse = client.Execute(Request)
                Console.WriteLine(response.Content)

                Dim jsonText = response.Content
                Dim jss As New JavaScriptSerializer()
                jss.MaxJsonLength = Int32.MaxValue

                If response.IsSuccessful Then
                    Dim result = jss.Deserialize(Of EMPEOProfile)(jsonText)

                    If result Is Nothing Then
                        Return New EMPEOProfile With {.error_message = "cannot get profile. Deserialize result is empty"}
                    End If

                    If result.data Is Nothing Then
                        Return New EMPEOProfile With {.error_message = "cannot get profile. Deserialize result.data is empty"}
                    End If

                    Return result
                Else
                    Dim resultError As EMPEOProfile
                    Try
                        resultError = jss.Deserialize(Of EMPEOProfile)(response.Content.ToString)
                        If resultError IsNot Nothing Then
                            Return New EMPEOProfile With {.error_message = "cannot get profile. Code: " & resultError.status.code & " Description: " & resultError.status.description}
                        Else
                            Return New EMPEOProfile With {.error_message = "cannot get profile." & response.Content.ToString}
                        End If
                    Catch ex As Exception
                        Return New EMPEOProfile With {.error_message = "cannot get profile." & response.Content.ToString}
                    End Try



                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Function getWorkInSite(userId As String,
                                        AccessToken As String) As ActionResultModel
            Try
                Dim getWorkInSite_url As String = ConfigurationManager.AppSettings("configGetWorkInSite")
                Dim ocpApimSubscriptionKey As String = ConfigurationManager.AppSettings("configEMPEOOcpApimSubscriptionKey")


                Dim client = New RestClient(getWorkInSite_url &
                                            "?userId=" & userId)
                client.Timeout = -1
                Dim Request = New RestRequest(Method.GET)
                Request.AddHeader("Content-Type", "application/json")
                Request.AddHeader("Ocp-Apim-Subscription-Key", ocpApimSubscriptionKey)
                Request.AddHeader("Authorization", "Bearer " & AccessToken)
                Dim response As IRestResponse = client.Execute(Request)
                Console.WriteLine(response.Content)

                Dim jsonText = response.Content
                Dim jss As New JavaScriptSerializer()
                jss.MaxJsonLength = Int32.MaxValue

                If response.IsSuccessful Then
                    Dim result = jss.Deserialize(Of EMPEOWorkinsiteRoot)(jsonText)

                    If result Is Nothing Then
                        Return New ActionResultModel With {.resultcode = g_status_fail, .message = "ไม่พบข้อมูล workinsite จากระบบ"}
                    End If

                    If result.status Is Nothing Then
                        Return New ActionResultModel With {.resultcode = g_status_fail, .message = "ไม่พบข้อมูล workinsite จากระบบ"}
                    End If


                    If result.status.code <> "1000" Then
                        Return New ActionResultModel With {.resultcode = g_status_fail, .message = result.status.description}
                    End If

                    If result.data Is Nothing Then
                        Return New ActionResultModel With {.resultcode = g_status_fail, .message = "ไม่พบข้อมูล workinsite จากระบบ"}
                    End If

                    If result.data Is Nothing Then
                        Return New ActionResultModel With {.resultcode = g_status_fail, .message = "ไม่พบข้อมูล workinsite จากระบบ"}
                    End If


                    Return New ActionResultModel With {.resultcode = g_status_success, .message = "Success", .data = result.data}

                Else
                    Dim resultError As EMPEOWorkinsiteRoot
                    Try
                        If response.Content Is Nothing Then
                            Return New ActionResultModel With {.resultcode = g_status_fail, .message = "ไม่พบข้อมูล workinsite จากระบบ" & response.Content.ToString}
                        End If

                        resultError = jss.Deserialize(Of EMPEOWorkinsiteRoot)(response.Content.ToString)
                        If resultError.status IsNot Nothing Then
                            Return New ActionResultModel With {.resultcode = g_status_fail, .message = resultError.status.description}
                        Else
                            Return New ActionResultModel With {.resultcode = g_status_fail, .message = "cannot get workinsite." & response.Content.ToString}
                        End If
                    Catch ex As Exception
                        Return New ActionResultModel With {.resultcode = g_status_fail, .message = "cannot get workinsite." & response.Content.ToString}
                    End Try



                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Function checkInOut(UserId As String,
                                    param As EMPEORequestDao,
                                        AccessToken As String) As ActionResultModel
            Try
                Dim jss As New JavaScriptSerializer()
                jss.MaxJsonLength = Int32.MaxValue

                Dim postWorkin_url As String = ConfigurationManager.AppSettings("configPostWorkin")
                Dim ocpApimSubscriptionKey As String = ConfigurationManager.AppSettings("configEMPEOOcpApimSubscriptionKey")
                Dim paramVersion As String = ConfigurationManager.AppSettings("configEMPEOProfileParamVersion")

                '  Dim client = New RestClient(timeStamp_url & "?version=" & paramVersion)

                Dim url As String = postWorkin_url

                Dim client = New RestClient(url)

                client.Timeout = -1
                Dim Request = New RestRequest(Method.POST)
                Request.AddHeader("Content-Type", "application/json-patch+json")
                Request.AddHeader("Cache-Control", "no-cache")
                Request.AddHeader("Ocp-Apim-Subscription-Key", ocpApimSubscriptionKey)
                Request.AddHeader("Authorization", "Bearer " & AccessToken)

                Dim body As New PostWorkinBody
                body.EmployeeRefId = Nothing
                body.UserId = UserId

                body.Latitude = param.Latitude
                body.Longitude = param.Longitude

                body.UUID = Nothing

                body.LocationName = param.LocationName
                body.Location = param.SiteName

                body.Note = Nothing
                body.IsWorkFromHome = False

                If param.ClockType = Util.g_checkin_clocktype Then
                    body.Status = 105 'check-in
                Else
                    body.Status = 106 'check-out
                End If

                body.Distance = param.Distance
                body.Attachments = Nothing


                Dim JsonBodyString = jss.Serialize(body)

                Request.AddParameter("application/json-patch+json", JsonBodyString, ParameterType.RequestBody)


                Dim response As IRestResponse = client.Execute(Request)

                Dim jsonText = response.Content

                If response.IsSuccessful Then
                    Dim result = jss.Deserialize(Of PostWorkinResult)(jsonText)

                    If result Is Nothing Then
                        Return New ActionResultModel With {.resultcode = g_status_fail, .message = "cannot get workinsite. Deserialize result is empty"}
                    End If

                    If result.status Is Nothing Then
                        Return New ActionResultModel With {.resultcode = g_status_fail, .message = "ไม่พบข้อมูล status การบันทึก จากระบบ API"}
                    End If


                    If result.status.code <> "1000" Then
                        Dim desc As String = result.status.description
                        Dim message As String = ""
                        If result.data IsNot Nothing Then message = result.data.message

                        Return New ActionResultModel With {.resultcode = g_status_fail, .message = result.status.description & " " & message}
                    End If


                        Return New ActionResultModel With {.resultcode = g_status_success, .message = result.status.description, .data = Nothing}

                Else
                    Dim resultError As PostWorkinResult
                    Try
                        resultError = jss.Deserialize(Of PostWorkinResult)(response.Content.ToString)
                        If resultError.status IsNot Nothing Then
                            Return New ActionResultModel With {.resultcode = g_status_fail, .message = resultError.status.description & " " & If(resultError.data IsNot Nothing, resultError.data.message, "")}
                        Else
                            Return New ActionResultModel With {.resultcode = g_status_fail, .message = "cannot save workin." & response.Content.ToString}
                        End If
                    Catch ex As Exception
                        Return New ActionResultModel With {.resultcode = g_status_fail, .message = "cannot save workin." & response.Content.ToString}
                    End Try
                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Function

#End Region

        Public Function validateClockType(pUserId As Integer, pClockType As String) As String
            Try
                Dim _CurDate As Date = Util.BkkNow
                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                          Pub_UserId, Pub_Password)

                    Dim objCheck = (From elm In dbContext.CheckInOut
                                    Where elm.StampDate = _CurDate.Date _
                                            And elm.UserId = pUserId
                                    Select elm).FirstOrDefault
                    If objCheck Is Nothing Then
                        If pClockType = g_checkout_clocktype Then
                            Return "ยังไม่พบการ CHECK IN ของผู้ใช้งานนี้!"
                        End If

                    Else
                        If objCheck.IsCheckIn = True Then
                            If pClockType = g_checkin_clocktype Then
                                Return "ผู้ใช้งานนี้ CHECK-IN ระบบแล้ว!"
                            End If
                        End If

                        If objCheck.IsCheckOut = True Then
                            If pClockType = g_checkout_clocktype Then
                                Return "ผู้ใช้งานนี้ CHECK-OUT ระบบแล้ว!"
                            End If
                        End If

                    End If
                End Using

                Return ""

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Private Sub saveChekInOut(param As EMPEORequestDao)
            Try
                Dim _CurDate As Date = Util.BkkNow
                ' Dim _checkdate As Date = Date.ParseExact(_CurDate, "dd/MM/yyyy", g_culture_en)
                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                              Pub_UserId, Pub_Password)
                    Dim objSaveCheck As CheckInOut = Nothing

                    objSaveCheck = (From elm In dbContext.CheckInOut
                                    Where elm.StampDate = _CurDate.Date _
                                            And elm.UserId = param.UserID
                                    Select elm).FirstOrDefault
                    If objSaveCheck IsNot Nothing Then
                        '--- update 
                        If param.ClockType = Util.g_checkin_clocktype Then
                            objSaveCheck.IsCheckIn = True
                            objSaveCheck.CheckInDateTime = _CurDate

                            objSaveCheck.CheckInLatiutude = param.Latitude
                            objSaveCheck.CheckInLongitude = param.Longitude
                            objSaveCheck.CheckInLocationName = param.LocationName
                            objSaveCheck.CheckInSiteName = param.SiteName
                        Else
                            objSaveCheck.IsCheckOut = True
                            objSaveCheck.CheckOutDateTime = _CurDate

                            objSaveCheck.CheckOutLatiutude = param.Latitude
                            objSaveCheck.CheckOutLongitude = param.Longitude
                            objSaveCheck.CheckOutLocationName = param.LocationName
                            objSaveCheck.CheckOutSiteName = param.SiteName
                        End If
                    Else
                        '---- insert
                        objSaveCheck = New CheckInOut
                        objSaveCheck.UserId = param.UserID
                        objSaveCheck.StampDate = _CurDate.Date

                        If param.ClockType = Util.g_checkin_clocktype Then
                            objSaveCheck.IsCheckIn = True
                            objSaveCheck.CheckInDateTime = _CurDate

                            objSaveCheck.CheckInLatiutude = param.Latitude
                            objSaveCheck.CheckInLongitude = param.Longitude
                            objSaveCheck.CheckInLocationName = param.LocationName
                            objSaveCheck.CheckInSiteName = param.SiteName
                        Else
                            objSaveCheck.IsCheckOut = True
                            objSaveCheck.CheckOutDateTime = _CurDate

                            objSaveCheck.CheckOutLatiutude = param.Latitude
                            objSaveCheck.CheckOutLongitude = param.Longitude
                            objSaveCheck.CheckOutLocationName = param.LocationName
                            objSaveCheck.CheckOutSiteName = param.SiteName
                        End If
                        dbContext.CheckInOut.Add(objSaveCheck)
                    End If

                    dbContext.SaveChanges()

                End Using



            Catch ex As Exception
                Throw ex
            End Try
        End Sub


#Region "Authen"
        Class EMPEORequestDao
            Public Property DeviceID As String
            Public Property UserID As Integer
            Public Property UserCode As String
            Public Property Email As String
            Public Property ClockType As String

            Public Property Latitude As Decimal
            Public Property Longitude As Decimal
            Public Property Distance As Integer
            Public Property LocationName As String
            Public Property SiteName As String
        End Class

        Class EMPEOAccessToken
            Public Property error_message As String
            Public Property access_token As String
            Public Property expires_in As Integer
            Public Property token_type As String
            Public Property scope As String
        End Class

#End Region

#Region "Get Profile"
        Class EMPEOProfile
            Public Property error_message As String '--- from local
            Public Property status As EMPEOProfileStatus
            Public Property data As String
        End Class

        Class EMPEOProfileStatus
            Public Property code As String
            Public Property description As String
        End Class
#End Region


#Region "Time Stamp"
        Class EMPEOTimeStampResult
            Public Property status As Status
            Public Property data As Data
            Public Property error_message As String '--- from local
        End Class
        Class Status
            Public Property code As Integer
            Public Property description As String
        End Class

        Class Data
            Public Property result As Boolean
            Public Property message As String
        End Class

        Class EMPEOTimeStampBody
            Public Property userId As String
            Public Property clockType As String

            Public Property latitude As Decimal
            Public Property longitude As Decimal
            Public Property locationName As String
            Public Property siteName As String
        End Class
#End Region

#Region "Workinsite"

        Public Class EMPEOWorkinsiteRoot
            Public Property status As EMPEOWorkinsiteStatus
            Public Property data As List(Of EMPEOWorkinsite)
        End Class


        Public Class EMPEOWorkinsiteStatus
            Public Property code As String
            Public Property description As String
        End Class

        Public Class EMPEOWorkinsite
            Public Property employeeRefId As String
            Public Property userId As String
            Public Property siteId As Integer
            Public Property siteName As String
            Public Property workinType As Integer
            Public Property deviceId As String
            Public Property isBiometicScanner As Boolean
            Public Property location As String
            Public Property locationName As String
            Public Property isWorkFromHome As Boolean
            Public Property radius As Integer
            Public Property note As String
            Public Property latitude As Double
            Public Property longitude As Double
            Public Property uuid As String
            Public Property major As String
            Public Property minor As String
            Public Property stationKey As String
            Public Property isTimeLock As Boolean
            Public Property isActive As Boolean
        End Class

#End Region

        Public Class PostWorkinBody
            Public Property EmployeeRefId As String ' : "80104", //รหัสพนักงาน จะใส่ EmployeeRefId หรือ UserId อย่างใดอย่างหนึ่ง
            Public Property UserId As String ' : "{EEA2CD94-05B2-448B-9829-8A9300F9CDDA}", //userId ของพนักงาน จะใส่ EmployeeRefId หรือ UserId อย่างใดอย่างหนึ่ง
            Public Property Latitude As Double ' : 13.5617816, //The latitude number of the workin location
            Public Property Longitude As Double ' : 100.3413238, //The longitude number of the workin location
            Public Property UUID As String ' : "B9407F30-F5F8-466E-AFF9-25556B57FE6D", //A unique id for IOMO beacon device
            Public Property LocationName As String ' : "โกไฟว์", //Location name จาก google
            Public Property Location As String  ': "โกไฟว์", //ดูจาก workin site ของพนักงาน ต้องตรงกัน
            Public Property Note As String ' : "ทดสอบ BFF", //Workin report written by employee
            Public Property IsWorkFromHome As Boolean ' : false, //Specify whether the workin type allowed employees to work from home Or Not. *Default Is false
            Public Property Status As Integer ' : 105, //105 = workin, 106 = workout
            Public Property Distance As String ' : 200, //The accurate distance between the actual workin location And the determined GPS location
            Public Property Attachments As Object
        End Class

#Region "workinsite result"

        Public Class PostWorkinResult

            Public Property status As PostWorkinStatus
            Public Property data As PostWorkinData
        End Class

        Public Class PostWorkinData

            Public Property message_EN As String
            Public Property message As String
            Public Property result As Boolean
        End Class

        Public Class PostWorkinStatus

            Public Property code As String
            Public Property description As String
        End Class


#End Region


    End Class



End Namespace