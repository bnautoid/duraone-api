﻿Imports System.IO
Imports System.Net
Imports System.Web.Hosting
Imports System.Web.Http
Imports System.Web.Http.Cors
Imports System.Web.Http.Description
Imports System.Security.Cryptography
Imports System.Data.Entity

Namespace Controllers.DURAONE
    Public Class PocketShareController
        Inherits ApiController

        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/getPocketShareHeader")>
        <EnableCors("*", "*", "*")>
        Public Function getPocketShareHeader(DeviceID As String, UserID As Integer, BranchID As Integer)
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                Dim _CurDate As Date = Util.BkkNow

                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                                  Pub_UserId, Pub_Password)

                    Dim getNotifications = (From e In dbContext.PocketShareHD
                                            Where (e.IsActive = True And e.BranchID = BranchID And (e.IsApproved Is Nothing OrElse e.IsApproved = False)) _
                                                And ((e.IsSend Is Nothing OrElse e.IsSend = False) Or (e.IsSend = True And e.IsRejected = True))
                                            Order By e.CreateDate Descending
                                            Select e).ToList
                    'Or (e.IsApproved Is Nothing OrElse e.IsApproved = False) _
                    'And (e.IsRejected Is Nothing OrElse e.IsRejected = False)

                    Dim result = (From e In getNotifications
                                  Select New PocketShareHeaderModel With {
                                    .ROWID = e.ROWID,
                                    .BranchID = e.BranchID,
                                    .OrderNumber = e.OrderNumber,
                                    .OrderDate = e.OrderDate.ToString("dd/MM/yyyy", g_culture_en),
                                    .OrderDescription = e.OrderDescription,
                                    .Refer1Number = e.Refer1Number,
                                    .Refer2Number = e.Refer2Number,
                                    .BuyerName = e.BuyerName,
                                    .BuyerTel = e.BuyerTel,
                                    .IsApproved = e.IsApproved.GetValueOrDefault(False),
                                    .ApprovedBy = e.ApprovedBy.GetValueOrDefault(0),
                                    .ApprovedDate = If(e.ApprovedDate Is Nothing, Nothing, e.ApprovedDate.Value.ToString("dd/MM/yyyy", g_culture_en)),
                                    .IsRejected = e.IsRejected.GetValueOrDefault(False),
                                    .RejectedBy = e.RejectedBy.GetValueOrDefault(0),
                                    .RejectedDate = If(e.RejectedDate Is Nothing, Nothing, e.RejectedDate.Value.ToString("dd/MM/yyyy", g_culture_en)),
                                    .IsSend = e.IsSend.GetValueOrDefault(False),
                                    .SendBy = e.SendBy.GetValueOrDefault(0),
                                    .SendDate = If(e.SendDate Is Nothing, Nothing, e.SendDate.Value.ToString("dd/MM/yyyy", g_culture_en)),
                                    .IsActive = e.IsActive.GetValueOrDefault(False),
                                    .Remark = e.Remark,
                                    .CreateDate = e.CreateDate.Value.ToString("dd/MM/yyyy", g_culture_en),
                                    .CreateBy = e.CreateBy.GetValueOrDefault(0),
                                    .LastUpdateDate = e.LastUpdateDate.Value.ToString("dd/MM/yyyy", g_culture_en),
                                    .LastUpdateBy = e.LastUpdateBy.GetValueOrDefault(0),
                                    .TotalLine = e.PocketShareDT.Count
                        }).ToList

                    Return Ok(New ActionResultModel(g_status_success, "Success", result))
                End Using

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try

        End Function

        Private Function getPocketShareHeaderById(DeviceID As String, UserID As Integer, rowId As Int64)
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                Dim _CurDate As Date = Util.BkkNow

                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                                  Pub_UserId, Pub_Password)

                    Dim getNotifications = (From e In dbContext.PocketShareHD
                                            Where e.IsActive = True And e.ROWID = rowId
                                            Order By e.CreateDate Descending
                                            Select e).ToList

                    '                    And (e.IsSend Is Nothing OrElse e.IsSend = False) _
                    'And (e.IsApproved Is Nothing OrElse e.IsApproved = False) _
                    'And (e.IsRejected Is Nothing OrElse e.IsRejected = False)

                    Dim result = (From e In getNotifications
                                  Select New PocketShareHeaderModel With {
                                    .ROWID = e.ROWID,
                                    .BranchID = e.BranchID,
                                    .OrderNumber = e.OrderNumber,
                                    .OrderDate = e.OrderDate.ToString("dd/MM/yyyy", g_culture_en),
                                    .OrderDescription = e.OrderDescription,
                                    .Refer1Number = e.Refer1Number,
                                    .Refer2Number = e.Refer2Number,
                                    .BuyerName = e.BuyerName,
                                    .BuyerTel = e.BuyerTel,
                                    .IsApproved = e.IsApproved.GetValueOrDefault(False),
                                    .ApprovedBy = e.ApprovedBy.GetValueOrDefault(0),
                                    .ApprovedDate = If(e.ApprovedDate Is Nothing, Nothing, e.ApprovedDate.Value.ToString("dd/MM/yyyy", g_culture_en)),
                                    .IsRejected = e.IsRejected.GetValueOrDefault(False),
                                    .RejectedBy = e.RejectedBy.GetValueOrDefault(0),
                                    .RejectedDate = If(e.RejectedDate Is Nothing, Nothing, e.RejectedDate.Value.ToString("dd/MM/yyyy", g_culture_en)),
                                    .IsSend = e.IsSend.GetValueOrDefault(False),
                                    .SendBy = e.SendBy.GetValueOrDefault(0),
                                    .SendDate = If(e.SendDate Is Nothing, Nothing, e.SendDate.Value.ToString("dd/MM/yyyy", g_culture_en)),
                                    .IsActive = e.IsActive.GetValueOrDefault(False),
                                    .Remark = e.Remark,
                                    .CreateDate = e.CreateDate.Value.ToString("dd/MM/yyyy", g_culture_en),
                                    .CreateBy = e.CreateBy.GetValueOrDefault(0),
                                    .LastUpdateDate = e.LastUpdateDate.Value.ToString("dd/MM/yyyy", g_culture_en),
                                    .LastUpdateBy = e.LastUpdateBy.GetValueOrDefault(0),
                                    .TotalLine = e.PocketShareDT.Count
                        }).FirstOrDefault

                    Return result
                End Using

            Catch ex As Exception
                Throw ex
            End Try

        End Function

        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/getPocketDetails")>
        <EnableCors("*", "*", "*")>
        Public Function getPocketDetails(DeviceID As String, UserID As Integer, OrderID As Int64)
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                Dim _CurDate As Date = Util.BkkNow

                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                                  Pub_UserId, Pub_Password)

                    Dim getNotifications = (From pks In dbContext.PocketShareDT
                                            Let prd = (From p In dbContext.Competitors Where p.ROWID = pks.CompetitorID Select p).FirstOrDefault
                                            Where prd IsNot Nothing _
                                                And pks.IsActive = True _
                                                And pks.OrderID = OrderID
                                            Order By pks.LineNumber
                                            Select pks, prd).ToList

                    Dim result = (From e In getNotifications
                                  Select New PocketShareDeatailModel With {
                            .ROWID = e.pks.ROWID,
                            .OrderID = e.pks.OrderID,
                            .OrderNumber = e.pks.OrderNumber,
                            .LineNumber = e.pks.LineNumber,
                            .CompetitorID = e.pks.CompetitorID,
                            .ProductNo = e.prd.ProductNo,
                            .ProductName = e.prd.ProductName,
                            .UnitCode = e.prd.UnitCode,
                            .Barcode = e.prd.Barcode,
                            .ActualQty = e.pks.ActualQty,
                            .IsActive = e.pks.IsActive.GetValueOrDefault(0),
                            .CreateBy = e.pks.CreateBy,
                            .CreateDate = If(e.pks.CreateDate Is Nothing, Nothing, e.pks.CreateDate.Value.ToString("dd/MM/yyyy", g_culture_en)),
                            .LastUpdateBy = e.pks.LastUpdateBy,
                            .LastUpdateDate = If(e.pks.LastUpdateDate Is Nothing, Nothing, e.pks.LastUpdateDate.Value.ToString("dd/MM/yyyy", g_culture_en))
                        }).ToList

                    Return Ok(New ActionResultModel(g_status_success, "Success", result))
                End Using

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try

        End Function

        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/savePocketShare")>
        <EnableCors("*", "*", "*")>
        Public Function savePocketShare(DeviceID As String, UserID As Integer, pocketShareHeaderModel As PocketShareHeaderModel)
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                Dim _CurDate As Date = Util.BkkNow
                Dim _isNewHeader As Boolean = False

                If pocketShareHeaderModel Is Nothing Then
                    Return Ok(New ActionResultModel(g_status_fail, "Not found Pocket Share Header parameter!", Nothing))
                End If

                'If pocketShareHeaderModel.PocketShareDetails Is Nothing Then
                '    Return Ok(New ActionResultModel(False, "Not found Pocket Share Detail Parameter!", Nothing))
                'End If

                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                                  Pub_UserId, Pub_Password)
                    '===== SAVE HEADER ===
                    Dim objSaveHeader As PocketShareHD = Nothing
                    objSaveHeader = (From elm In dbContext.PocketShareHD
                                     Where elm.ROWID = pocketShareHeaderModel.ROWID
                                     Select elm).FirstOrDefault
                    If objSaveHeader Is Nothing Then
                        '---- insert new header
                        _isNewHeader = True
                        objSaveHeader = New PocketShareHD
                        objSaveHeader.BranchID = pocketShareHeaderModel.BranchID
                        objSaveHeader.OrderNumber = GenerateBillNumber(pocketShareHeaderModel.BranchID) '--- Auto Generate
                        objSaveHeader.OrderDate = Date.ParseExact(pocketShareHeaderModel.OrderDate, "dd/MM/yyyy", g_culture_en).Date
                        objSaveHeader.OrderDescription = pocketShareHeaderModel.OrderDescription
                        objSaveHeader.Refer1Number = pocketShareHeaderModel.Refer1Number
                        objSaveHeader.Refer2Number = pocketShareHeaderModel.Refer2Number
                        objSaveHeader.BuyerName = pocketShareHeaderModel.BuyerName
                        objSaveHeader.BuyerTel = pocketShareHeaderModel.BuyerTel
                        objSaveHeader.IsActive = pocketShareHeaderModel.IsActive
                        objSaveHeader.Remark = pocketShareHeaderModel.Remark
                        objSaveHeader.CreateDate = _CurDate
                        objSaveHeader.CreateBy = pocketShareHeaderModel.CreateBy
                        objSaveHeader.LastUpdateDate = _CurDate
                        objSaveHeader.LastUpdateBy = pocketShareHeaderModel.LastUpdateBy

                    Else
                        '---- update header
                        objSaveHeader.OrderDate = Date.ParseExact(pocketShareHeaderModel.OrderDate, "dd/MM/yyyy", g_culture_en).Date
                        objSaveHeader.Refer1Number = pocketShareHeaderModel.Refer1Number
                        objSaveHeader.Refer2Number = pocketShareHeaderModel.Refer2Number
                        objSaveHeader.BuyerName = pocketShareHeaderModel.BuyerName
                        objSaveHeader.BuyerTel = pocketShareHeaderModel.BuyerTel
                        objSaveHeader.IsActive = pocketShareHeaderModel.IsActive
                        objSaveHeader.Remark = pocketShareHeaderModel.Remark

                        objSaveHeader.IsSend = Nothing
                        objSaveHeader.SendBy = Nothing
                        objSaveHeader.SendDate = Nothing

                        objSaveHeader.IsRejected = Nothing
                        objSaveHeader.RejectedBy = Nothing
                        objSaveHeader.RejectedDate = Nothing

                        objSaveHeader.LastUpdateDate = _CurDate
                        objSaveHeader.LastUpdateBy = pocketShareHeaderModel.LastUpdateBy
                    End If


                    '===== SAVE DETAIL ===
                    If pocketShareHeaderModel.PocketShareDetails IsNot Nothing Then
                        Dim objSaveDetail As PocketShareDT = Nothing

                        Dim lastLineNo As Integer? = 0

                        If pocketShareHeaderModel.ROWID > 0 Then
                            lastLineNo = (From elm In dbContext.PocketShareDT
                                          Where elm.OrderID = pocketShareHeaderModel.ROWID
                                          Select elm.LineNumber Order By LineNumber Descending).FirstOrDefault
                        End If


                        For Each _objDetail In pocketShareHeaderModel.PocketShareDetails

                            objSaveDetail = (From elm In dbContext.PocketShareDT
                                             Where elm.ROWID = _objDetail.ROWID
                                             Select elm).FirstOrDefault

                            If objSaveDetail Is Nothing Then
                                '---- insert new detail
                                objSaveDetail = New PocketShareDT
                                objSaveDetail.OrderID = objSaveHeader.ROWID
                                objSaveDetail.OrderNumber = objSaveHeader.OrderNumber


                                objSaveDetail.LineNumber = lastLineNo.GetValueOrDefault(0) + 1
                                lastLineNo += 1

                                objSaveDetail.CompetitorID = _objDetail.CompetitorID
                                objSaveDetail.ActualQty = _objDetail.ActualQty
                                objSaveDetail.IsActive = _objDetail.IsActive
                                objSaveDetail.CreateDate = _CurDate
                                objSaveDetail.CreateBy = _objDetail.CreateBy
                                objSaveDetail.LastUpdateDate = _CurDate
                                objSaveDetail.LastUpdateBy = _objDetail.LastUpdateBy

                                objSaveHeader.PocketShareDT.Add(objSaveDetail)
                            Else
                                '---- update detail
                                objSaveDetail.ActualQty = _objDetail.ActualQty
                                objSaveDetail.IsActive = _objDetail.IsActive
                                objSaveDetail.LastUpdateDate = _CurDate
                                objSaveDetail.LastUpdateBy = _objDetail.LastUpdateBy
                            End If
                        Next
                    End If


                    If _isNewHeader Then
                        dbContext.PocketShareHD.Add(objSaveHeader)
                        dbContext.SaveChanges()
                    Else
                        dbContext.SaveChanges()
                    End If


                    Return Ok(New ActionResultModel(g_status_success, "Success", getPocketShareHeaderById(DeviceID, UserID, objSaveHeader.ROWID)))
                End Using

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function

        Private Function GenerateBillNumber(branchID As Integer) As String
            Dim _Prefix As String = ""
            Dim _OrderNumber As String = ""
            Dim _Year As String
            Dim _Month As String
            Try

                Dim _CurDate As Date = Util.BkkNow

                _Year = _CurDate.ToString("yy", g_culture_en)
                _Month = _CurDate.ToString("MM", g_culture_en)
                _Prefix = "B" & branchID.ToString.PadLeft(3, "0") & "-" & _Year & _Month

                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                                  Pub_UserId, Pub_Password)
                    'B003-2012 ___ 0001 (Running)
                    Dim _ItemObject = (From elm In dbContext.PocketShareHD
                                       Where elm.OrderNumber.StartsWith(_Prefix)
                                       Select elm.OrderNumber.Substring(9, 4)).Max
                    If _ItemObject Is Nothing Then
                        _OrderNumber = _Prefix & "1".PadLeft(4, "0000")
                    Else
                        _ItemObject = Val(_ItemObject) + 1
                        _OrderNumber = _Prefix & _ItemObject.PadLeft(4, "0000")
                    End If

                    Return _OrderNumber

                End Using

            Catch ex As Exception
                Return String.Empty
            End Try
        End Function


        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/saveSendPocketShare")>
        <EnableCors("*", "*", "*")>
        Public Function saveSendPocketShare(DeviceID As String, UserID As Integer, RowIds As List(Of Int64)) As IHttpActionResult
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                Dim _CurDate As Date = Util.BkkNow
                Dim _sender As Users = Nothing
                Dim _aprover As Users = Nothing

                If RowIds Is Nothing OrElse RowIds.Count = 0 Then
                    Return Ok(New ActionResultModel(g_status_fail, "ไม่พบข้อมูลสำหรับการบันทึก", Nothing))
                End If

                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                              Pub_UserId, Pub_Password)
                    For Each _rowId As Int64 In RowIds
                        Dim pocketShareUpdate = (From elm In dbContext.PocketShareHD
                                                 Where elm.ROWID = _rowId
                                                 Select elm).FirstOrDefault
                        If pocketShareUpdate IsNot Nothing Then
                            pocketShareUpdate.IsSend = True
                            pocketShareUpdate.SendDate = _CurDate
                            pocketShareUpdate.SendBy = UserID

                            '--- auto aprrove ---
                            pocketShareUpdate.IsApproved = True
                            pocketShareUpdate.ApprovedDate = _CurDate
                            pocketShareUpdate.ApprovedBy = UserID
                            '--------------------

                            pocketShareUpdate.LastUpdateDate = _CurDate
                            pocketShareUpdate.LastUpdateBy = UserID
                        End If
                    Next

                    dbContext.SaveChanges()

                    '-- get sender/approver
                    _sender = (From elm In dbContext.Users
                               Where elm.UserID = UserID
                               Select elm).FirstOrDefault

                    Dim _userRelate = (From elm In dbContext.UserRelated
                                       Where elm.StaffUserID = UserID
                                       Select elm).FirstOrDefault
                    If _userRelate IsNot Nothing Then
                        _aprover = (From elm In dbContext.Users
                                    Where elm.UserID = _userRelate.SuperUserID
                                    Select elm).FirstOrDefault
                    End If

                End Using

                '================== NOTI ==================
                Dim objSaveNotify As New NotificationModel
                With objSaveNotify
                    .NotifyDate = _CurDate.Date
                    .NotifyType = g_notifyType_APPORVE_POCKETSHARE
                    '.NotifySubject = "Approve Pocket Share"
                    '.NotifyDescription = If(_sender Is Nothing, "", _sender.FirstName) & " ส่งข้อมูล " & RowIds.Count & " รายการ เพื่อรอยืนยันบิล"
                    '.NotifyDescription = "รอยืนยันบิล"

                    .NotifySubject = "New Pocket Share"
                    .NotifyDescription = "มีรายการ Pocket Share ใหม่" & If(_sender Is Nothing, "", " จาก : " & _sender.FirstName)
                    .NotifyNumber = RowIds.Count
                    .NotifyToUserID = If(_aprover Is Nothing, 0, _aprover.UserID)
                    .NotifyToBranchID = 0
                    .NotifyToStoreID = 0
                    .IsRead = False
                    .ReadDate = Nothing
                    .CreateDate = _CurDate
                    .CreateBy = UserID
                    .LastUpdateDate = _CurDate
                    .LastUpdateBy = UserID
                End With

                Dim noti As New NotificationsController
                noti.saveNotification(DeviceID,UserID, objSaveNotify)
                '=============================================

                Return Ok(New ActionResultModel(g_status_success, "Success", Nothing))

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function

        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/deletePocketShare")>
        <EnableCors("*", "*", "*")>
        Public Function deletePocketShare(DeviceID As String, UserID As Integer, RowId As Long) As IHttpActionResult
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                              Pub_UserId, Pub_Password)

                    Dim pocketShareDetailDelete = (From elm In dbContext.PocketShareDT
                                                   Where elm.OrderID = RowId
                                                   Select elm).ToList
                    If pocketShareDetailDelete IsNot Nothing Then
                        dbContext.PocketShareDT.RemoveRange(pocketShareDetailDelete)
                    End If

                    Dim pocketShareDelete = (From elm In dbContext.PocketShareHD
                                             Where elm.ROWID = RowId
                                             Select elm).FirstOrDefault
                    If pocketShareDelete IsNot Nothing Then
                        dbContext.PocketShareHD.Remove(pocketShareDelete)
                        dbContext.SaveChanges()
                    End If

                    dbContext.SaveChanges()
                    Return Ok(New ActionResultModel(g_status_success, "Success", Nothing))

                End Using



            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function

        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/deletePocketShareDetail")>
        <EnableCors("*", "*", "*")>
        Public Function deletePocketShareDetail(DeviceID As String, UserID As Integer, RowId As Long) As IHttpActionResult
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                              Pub_UserId, Pub_Password)

                    Dim pocketShareDetailDelete = (From elm In dbContext.PocketShareDT
                                                   Where elm.ROWID = RowId
                                                   Select elm).ToList
                    If pocketShareDetailDelete IsNot Nothing Then
                        dbContext.PocketShareDT.RemoveRange(pocketShareDetailDelete)
                    End If

                    dbContext.SaveChanges()
                    Return Ok(New ActionResultModel(g_status_success, "Success", Nothing))

                End Using



            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function

        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/getUserListPocketShareForApprove")>
        <EnableCors("*", "*", "*")>
        Public Function getUserListPocketShareForApprove(DeviceID As String, UserID As Integer) As IHttpActionResult
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                              Pub_UserId, Pub_Password)
                    Dim _CurDate As Date = Util.BkkNow
                    '-- get user relate 
                    Dim _userRelate As List(Of Integer) = (From elm In dbContext.UserRelated
                                                           Where elm.SuperUserID = UserID
                                                           Select elm.StaffUserID).ToList

                    Dim getListForApprove = (From psHD In dbContext.PocketShareHD
                                             Let branch = (From br In dbContext.Branches Where br.BranchID = psHD.BranchID Select br).FirstOrDefault
                                             Where psHD.IsSend = True AndAlso _userRelate.Contains(psHD.SendBy) _
                                             And If(psHD.IsActive, True) = True _
                                             And (psHD.SendDate IsNot Nothing AndAlso DbFunctions.TruncateTime(psHD.SendDate) = _CurDate.Date)
                                             Select psHD, branch, store = branch.Stores).ToList

                    'And (psHD.IsApproved Is Nothing OrElse psHD.IsApproved = False) _
                    'And (psHD.IsRejected Is Nothing OrElse psHD.IsRejected = False)

                    Dim groupByUser = (From elm In getListForApprove
                                       Let user = (From usr In dbContext.Users Where usr.UserID = elm.psHD.SendBy).FirstOrDefault
                                       Where user IsNot Nothing
                                       Group By _UserID = user.UserID,
                                           user.UserCode, user.FirstName, user.LastName, user.Title,
                                           _StoreID = elm.store.StoreID, elm.store.StoreName,
                                           elm.branch.BranchID, elm.branch.BranchName
                                           Into Group
                                       Select New ApproveListUserModel With {
                                           .UserID = _UserID,
                                           .UserCode = UserCode,
                                           .FirstName = FirstName,
                                           .LastName = LastName,
                                           .FullName = FirstName & " " & LastName,
                                           .Title = Title,
                                           .StoreID = _StoreID,
                                           .StoreName = StoreName,
                                           .BranchID = BranchID,
                                           .BrandName = BranchName,
                                           .TotalItems = Group.Count
                                           }).ToList

                    Return Ok(New ActionResultModel(g_status_success, "Success", groupByUser))
                End Using

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function

        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/getBillPocketShareForApprove")>
        <EnableCors("*", "*", "*")>
        Public Function getBillPocketShareForApprove(DeviceID As String, ApproverID As Integer, UserID As Integer) As IHttpActionResult
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                              Pub_UserId, Pub_Password)

                    Dim _CurDate As Date = Util.BkkNow

                    '-- get user relate 
                    'Dim _userRelate As List(Of Integer) = (From elm In dbContext.UserRelated
                    '                                       Where elm.SuperUserID = UserID
                    '                                       Select elm.StaffUserID).ToList

                    Dim getListForApprove = (From elm In dbContext.PocketShareHD
                                             Where elm.IsSend = True And elm.SendBy = ApproverID _
                                             And elm.IsSend = True _
                                             And If(elm.IsActive, True) = True _
                                             And (elm.SendDate IsNot Nothing AndAlso DbFunctions.TruncateTime(elm.SendDate) = _CurDate.Date)
                                             Select elm).ToList

                    'And (elm.IsApproved Is Nothing OrElse elm.IsApproved = False) _
                    'And (elm.IsRejected Is Nothing OrElse elm.IsRejected = False)

                    Dim result = (From e In getListForApprove
                                  Select New PocketShareHeaderModel With {
                                    .ROWID = e.ROWID,
                                    .BranchID = e.BranchID,
                                    .OrderNumber = e.OrderNumber,
                                    .OrderDate = e.OrderDate.ToString("dd/MM/yyyy", g_culture_en),
                                    .OrderDescription = e.OrderDescription,
                                    .Refer1Number = e.Refer1Number,
                                    .Refer2Number = e.Refer2Number,
                                    .BuyerName = e.BuyerName,
                                    .BuyerTel = e.BuyerTel,
                                    .IsApproved = e.IsApproved.GetValueOrDefault(False),
                                    .ApprovedBy = e.ApprovedBy.GetValueOrDefault(0),
                                    .ApprovedDate = If(e.ApprovedDate Is Nothing, Nothing, e.ApprovedDate.Value.ToString("dd/MM/yyyy", g_culture_en)),
                                    .IsRejected = e.IsRejected.GetValueOrDefault(False),
                                    .RejectedBy = e.RejectedBy.GetValueOrDefault(0),
                                    .RejectedDate = If(e.RejectedDate Is Nothing, Nothing, e.RejectedDate.Value.ToString("dd/MM/yyyy", g_culture_en)),
                                    .IsSend = e.IsSend.GetValueOrDefault(False),
                                    .SendBy = e.SendBy.GetValueOrDefault(0),
                                    .SendDate = If(e.SendDate Is Nothing, Nothing, e.SendDate.Value.ToString("dd/MM/yyyy", g_culture_en)),
                                    .IsActive = e.IsActive.GetValueOrDefault(False),
                                    .Remark = e.Remark,
                                    .CreateDate = e.CreateDate.Value.ToString("dd/MM/yyyy", g_culture_en),
                                    .CreateBy = e.CreateBy.GetValueOrDefault(0),
                                    .LastUpdateDate = e.LastUpdateDate.Value.ToString("dd/MM/yyyy", g_culture_en),
                                    .LastUpdateBy = e.LastUpdateBy.GetValueOrDefault(0),
                                    .TotalLine = e.PocketShareDT.Count
                                      }).ToList

                    Return Ok(New ActionResultModel(g_status_success, "Success", result))
                End Using



            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function

        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/saveApprovePocketShare")>
        <EnableCors("*", "*", "*")>
        Public Function saveApprovePocketShare(DeviceID As String, UserID As Integer, RowIds As List(Of Int64)) As IHttpActionResult
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                Dim _CurDate As Date = Util.BkkNow
                Dim _UserTask As Integer = 0

                If RowIds Is Nothing OrElse RowIds.Count = 0 Then
                    Return Ok(New ActionResultModel(g_status_fail, "ไม่พบข้อมูลสำหรับการบันทึก", Nothing))
                End If


                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                              Pub_UserId, Pub_Password)

                    For Each _rowId As Int64 In RowIds
                        Dim pricingUpdate = (From elm In dbContext.PocketShareHD
                                             Where elm.ROWID = _rowId
                                             Select elm).FirstOrDefault
                        If pricingUpdate IsNot Nothing Then
                            pricingUpdate.IsApproved = True
                            pricingUpdate.ApprovedDate = _CurDate
                            pricingUpdate.ApprovedBy = UserID

                            pricingUpdate.LastUpdateDate = _CurDate
                            pricingUpdate.LastUpdateBy = UserID
                            _UserTask = pricingUpdate.CreateBy ' --- get user task
                        End If
                    Next

                    dbContext.SaveChanges()

                End Using

                '================== NOTI ==================
                Dim objSaveNotify As New NotificationModel
                With objSaveNotify
                    .NotifyDate = _CurDate.Date
                    .NotifyType = g_notifyType_POCKETSHARE
                    .NotifySubject = "Pocket Share"
                    .NotifyDescription = "บิลผ่านการยืนยันแล้ว"
                    .NotifyNumber = RowIds.Count
                    .NotifyToUserID = _UserTask
                    .NotifyToBranchID = 0
                    .NotifyToStoreID = 0
                    .IsRead = False
                    .ReadDate = Nothing
                    .CreateDate = _CurDate
                    .CreateBy = UserID
                    .LastUpdateDate = _CurDate
                    .LastUpdateBy = UserID
                End With

                Dim noti As New NotificationsController
                noti.saveNotification(DeviceID, UserID, objSaveNotify)
                '=============================================

                Return Ok(New ActionResultModel(g_status_success, "Success", Nothing))

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function


        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/rejectPocketShare")>
        <EnableCors("*", "*", "*")>
        Public Function rejectPocketShare(DeviceID As String, UserID As Integer, RowIds As List(Of Int64)) As IHttpActionResult
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                Dim _CurDate As Date = Util.BkkNow
                Dim _UserTask As Integer = 0
                Dim _ProductText As New StringBuilder
                _ProductText.Append("------------")
                _ProductText.Append(vbNewLine)
                _ProductText.Append("เลขที่ : ")

                If RowIds Is Nothing OrElse RowIds.Count = 0 Then
                    Return Ok(New ActionResultModel(g_status_fail, "ไม่พบข้อมูลสำหรับการบันทึก", Nothing))
                End If


                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                              Pub_UserId, Pub_Password)

                    For Each _rowId As Int64 In RowIds
                        Dim pocketShare = (From elm In dbContext.PocketShareHD
                                           Where elm.ROWID = _rowId
                                           Select elm).FirstOrDefault
                        If pocketShare IsNot Nothing Then
                            pocketShare.IsRejected = True
                            pocketShare.RejectedDate = _CurDate
                            pocketShare.RejectedBy = UserID

                            pocketShare.LastUpdateDate = _CurDate
                            pocketShare.LastUpdateBy = UserID

                            _UserTask = pocketShare.CreateBy ' --- get user task

                            '==== notify get product
                            _ProductText.Append(pocketShare.OrderNumber)
                            If RowIds.IndexOf(_rowId) <> RowIds.Count - 1 Then _ProductText.Append(" , ")
                            '======================

                        End If
                    Next

                    dbContext.SaveChanges()

                End Using

                '================== NOTI ==================
                Dim objSaveNotify As New NotificationModel
                With objSaveNotify
                    .NotifyDate = _CurDate.Date
                    .NotifyType = g_notifyType_REJECT_POCKETSHARE
                    .NotifySubject = "Pocket Share"
                    .NotifyDescription = "บิลถูกปฏิเสธ! " & RowIds.Count.ToString & " รายการ" & vbNewLine & _ProductText.ToString
                    .NotifyNumber = RowIds.Count
                    .NotifyToUserID = _UserTask
                    .NotifyToBranchID = 0
                    .NotifyToStoreID = 0
                    .IsRead = False
                    .ReadDate = Nothing
                    .CreateDate = _CurDate
                    .CreateBy = UserID
                    .LastUpdateDate = _CurDate
                    .LastUpdateBy = UserID
                End With

                Dim noti As New NotificationsController
                noti.saveNotification(DeviceID, UserID, objSaveNotify)
                '=============================================

                Return Ok(New ActionResultModel(g_status_success, "Success", Nothing))

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function


        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/editPocketShareDetail")>
        <EnableCors("*", "*", "*")>
        Public Function editPocketShareDetail(DeviceID As String, UserID As Integer, RowId As Int64, NewQty As Decimal) As IHttpActionResult
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                Dim _CurDate As Date = Util.BkkNow

                If RowId = 0 Then
                    Return Ok(New ActionResultModel(g_status_fail, "ไม่พบข้อมูลสำหรับการบันทึก", Nothing))
                End If

                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                              Pub_UserId, Pub_Password)

                    Dim detailUpdate = (From elm In dbContext.PocketShareDT
                                        Where elm.ROWID = RowId
                                        Select elm).FirstOrDefault

                    If detailUpdate IsNot Nothing Then
                        detailUpdate.ActualQty = NewQty

                        detailUpdate.LastUpdateDate = _CurDate
                        detailUpdate.LastUpdateBy = UserID

                    End If
                    dbContext.SaveChanges()
                End Using

                Return Ok(New ActionResultModel(g_status_success, "Success", Nothing))

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function

    End Class



End Namespace