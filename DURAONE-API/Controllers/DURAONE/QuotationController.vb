﻿Imports System.IO
Imports System.Net
Imports System.Web.Hosting
Imports System.Web.Http
Imports System.Web.Http.Cors
Imports System.Web.Http.Description
Imports System.Security.Cryptography
Imports System.Data.Entity

Namespace Controllers.DURAONE
    Public Class QuotationController
        Inherits ApiController

        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/getQuotationGetHeaders")>
        <EnableCors("*", "*", "*")>
        Public Function getQuotationGetHeaders(DeviceID As String,
                                               UserID As Integer,
                                               BranchID As Integer,
                                               IsPartialOnly As Boolean,
                                               IsCompletedOnly As Boolean,
                                               lastMonths As Integer) As IHttpActionResult

            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                If Not ModelState.IsValid Then
                    Return BadRequest(ModelState)
                End If

                Dim _CurDate As Date = Util.BkkNow.Date
                Dim _minDate As Date

                If lastMonths = 0 Then
                    '---- default
                    _minDate = _CurDate.AddMonths(-1)
                Else
                    _minDate = _CurDate.AddMonths(lastMonths * (-1))
                End If

                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                                Pub_UserId, Pub_Password)

                    Dim objQuotationHeaders = (From elm In dbContext.QuotationHD
                                               Where elm.BranchID = BranchID _
                                                   And elm.OrderDate >= _minDate
                                               Order By elm.CreateDate Descending
                                               Select elm)

                    If IsPartialOnly Then objQuotationHeaders = objQuotationHeaders.Where(Function(elm) If(elm.IsCompleted, False) = False And elm.IsActive = True)
                    If IsCompletedOnly Then objQuotationHeaders = objQuotationHeaders.Where(Function(elm) If(elm.IsCompleted, False) = True And elm.IsActive = True)


                    Dim result = (From e In objQuotationHeaders.ToList
                                  Let _dtCount As Integer? = (From dt In e.QuotationDT Where dt.IsActive = True Select dt).ToList.Count
                                  Let _dtPrice As Integer? = (From dt In e.QuotationDT Where dt.IsActive = True Select total = (dt.UnitPrice * dt.Quantity)).ToList.Sum
                                  Select New QuotationHeaderModel With {
                                    .ROWID = e.ROWID,
                                    .BranchID = e.BranchID,
                                    .OrderNumber = e.OrderNumber,
                                    .OrderDate = If(e.OrderDate Is Nothing, "", e.OrderDate.Value.ToString("dd/MM/yyyy", g_culture_en)),
                                    .OrderDescription = e.OrderDescription,
                                    .Refer1Number = e.Refer1Number,
                                    .Refer2Number = e.Refer2Number,
                                    .BuyerName = e.BuyerName,
                                    .BuyerTel = e.BuyerTel,
                                    .BuyerLineID = e.BuyerLineID,
                                    .IsActive = e.IsActive.GetValueOrDefault(False),
                                    .IsCompleted = e.IsCompleted.GetValueOrDefault(False),
                                    .CancellationReasons = e.CancellationReasons,
                                    .Remark = e.Remark,
                                    .CreateDate = e.CreateDate.Value.ToString("dd/MM/yyyy", g_culture_en),
                                    .CreateBy = e.CreateBy.GetValueOrDefault(0),
                                    .LastUpdateDate = e.LastUpdateDate.Value.ToString("dd/MM/yyyy", g_culture_en),
                                    .LastUpdateBy = e.LastUpdateBy.GetValueOrDefault(0),
                                    .TotalItem = _dtCount.GetValueOrDefault(0),
                                    .TotalPrice = _dtPrice.GetValueOrDefault(0)
                        }).ToList

                    Return Ok(New ActionResultModel(g_status_success, "Success", result))

                End Using

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function


        Private Function getQuotationGetHeaderById(DeviceID As String, UserID As Integer, OrderID As Int64) As QuotationHeaderModel
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Throw New Exception(msgChecked.message)
                End If
                '=====================================



                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                                Pub_UserId, Pub_Password)

                    Dim objQuotationHeaders = (From elm In dbContext.QuotationHD
                                               Where elm.IsActive = True _
                                                   And elm.ROWID = OrderID
                                               Order By elm.CreateDate Descending
                                               Select elm).ToList

                    Dim result = (From e In objQuotationHeaders
                                  Let _dtCount As Integer? = (From dt In e.QuotationDT Where dt.IsActive = True Select dt).ToList.Count
                                  Let _dtPrice As Integer? = (From dt In e.QuotationDT Where dt.IsActive = True Select total = (dt.UnitPrice * dt.Quantity)).ToList.Sum
                                  Select New QuotationHeaderModel With {
                                    .ROWID = e.ROWID,
                                    .BranchID = e.BranchID,
                                    .OrderNumber = e.OrderNumber,
                                    .OrderDate = If(e.OrderDate Is Nothing, "", e.OrderDate.Value.ToString("dd/MM/yyyy", g_culture_en)),
                                    .OrderDescription = e.OrderDescription,
                                    .Refer1Number = e.Refer1Number,
                                    .Refer2Number = e.Refer2Number,
                                    .BuyerName = e.BuyerName,
                                    .BuyerTel = e.BuyerTel,
                                    .BuyerLineID = e.BuyerLineID,
                                    .IsActive = e.IsActive.GetValueOrDefault(False),
                                    .IsCompleted = e.IsCompleted.GetValueOrDefault(False),
                                    .CancellationReasons = e.CancellationReasons,
                                    .Remark = e.Remark,
                                    .CreateDate = e.CreateDate.Value.ToString("dd/MM/yyyy", g_culture_en),
                                    .CreateBy = e.CreateBy.GetValueOrDefault(0),
                                    .LastUpdateDate = e.LastUpdateDate.Value.ToString("dd/MM/yyyy", g_culture_en),
                                    .LastUpdateBy = e.LastUpdateBy.GetValueOrDefault(0),
                                    .TotalItem = _dtCount.GetValueOrDefault(0),
                                    .TotalPrice = _dtPrice.GetValueOrDefault(0)
                        }).FirstOrDefault

                    Return result

                End Using

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/getQuotationDetails")>
        <EnableCors("*", "*", "*")>
        Public Function getQuotationDetails(DeviceID As String, UserID As Integer, OrderID As Int64)
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                Dim _CurDate As Date = Util.BkkNow

                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                                  Pub_UserId, Pub_Password)

                    Dim getNotifications = (From qt In dbContext.QuotationDT
                                            Let prd = (From p In dbContext.Competitors Where p.ROWID = qt.CompetitorID Select p).FirstOrDefault
                                            Where prd IsNot Nothing _
                                                And qt.IsActive = True _
                                                And qt.OrderID = OrderID
                                            Order By qt.LineNumber
                                            Select qt, prd).ToList

                    Dim result = (From e In getNotifications
                                  Select New QuotationDetailModel With {
                            .ROWID = e.qt.ROWID,
                            .OrderID = e.qt.OrderID,
                            .OrderNumber = e.qt.OrderNumber,
                            .LineNumber = e.qt.LineNumber,
                            .CompetitorID = e.qt.CompetitorID,
                            .ProductNo = e.prd.ProductNo,
                            .ProductName = e.prd.ProductName,
                            .UnitCode = e.prd.UnitCode,
                            .Barcode = e.prd.Barcode,
                            .UnitPrice = e.qt.UnitPrice,
                            .Quantity = e.qt.Quantity,
                            .IsActive = e.qt.IsActive.GetValueOrDefault(0),
                            .CreateBy = e.qt.CreateBy,
                            .CreateDate = If(e.qt.CreateDate Is Nothing, Nothing, e.qt.CreateDate.Value.ToString("dd/MM/yyyy", g_culture_en)),
                            .LastUpdateBy = e.qt.LastUpdateBy,
                            .LastUpdateDate = If(e.qt.LastUpdateDate Is Nothing, Nothing, e.qt.LastUpdateDate.Value.ToString("dd/MM/yyyy", g_culture_en))
                        }).ToList

                    Return Ok(New ActionResultModel(g_status_success, "Success", result))
                End Using

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try

        End Function


        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/saveQuotation")>
        <EnableCors("*", "*", "*")>
        Public Function saveQuotation(DeviceID As String, UserID As Integer, QuotationHeaderModel As QuotationHeaderModel)
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                Dim _CurDate As Date = Util.BkkNow
                Dim _isNewHeader As Boolean = False

                If QuotationHeaderModel Is Nothing Then
                    Return Ok(New ActionResultModel(g_status_fail, "Not found Pocket Share Header parameter!", Nothing))
                End If

                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                                  Pub_UserId, Pub_Password)
                    '===== SAVE HEADER ===
                    Dim objSaveHeader As QuotationHD = Nothing
                    objSaveHeader = (From elm In dbContext.QuotationHD
                                     Where elm.ROWID = QuotationHeaderModel.ROWID
                                     Select elm).FirstOrDefault
                    If objSaveHeader Is Nothing Then
                        '---- insert new header
                        _isNewHeader = True
                        objSaveHeader = New QuotationHD
                        objSaveHeader.BranchID = QuotationHeaderModel.BranchID
                        objSaveHeader.OrderNumber = Me.GenerateQuotationNumber(QuotationHeaderModel.BranchID) '--- Auto Generate
                        objSaveHeader.OrderDate = Date.ParseExact(QuotationHeaderModel.OrderDate, "dd/MM/yyyy", g_culture_en).Date
                        objSaveHeader.OrderDescription = QuotationHeaderModel.OrderDescription
                        objSaveHeader.Refer1Number = QuotationHeaderModel.Refer1Number
                        objSaveHeader.Refer2Number = QuotationHeaderModel.Refer2Number

                        objSaveHeader.BuyerName = QuotationHeaderModel.BuyerName
                        objSaveHeader.BuyerTel = QuotationHeaderModel.BuyerTel
                        objSaveHeader.BuyerLineID = QuotationHeaderModel.BuyerLineID

                        objSaveHeader.IsCompleted = Nothing
                        objSaveHeader.CompletedDate = Nothing
                        objSaveHeader.CompletedBy = Nothing

                        objSaveHeader.IsActive = True
                        objSaveHeader.CancellationReasons = Nothing
                        objSaveHeader.CancelledDate = Nothing
                        objSaveHeader.CancelledBy = Nothing
                        objSaveHeader.Remark = QuotationHeaderModel.Remark

                        objSaveHeader.CreateDate = _CurDate
                        objSaveHeader.CreateBy = QuotationHeaderModel.CreateBy
                        objSaveHeader.LastUpdateDate = _CurDate
                        objSaveHeader.LastUpdateBy = QuotationHeaderModel.LastUpdateBy

                    Else
                        '---- update header
                        objSaveHeader.OrderDate = Date.ParseExact(QuotationHeaderModel.OrderDate, "dd/MM/yyyy", g_culture_en).Date
                        objSaveHeader.Refer1Number = QuotationHeaderModel.Refer1Number
                        objSaveHeader.Refer2Number = QuotationHeaderModel.Refer2Number

                        objSaveHeader.BuyerName = QuotationHeaderModel.BuyerName
                        objSaveHeader.BuyerTel = QuotationHeaderModel.BuyerTel
                        objSaveHeader.BuyerLineID = QuotationHeaderModel.BuyerLineID

                        'objSaveHeader.IsActive = QuotationHeaderModel.IsActive
                        'objSaveHeader.IsCompleted = QuotationHeaderModel.IsCompleted
                        'objSaveHeader.CompetedDate = QuotationHeaderModel.CompetedDate
                        'objSaveHeader.CancellationReasons = QuotationHeaderModel.CancellationReasons
                        objSaveHeader.Remark = QuotationHeaderModel.Remark

                        objSaveHeader.LastUpdateDate = _CurDate
                        objSaveHeader.LastUpdateBy = QuotationHeaderModel.LastUpdateBy
                    End If


                    '===== SAVE DETAIL ===
                    If QuotationHeaderModel.QuotationDetailModel IsNot Nothing Then
                        Dim objSaveDetail As QuotationDT = Nothing

                        Dim lastLineNo As Integer? = (From elm In dbContext.QuotationDT
                                                      Where elm.OrderID = QuotationHeaderModel.ROWID
                                                      Select elm.LineNumber
                                                      Order By LineNumber Descending).FirstOrDefault

                        For Each _objDetail In QuotationHeaderModel.QuotationDetailModel

                            objSaveDetail = (From elm In dbContext.QuotationDT
                                             Where elm.ROWID = _objDetail.ROWID
                                             Select elm).FirstOrDefault

                            If objSaveDetail Is Nothing Then
                                '---- insert new detail
                                objSaveDetail = New QuotationDT
                                objSaveDetail.OrderID = objSaveHeader.ROWID
                                objSaveDetail.OrderNumber = objSaveHeader.OrderNumber


                                objSaveDetail.LineNumber = lastLineNo.GetValueOrDefault(0) + 1
                                lastLineNo += 1

                                objSaveDetail.CompetitorID = _objDetail.CompetitorID
                                objSaveDetail.Quantity = _objDetail.Quantity
                                objSaveDetail.UnitPrice = _objDetail.UnitPrice
                                objSaveDetail.PriceDescription = _objDetail.PriceDescription
                                objSaveDetail.IsActive = _objDetail.IsActive
                                objSaveDetail.CreateDate = _CurDate
                                objSaveDetail.CreateBy = _objDetail.CreateBy
                                objSaveDetail.LastUpdateDate = _CurDate
                                objSaveDetail.LastUpdateBy = _objDetail.LastUpdateBy

                                objSaveHeader.QuotationDT.Add(objSaveDetail)
                            Else
                                '---- update detail
                                objSaveDetail.Quantity = _objDetail.Quantity
                                objSaveDetail.UnitPrice = _objDetail.UnitPrice
                                objSaveDetail.PriceDescription = _objDetail.PriceDescription

                                objSaveDetail.IsActive = _objDetail.IsActive
                                objSaveDetail.LastUpdateDate = _CurDate
                                objSaveDetail.LastUpdateBy = _objDetail.LastUpdateBy
                            End If
                        Next
                    End If


                    If _isNewHeader Then
                        dbContext.QuotationHD.Add(objSaveHeader)
                        dbContext.SaveChanges()
                    Else
                        dbContext.SaveChanges()
                    End If


                    Return Ok(New ActionResultModel(g_status_success, "Success", getQuotationGetHeaderById(DeviceID, UserID, objSaveHeader.ROWID)))
                End Using

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function

        Private Function GenerateQuotationNumber(branchID As Integer) As String
            Dim _Prefix As String = ""
            Dim _OrderNumber As String = ""
            Dim _Year As String
            Dim _Month As String
            Try

                Dim _CurDate As Date = Util.BkkNow

                _Year = _CurDate.ToString("yy", g_culture_en)
                _Month = _CurDate.ToString("MM", g_culture_en)
                _Prefix = "Q" & branchID.ToString.PadLeft(3, "0") & "-" & _Year & _Month

                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                                  Pub_UserId, Pub_Password)
                    'B003-2012 ___ 0001 (Running)
                    Dim _ItemObject = (From elm In dbContext.QuotationHD
                                       Where elm.OrderNumber.StartsWith(_Prefix)
                                       Select elm.OrderNumber.Substring(9, 4)).Max
                    If _ItemObject Is Nothing Then
                        _OrderNumber = _Prefix & "1".PadLeft(4, "0000")
                    Else
                        _ItemObject = Val(_ItemObject) + 1
                        _OrderNumber = _Prefix & _ItemObject.PadLeft(4, "0000")
                    End If

                    Return _OrderNumber

                End Using

            Catch ex As Exception
                Return String.Empty
            End Try
        End Function


        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/editQuotationDetail")>
        <EnableCors("*", "*", "*")>
        Public Function editQuotationDetail(DeviceID As String, UserID As Integer, RowId As Int64, NewQty As Decimal) As IHttpActionResult
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                Dim _CurDate As Date = Util.BkkNow

                If RowId = 0 Then
                    Return Ok(New ActionResultModel(g_status_fail, "ไม่พบข้อมูลสำหรับการบันทึก", Nothing))
                End If

                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                              Pub_UserId, Pub_Password)

                    Dim detailUpdate = (From elm In dbContext.QuotationDT
                                        Where elm.ROWID = RowId
                                        Select elm).FirstOrDefault

                    If detailUpdate IsNot Nothing Then
                        detailUpdate.Quantity = NewQty

                        detailUpdate.LastUpdateDate = _CurDate
                        detailUpdate.LastUpdateBy = UserID

                    End If
                    dbContext.SaveChanges()
                End Using

                Return Ok(New ActionResultModel(g_status_success, "Success", Nothing))

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function

        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/deleteQuotationDetail")>
        <EnableCors("*", "*", "*")>
        Public Function deleteQuotationDetail(DeviceID As String, UserID As Integer, RowId As Long) As IHttpActionResult
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                              Pub_UserId, Pub_Password)

                    Dim objDetailDelete = (From elm In dbContext.QuotationDT
                                           Where elm.ROWID = RowId
                                           Select elm).ToList
                    If objDetailDelete IsNot Nothing Then
                        dbContext.QuotationDT.RemoveRange(objDetailDelete)
                    End If

                    dbContext.SaveChanges()
                    Return Ok(New ActionResultModel(g_status_success, "Success", Nothing))

                End Using



            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function


        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/saveQuotationCompleted")>
        <EnableCors("*", "*", "*")>
        Public Function saveQuotationCompleted(DeviceID As String, UserID As Integer, RowIds As List(Of Int64)) As IHttpActionResult
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                Dim _CurDate As Date = Util.BkkNow
                Dim _UserTask As Integer = 0
                Dim _RowHD As Int64 = 0

                If RowIds Is Nothing OrElse RowIds.Count = 0 Then
                    Return Ok(New ActionResultModel(g_status_fail, "ไม่พบข้อมูลสำหรับการบันทึก", Nothing))
                End If

                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                              Pub_UserId, Pub_Password)

                    For Each _rowId As Int64 In RowIds
                        Dim objUpdate = (From elm In dbContext.QuotationDT
                                         Where elm.ROWID = _rowId
                                         Select elm).FirstOrDefault
                        If objUpdate IsNot Nothing Then
                            objUpdate.IsCompleted = True
                            objUpdate.CompletedDate = _CurDate
                            objUpdate.CompletedBy = UserID

                            objUpdate.LastUpdateDate = _CurDate
                            objUpdate.LastUpdateBy = UserID

                            _UserTask = objUpdate.CreateBy ' --- get user task
                            _RowHD = objUpdate.OrderID '-- header ID
                        End If
                    Next

                    Dim objUpdateHeader = (From elm In dbContext.QuotationHD
                                           Where elm.ROWID = _RowHD
                                           Select elm).FirstOrDefault
                    If objUpdateHeader IsNot Nothing Then
                        objUpdateHeader.IsCompleted = True
                        objUpdateHeader.CompletedDate = _CurDate
                        objUpdateHeader.CompletedBy = _UserTask

                        objUpdateHeader.LastUpdateDate = _CurDate
                        objUpdateHeader.LastUpdateBy = UserID
                    End If

                    dbContext.SaveChanges()

                End Using

                Return Ok(New ActionResultModel(g_status_success, "Success", Nothing))

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function


        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/saveQuotationCancellation")>
        <EnableCors("*", "*", "*")>
        Public Function saveQuotationCancellation(DeviceID As String,
                                                  UserID As Integer,
                                                  RowId As Int64,
                                                  Reason As String) As IHttpActionResult
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                Dim _CurDate As Date = Util.BkkNow

                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                              Pub_UserId, Pub_Password)


                    Dim objUpdate = (From elm In dbContext.QuotationHD
                                     Where elm.ROWID = RowId
                                     Select elm).FirstOrDefault
                    If objUpdate IsNot Nothing Then
                        objUpdate.IsActive = False
                        objUpdate.CancelledDate = _CurDate
                        objUpdate.CancelledBy = UserID
                        objUpdate.CancellationReasons = Reason

                        objUpdate.LastUpdateDate = _CurDate
                        objUpdate.LastUpdateBy = UserID
                    End If


                    dbContext.SaveChanges()

                End Using

                Return Ok(New ActionResultModel(g_status_success, "Success", Nothing))

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function

    End Class

End Namespace