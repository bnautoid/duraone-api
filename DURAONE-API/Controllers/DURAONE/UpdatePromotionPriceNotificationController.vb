﻿Imports System.IO
Imports System.Net
Imports System.Web.Hosting
Imports System.Web.Http
Imports System.Web.Http.Cors
Imports System.Web.Http.Description
Imports System.Security.Cryptography
Imports System.Data.Entity
Imports System.Data.Entity.Core.Objects

Namespace Controllers.DURAONE
    Public Class UpdatePromotionPriceNotificationController
        Inherits ApiController


        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/GetPromotionPriceNotificationSummary")>
        <EnableCors("*", "*", "*")>
        Public Function GetPromotionPriceNotificationSummary(DeviceID As String,
                                                       UserID As Integer,
                                                       BranchId As Integer) As IHttpActionResult
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                If Not ModelState.IsValid Then
                    Return BadRequest(ModelState)
                End If

                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                               Pub_UserId, Pub_Password)

                    Dim objBrach = (From elm In dbContext.Branches
                                    Where elm.BranchID = BranchId
                                    Select elm).FirstOrDefault
                    If objBrach Is Nothing Then
                        Return Ok(New ActionResultModel(g_status_fail, "ไม่พบข้อมูลสาขา (ID: " & BranchId.ToString & ")", Nothing))
                    End If

                    Dim NotiSummary = (From noti In dbContext.UpdatePricingNotification
                                       Join prod In dbContext.Competitors On prod.ROWID Equals noti.CompetitorID
                                       Where prod IsNot Nothing _
                                             And noti.BranchID = BranchId _
                                             And If(noti.IsActive, False) = True _
                                             And noti.CreateDate IsNot Nothing
                                       Group By CreatedDate = EntityFunctions.TruncateTime(noti.CreateDate) Into Group
                                       Select CreatedDate, TotalNoti = Group.Count).AsEnumerable _
                                             .Select(Function(elm) New UpdatePricingNotificationSummaryModel With {
                                             .BranchID = BranchId,
                                             .CreateDate = If(elm.CreatedDate Is Nothing, Nothing, elm.CreatedDate.Value.ToString("dd/MM/yyyy", g_culture_en)),
                                             .TotalNotify = elm.TotalNoti}).ToList

                    Return Ok(New ActionResultModel(g_status_success, "success", NotiSummary))
                End Using

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function


        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/GetPromotionPriceNotification")>
        <EnableCors("*", "*", "*")>
        Public Function GetPromotionPriceNotification(DeviceID As String,
                                                       UserID As Integer,
                                                       BranchId As Integer,
                                                       NotifyDate As String) As IHttpActionResult
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                If Not ModelState.IsValid Then
                    Return BadRequest(ModelState)
                End If



                'Dim res = Me.localGetProductCompetitor(UserID, StoreID, Province, Barcode, ProductId)
                'Return Ok(res)


                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                               Pub_UserId, Pub_Password)

                    Dim _NotifyCreatedDate As Date = Util.convertStringToDate(NotifyDate)

                    Dim objBrach = (From elm In dbContext.Branches
                                    Where elm.BranchID = BranchId
                                    Select elm).FirstOrDefault
                    If objBrach Is Nothing Then
                        Return Ok(New ActionResultModel(g_status_fail, "ไม่พบข้อมูลสาขา (ID: " & BranchId.ToString & ")", Nothing))
                    End If

                    Dim Notifications = (From noti In dbContext.UpdatePricingNotification
                                         Join prod In dbContext.Competitors On prod.ROWID Equals noti.CompetitorID
                                         Where noti.BranchID = BranchId _
                                             And EntityFunctions.TruncateTime(noti.CreateDate) = _NotifyCreatedDate _
                                             And If(noti.IsActive, False) = True
                                         Select noti, prod).ToList

                    Dim objProductController As New DURAONE.ProductController

                    Dim objPromotionNotifies As New List(Of UpdatePricingNotificationModel)

                    For Each _data In Notifications
                        Dim ProductResult = objProductController.localGetProductCompetitor(UserID, objBrach.StoreID, objBrach.Province, _data.prod.Barcode, _data.prod.ROWID)

                        If ProductResult.resultcode = g_status_success Then
                            If ProductResult.data Is Nothing Then
                                Return Ok(New ActionResultModel(g_status_fail, "ไม่พบข้อมูลบาร์โค้ดสินค้า " & _data.prod.Barcode, Nothing))
                            End If

                            'update Calcualtion Barcode (ใช้สำหรับ re-check ค่ากับการคำนวณ เพราะ Barcode จาก Compet อาจะไม่ตรงกับ Material ตอน return)
                            Dim objCompetitorModel As CompetitorModel = ProductResult.data
                            'objCompetitorModel.CalculationBarcode = Barcode


                            Dim objPromotionNotify = New UpdatePricingNotificationModel With {
                                      .ROWID = _data.noti.ROWID,
                                      .BranchID = BranchId,
                                                           _
                                      .CompetitorID = _data.prod.ROWID,
                                      .ProductNo = _data.prod.ProductNo,
                                      .ProductName = _data.prod.ProductName,
                                      .UnitCode = _data.prod.UnitCode,
                                      .Barcode = _data.prod.Barcode,
                                                                    _
                                      .NormalPrice = objCompetitorModel.NormalPrice,
                                      .RetailPrice = objCompetitorModel.LastNormalPrice,
                                      .PromotionPrice = objCompetitorModel.PromotionPrice,
                                      .ExpireDate = objCompetitorModel.PromotionDate,
                                      .NewPromotionPrice = If(_data.noti.NewPromotionPrice, 0),
                                                                                               _
                                      .Action = _data.noti.Action,
                                      .ActionCode = _data.noti.ActionCode,
                                      .Subsidized = _data.noti.Subsidized.GetValueOrDefault(0),
                                                                                               _
                                      .EffectiveDate = If(_data.noti.EffectiveDate Is Nothing, Nothing, _data.noti.EffectiveDate.Value.ToString("dd/MM/yyyy", g_culture_en)),
                                      .NewExpireDate = If(_data.noti.NewExpireDate Is Nothing, Nothing, _data.noti.NewExpireDate.Value.ToString("dd/MM/yyyy", g_culture_en)),
                                                                                                                                                                             _
                                      .IsAddToCart = If(_data.noti.IsAddToCart, False),
                                      .AddToCartDate = If(_data.noti.AddToCartDate Is Nothing, Nothing, _data.noti.AddToCartDate.Value.ToString("dd/MM/yyyy", g_culture_en)),
                                      .AddToCartBy = If(_data.noti.AddToCartBy, 0),
                                      .AddToCartRowId = If(_data.noti.AddToCartRowId, 0),
                                                                                         _
                                      .CreateBy = If(_data.noti.CreateBy, 0),
                                      .CreateDate = If(_data.noti.CreateDate Is Nothing, Nothing, _data.noti.CreateDate.Value.ToString("dd/MM/yyyy", g_culture_en)),
                                      .LastUpdateBy = If(_data.noti.LastUpdateBy, 0),
                                      .LastUpdateDate = If(_data.noti.LastUpdateDate Is Nothing, Nothing, _data.noti.LastUpdateDate.Value.ToString("dd/MM/yyyy", g_culture_en))
                                      }


                            objPromotionNotifies.Add(objPromotionNotify)
                        End If

                    Next

                    Return Ok(New ActionResultModel(g_status_success, "success", objPromotionNotifies))
                End Using



            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function


        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/addPromotionToCartFromNotification")>
        <EnableCors("*", "*", "*")>
        Public Function addPromotionToCartFromNotification(objs As List(Of SaveUpdatePricingModel)) As IHttpActionResult
            Try
                If objs Is Nothing OrElse objs.Count = 0 Then
                    Return Ok(New ActionResultModel(g_status_fail, "ไม่พบข้อมูลสำหรับการบันทึก", Nothing))
                End If

                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(objs.FirstOrDefault.DeviceID, objs.FirstOrDefault.UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================
                If Not ModelState.IsValid Then
                    Return BadRequest(ModelState)
                End If

                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                               Pub_UserId, Pub_Password)

                    Dim objUpdatePricingController As New UpdatePricingController
                    Dim objUpdateNormalPriceController As New UpdateNormalPriceController

                    Dim resultSaved As ActionResultModel = Nothing


                    For Each _data In objs

                        If _data.NotificationActionCode = Util.g_noti_action_promotion Then
                            '-- promotion price
                            resultSaved = objUpdatePricingController.savePromotionPriceToDB(_data)

                        ElseIf _data.NotificationActionCode = Util.g_noti_action_retail Then
                            '-- normal price
                            resultSaved = objUpdateNormalPriceController.saveNormalPriceToDB(_data)

                        Else
                            Return Ok(New ActionResultModel(g_status_fail, "Notification Action Code required!", Nothing))

                        End If


                        If resultSaved.resultcode <> g_status_success Then
                            Return Ok(resultSaved)
                        End If

                        '-- update promotion notification
                        Dim objUpdateNoti = (From elm In dbContext.UpdatePricingNotification
                                             Where elm.ROWID = _data.ReferNotificationRowId
                                             Select elm).FirstOrDefault
                        If objUpdateNoti IsNot Nothing Then
                            objUpdateNoti.IsAddToCart = True
                            objUpdateNoti.AddToCartDate = Util.BkkNow
                            objUpdateNoti.AddToCartBy = _data.UserID

                            'Dim objNewPrice = CType(resultSaved.data, UpdatePricing)
                            objUpdateNoti.AddToCartRowId = If(resultSaved.data Is Nothing, 0, resultSaved.data.ROWID)
                            dbContext.SaveChanges()
                        End If

                    Next

                    Return Ok(New ActionResultModel(g_status_success, "success", Nothing))
                End Using

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function

    End Class

End Namespace