﻿Imports System.IO
Imports System.Net
Imports System.Web.Hosting
Imports System.Web.Http
Imports System.Web.Http.Cors
Imports System.Web.Http.Description
Imports System.Security.Cryptography
Imports System.Data.Entity
Imports System.Data.Entity.Core.Objects

Namespace Controllers.DURAONE
    Public Class SmartPocketShareReportController
        Inherits ApiController

        Private Const mDURAONEBrand As String = "DURA ONE"

        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/GetSPRHeaderBySM")>
        <EnableCors("*", "*", "*")>
        Public Function GetSPRHeaderBySM(DeviceID As String,
                                         UserID As Integer,
                                         Year As Integer,
                                         Month As Integer,
                                         SMCode As String) As IHttpActionResult
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                If Not ModelState.IsValid Then
                    Return BadRequest(ModelState)
                End If

                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                               Pub_UserId, Pub_Password)

                    dbContext.Database.CommandTimeout = 180

                    'elm.NetValueTHB > 0
                    Dim objPocketShares = (From elm In dbContext.SmartPocketShareReport
                                           Where elm.Year = Year And elm.Month = Month _
                                             And elm.Region7SM = SMCode
                                           Group By YearValue = elm.Year,
                                                    MonthValue = elm.Month,
                                                    Region7SM = elm.Region7SM,
                                                    ProductGsroup4SubCate = elm.Group4SubCate
                                                                   Into DURANetValue = Sum(If(elm.Brand = mDURAONEBrand, elm.NetAmount, 0)),
                                                    OTHERNetValue = Sum(If(elm.Brand = mDURAONEBrand, 0, elm.NetValueTHB))
                                           Select New SPRHeaderModel With {
                                               .YearValue = YearValue,
                                               .MonthValue = MonthValue,
                                               .Region7SM = Region7SM,
                                               .ProductGroup4SubCate = ProductGsroup4SubCate,
                                                                                             _
                                               .DURANetValue = DURANetValue,
                                               .OTHERNetValue = OTHERNetValue,
                                                                              _
                                               .DURAPercentage = If((DURANetValue + OTHERNetValue) = 0, 0, (DURANetValue / (DURANetValue + OTHERNetValue)) * 100),
                                               .OTHERPercentage = If((DURANetValue + OTHERNetValue) = 0, 0, (OTHERNetValue / (DURANetValue + OTHERNetValue)) * 100),
                                                                                                                                                                    _
                                               .BranchCustCode = Nothing,
                                               .BranchID = 0,
                                               .BranchName = Nothing
                                               }).ToList

                    'Into DURANetValue = Sum(If(elm.Brand = mDURAONEBrand, elm.NetAmount, 0)),
                    '                                OTHERNetValue = Sum(If(elm.Brand = mDURAONEBrand, 0, elm.NetValueTHB))

                    Return Ok(New ActionResultModel(g_status_success, "success", objPocketShares))
                End Using

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.ToString, Nothing))
            End Try
        End Function

        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/GetSPRHeaderByBranch")>
        <EnableCors("*", "*", "*")>
        Public Function GetSPRHeaderByBranch(DeviceID As String,
                                             UserID As Integer,
                                             Year As Integer,
                                             Month As Integer,
                                             SMCode As String,
                                             BranchId As Integer) As IHttpActionResult
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                If Not ModelState.IsValid Then
                    Return BadRequest(ModelState)
                End If

                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                               Pub_UserId, Pub_Password)

                    dbContext.Database.CommandTimeout = 180

                    Dim _branchCustCode As String = Nothing
                    Dim _branchId As Integer = 0
                    Dim _branchName As String = Nothing

                    Dim objBrach = (From elm In dbContext.Branches
                                    Where elm.BranchID = BranchId
                                    Select elm).FirstOrDefault
                    If BranchId <> 0 And objBrach Is Nothing Then
                        Return Ok(New ActionResultModel(g_status_fail, "ไม่พบข้อมูลสาขา (ID: " & BranchId.ToString & ")", Nothing))
                    Else
                        _branchId = objBrach.BranchID
                        _branchName = objBrach.BranchName
                        _branchCustCode = objBrach.CustCode
                    End If

                    'And elm.NetValueTHB > 0
                    Dim objPocketShares = (From elm In dbContext.SmartPocketShareReport
                                           Where elm.Year = Year And elm.Month = Month _
                                               And elm.Region7SM = SMCode _
                                             And elm.CustomerCode = _branchCustCode
                                           Group By YearValue = elm.Year,
                                                    MonthValue = elm.Month,
                                                    Region7SM = elm.Region7SM,
                                                    BranchCustCode = elm.CustomerCode,
                                                    ProductGsroup4SubCate = elm.Group4SubCate
                                               Into DURANetValue = Sum(If(elm.Brand = mDURAONEBrand, elm.NetAmount, 0)),
                                                    OTHERNetValue = Sum(If(elm.Brand = mDURAONEBrand, 0, elm.NetValueTHB))
                                           Select New SPRHeaderModel With {
                                               .YearValue = YearValue,
                                               .MonthValue = MonthValue,
                                               .Region7SM = Region7SM,
                                               .ProductGroup4SubCate = ProductGsroup4SubCate,
                                                                                             _
                                               .DURANetValue = DURANetValue,
                                               .OTHERNetValue = OTHERNetValue,
                                                                              _
                                               .DURAPercentage = If((DURANetValue + OTHERNetValue) = 0, 0, (DURANetValue / (DURANetValue + OTHERNetValue)) * 100),
                                               .OTHERPercentage = If((DURANetValue + OTHERNetValue) = 0, 0, (OTHERNetValue / (DURANetValue + OTHERNetValue)) * 100),
                                                                                                                                                                    _
                                               .BranchCustCode = _branchCustCode,
                                               .BranchID = _branchId,
                                               .BranchName = _branchName
                                               }).ToList


                    Return Ok(New ActionResultModel(g_status_success, "success", objPocketShares))
                End Using

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function



        '######################################################################
        '########### DETIAL BRAND VALUE/PERCENT FOR PIE CHART #################
        '######################################################################

        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/GetSPRDetailBySM")>
        <EnableCors("*", "*", "*")>
        Public Function GetSPRDetailBySM(DeviceID As String,
                                 UserID As Integer,
                                 Year As Integer,
                                 Month As Integer,
                                 SMCode As String,
                                 Group4SubCate As String) As IHttpActionResult
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                If Not ModelState.IsValid Then
                    Return BadRequest(ModelState)
                End If

                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                               Pub_UserId, Pub_Password)

                    dbContext.Database.CommandTimeout = 180
                    'elm.NetValueTHB > 0

                    Dim _totalNetValue As Decimal? = (From elm In dbContext.SmartPocketShareReport
                                                      Where elm.Year = Year And elm.Month = Month _
                                             And elm.Region7SM = SMCode _
                                             And elm.Group4SubCate = Group4SubCate
                                                      Select NetValue = If(elm.Brand = mDURAONEBrand, If(elm.NetAmount, 0), If(elm.NetValueTHB, 0))).ToList.Sum

                    Dim objPocketShareDetails = (From elm In dbContext.SmartPocketShareReport
                                                 Where elm.Year = Year And elm.Month = Month _
                                             And elm.Region7SM = SMCode _
                                             And elm.Group4SubCate = Group4SubCate
                                                 Group By YearValue = elm.Year,
                                                    MonthValue = elm.Month,
                                                    Region7SM = elm.Region7SM,
                                                    ProductGsroup4SubCate = elm.Group4SubCate,
                                                    Brands = elm.Brand
                    Into NetValue = Sum(If(elm.Brand = mDURAONEBrand, If(elm.NetAmount, 0), If(elm.NetValueTHB, 0)))
                                                 Select New SPRDetailModel With {
                                               .YearValue = YearValue,
                                               .MonthValue = MonthValue,
                                               .Region7SM = Region7SM,
                                               .ProductGroup4SubCate = ProductGsroup4SubCate,
                                                                                             _
                                               .NetValue = NetValue,
                                               .Percentage = (NetValue / (If(_totalNetValue, 0))) * 100,
                                                                                                        _
                                               .BranchCustCode = Nothing,
                                               .BranchID = 0,
                                               .BranchName = Nothing, _
                                                                      _
                                                .Brand = Brands
                                               }).ToList

                    '--- update DURA ONE to index 0 --
                    Dim findDuraOne = (From elm In objPocketShareDetails
                                       Where elm.Brand.Trim.ToUpper = mDURAONEBrand
                                       Select elm).FirstOrDefault
                    If findDuraOne IsNot Nothing Then
                        objPocketShareDetails.Remove(findDuraOne)
                        objPocketShareDetails.Insert(0, findDuraOne)
                    End If

                    Return Ok(New ActionResultModel(g_status_success, "success", objPocketShareDetails))
                End Using


            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function


        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/GetSPRDetailByBranch")>
        <EnableCors("*", "*", "*")>
        Public Function GetSPRDetailByBranch(DeviceID As String,
                             UserID As Integer,
                             Year As Integer,
                             Month As Integer,
                             SMCode As String,
                             BranchId As Integer,
                             Group4SubCate As String) As IHttpActionResult
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                If Not ModelState.IsValid Then
                    Return BadRequest(ModelState)
                End If

                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                               Pub_UserId, Pub_Password)

                    dbContext.Database.CommandTimeout = 180


                    Dim _branchCustCode As String = Nothing
                    Dim _branchId As Integer = 0
                    Dim _branchName As String = Nothing

                    Dim objBrach = (From elm In dbContext.Branches
                                    Where elm.BranchID = BranchId
                                    Select elm).FirstOrDefault
                    If BranchId <> 0 And objBrach Is Nothing Then
                        Return Ok(New ActionResultModel(g_status_fail, "ไม่พบข้อมูลสาขา (ID: " & BranchId.ToString & ")", Nothing))
                    Else
                        _branchId = objBrach.BranchID
                        _branchName = objBrach.BranchName
                        _branchCustCode = objBrach.CustCode
                    End If

                    'elm.NetValueTHB > 0
                    Dim _totalNetValue As Decimal? = (From elm In dbContext.SmartPocketShareReport
                                                      Where elm.Year = Year And elm.Month = Month _
                                             And elm.Region7SM = SMCode _
                                             And elm.CustomerCode = _branchCustCode _
                                             And elm.Group4SubCate = Group4SubCate
                                                      Select NetValue = If(elm.Brand = mDURAONEBrand, If(elm.NetAmount, 0), If(elm.NetValueTHB, 0))).ToList.Sum

                    Dim objPocketShareDetails = (From elm In dbContext.SmartPocketShareReport
                                                 Where elm.Year = Year And elm.Month = Month _
                                             And elm.Region7SM = SMCode _
                                                      And elm.CustomerCode = _branchCustCode _
                                             And elm.Group4SubCate = Group4SubCate
                                                 Group By YearValue = elm.Year,
                                                    MonthValue = elm.Month,
                                                    Region7SM = elm.Region7SM,
                                                    ProductGsroup4SubCate = elm.Group4SubCate,
                                                    Brands = elm.Brand
                    Into NetValue = Sum(If(elm.Brand = mDURAONEBrand, If(elm.NetAmount, 0), If(elm.NetValueTHB, 0)))
                                                 Select New SPRDetailModel With {
                                               .YearValue = YearValue,
                                               .MonthValue = MonthValue,
                                               .Region7SM = Region7SM,
                                               .ProductGroup4SubCate = ProductGsroup4SubCate,
                                                                                             _
                                               .NetValue = NetValue,
                                               .Percentage = (NetValue / (If(_totalNetValue, 0))) * 100,
                                                                                                        _
                                               .BranchCustCode = _branchCustCode,
                                               .BranchID = _branchId,
                                               .BranchName = _branchName,
                                               .Brand = Brands
                                               }).ToList

                    '--- update DURA ONE to index 0 --
                    Dim findDuraOne = (From elm In objPocketShareDetails
                                       Where elm.Brand.Trim.ToUpper = mDURAONEBrand
                                       Select elm).FirstOrDefault
                    If findDuraOne IsNot Nothing Then
                        objPocketShareDetails.Remove(findDuraOne)
                        objPocketShareDetails.Insert(0, findDuraOne)
                    End If

                    Return Ok(New ActionResultModel(g_status_success, "success", objPocketShareDetails))
                End Using

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function

    End Class
End Namespace