﻿Imports System.IO
Imports System.Net
Imports System.Web.Hosting
Imports System.Web.Http
Imports System.Web.Http.Cors
Imports System.Web.Http.Description
Imports System.Security.Cryptography
Imports System.Threading.Tasks
Imports System.Net.Http
Imports System.Web.Script.Serialization
Imports RestSharp

Namespace Controllers.DURAONE
    Public Class CalculationController
        Inherits ApiController


        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/getCalFormulaVariables")>
        <EnableCors("*", "*", "*")>
        Function getCalFormulaVariables(DeviceID As String, UserID As Integer) As IHttpActionResult
            Try
                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                            Pub_UserId, Pub_Password)
                    Dim vars = (From elm In dbContext.CalFormulaVariable
                                Select New CalFormulaVariableModel With {
                                                    .VarKey = elm.VarKey,
                                                    .VarName = elm.VarName,
                                                    .VarUnit = elm.VarUnit
                                                    }).ToList

                    Return Ok(New ActionResultModel(g_status_success, "Success", vars))
                End Using

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function


        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/getCalProjectTypes")>
        <EnableCors("*", "*", "*")>
        Function getCalProjectTypes(DeviceID As String, UserID As Integer) As IHttpActionResult
            Try
                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                            Pub_UserId, Pub_Password)
                    Dim projectTypes = (From elm In dbContext.CalProjectType
                                        Where elm.IsActive = True
                                        Select New CalProjectTypeModel With {
                                                    .ProjectID = elm.ProjectID,
                                                    .ProjectName = elm.ProjectName,
                                                    .ImageUrl = elm.ImageUrl,
                                                    .Remark = elm.Remark
                                                    }).ToList

                    Return Ok(New ActionResultModel(g_status_success, "Success", projectTypes))
                End Using

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function


        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/getCalSubProjects")>
        <EnableCors("*", "*", "*")>
        Function getCalSubProjects(DeviceID As String, UserID As Integer,
                                   ProjectID As Integer) As IHttpActionResult
            Try
                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                            Pub_UserId, Pub_Password)
                    Dim subProjects = (From elm In dbContext.CalSubProject
                                       Where elm.IsActive = True _
                                           And elm.ProjectID = ProjectID
                                       Select New CalSubProjectModel With {
                                                    .ProjectID = elm.ProjectID,
                                                    .ProjectName = elm.CalProjectType.ProjectName,
                                                    .SubProjectID = elm.SubProjectID,
                                                    .SubProjectName = elm.SubProjectName,
                                                    .ImageUrl = elm.ImageUrl,
                                                    .Remark = elm.Remark,
                                                    .Main1Formula = elm.Main1Formula,
                                                    .Main2Formula = elm.Main2Formula,
                                                       .Main1Input = elm.Main1Input,
                                                       .Main2Input = elm.Main2Input
                                                    }).ToList

                    Return Ok(New ActionResultModel(g_status_success, "Success", subProjects))
                End Using

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function

        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/getMaterialMain1Groups")>
        <EnableCors("*", "*", "*")>
        Function getMaterialMain1Groups(DeviceID As String, UserID As Integer,
                                        SubProjectID As Integer) As IHttpActionResult
            Try
                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                            Pub_UserId, Pub_Password)

                    Dim subProjects = (From elm In dbContext.CalRelateMain1
                                       Where elm.SubProjectID = SubProjectID
                                       Select New CalMaterialGroupModel With {
                                                    .MatGroupID = elm.MatGroupID,
                                                    .MatGroupName = elm.CalMaterialGroup.MatGroupName,
                                                    .Remark = If(elm.CalMaterialGroup.Remark, "")
                                                    }).ToList
                    Return Ok(New ActionResultModel(g_status_success, "Success", subProjects))
                End Using


            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function


        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/getMaterialMain2Groups")>
        <EnableCors("*", "*", "*")>
        Function getMaterialMain2Groups(DeviceID As String, UserID As Integer,
                                        SubProjectID As Integer) As IHttpActionResult
            Try
                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                            Pub_UserId, Pub_Password)

                    Dim subProjects = (From elm In dbContext.CalRelateMain2
                                       Where elm.SubProjectID = SubProjectID
                                       Select New CalMaterialGroupModel With {
                                                    .MatGroupID = elm.MatGroupID,
                                                    .MatGroupName = elm.CalMaterialGroup.MatGroupName,
                                                    .Remark = If(elm.CalMaterialGroup.Remark, "")
                                                    }).ToList
                    Return Ok(New ActionResultModel(g_status_success, "Success", subProjects))
                End Using


            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function

        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/getMaterialMain1Parts")>
        <EnableCors("*", "*", "*")>
        Function getMaterialMain1Parts(DeviceID As String, UserID As Integer,
                                        SubProjectID As Integer, GroupId As Integer) As IHttpActionResult
            Try
                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                            Pub_UserId, Pub_Password)

                    Dim subProjects = (From elm In dbContext.CalRelateMain1Parts
                                       Where elm.CalRelateMain1.SubProjectID = SubProjectID And elm.CalRelateMain1.MatGroupID = GroupId
                                       Select New CalMaterialModel With {
                                           .MatID = 0,
                                           .CategoryID = 0,
                                           .CalPcsM = 0,
                                           .CalPcsSQM = 0,
                                           .CalX = 0,
                                        .MatGroupID = elm.MatGroupID,
                                        .MatGroupName = elm.CalMaterialGroup.MatGroupName,
                                        .Formula = elm.Formula,
                                        .Remark = If(elm.CalMaterialGroup.Remark, ""),
                                        .Quantity = 0
                                                    }).ToList
                    Return Ok(New ActionResultModel(g_status_success, "Success", subProjects))
                End Using


            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function



        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/getMaterialByGroup")>
        <EnableCors("*", "*", "*")>
        Function getMaterialByGroup(DeviceID As String, UserID As Integer,
                                    GroupID As Integer) As IHttpActionResult
            Try
                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                            Pub_UserId, Pub_Password)

                    Dim subProjects = (From elm In dbContext.CalMaterial
                                       Where elm.MatGroupID = GroupID
                                       Select New CalMaterialModel With {
                                                    .MatID = elm.MatID,
                                                    .MatGroupID = elm.MatGroupID,
                                                    .MatGroupName = elm.CalMaterialGroup.MatGroupName,
                                           .MatName = elm.MatName,
                                           .Barcode = elm.Barcode,
                                           .CalType = elm.CalType,
                                           .CalX = If(elm.CalX, 0),
                                           .CalPcsSQM = If(elm.CalPcsSQM, 0),
                                           .CalPcsM = If(elm.CalPcsM, 0),
                                           .CategoryID = If(elm.CategoryID, 0),
                                           .CategoryName = If(elm.Categories Is Nothing, "", elm.Categories.CategoryName),
                                           .Category3 = elm.Category3,
                                           .Category4 = elm.Category4,
                                           .Remark = If(elm.CalMaterialGroup.Remark, ""),
                                           .Quantity = 0
                                                    }).ToList

                    Return Ok(New ActionResultModel(g_status_success, "Success", subProjects))
                End Using


            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function

        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/validateMaterialCompetitor")>
        <EnableCors("*", "*", "*")>
        Public Function validateMaterialCompetitor(DeviceID As String,
                                                   UserID As Integer,
                                                   StoreID As Integer,
                                                   Province As String,
                                                   Materials As List(Of CalMaterialModel)) As IHttpActionResult
            Try

                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                If Not ModelState.IsValid Then
                    Return BadRequest(ModelState)
                End If

                If StoreID = 0 Then
                    Return New ActionResultModel(g_status_fail, "StoreID is Required", Nothing)
                End If



                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                                Pub_UserId, Pub_Password)

                    For Each _mat In Materials
                        Dim CompetitorsInfo = (From elm In dbContext.Competitors
                                               Let ToleranceValue As Decimal? = (If(elm.NormalPrice, 0) * If(elm.FaultTolerance, 0))
                                               Where elm.IsActive = True _
                                                   And elm.StoreID = StoreID _
                                                   And elm.Province = Province _
                                                   And elm.Barcode = _mat.Barcode
                                               Select elm).FirstOrDefault

                        If CompetitorsInfo Is Nothing Then
                            're-check with product number
                            CompetitorsInfo = (From elm In dbContext.Competitors
                                               Let ToleranceValue As Decimal? = (If(elm.NormalPrice, 0) * If(elm.FaultTolerance, 0))
                                               Where elm.IsActive = True _
                                                       And elm.StoreID = StoreID _
                                                       And elm.Province = Province _
                                                       And elm.ProductNo = _mat.Barcode
                                               Select elm).FirstOrDefault
                        End If

                        If CompetitorsInfo Is Nothing Then
                            _mat.ValidateStatus = g_status_fail
                            _mat.ValidateMessage = "ไม่พบข้อมูลราคาสินค้า"
                        Else
                            _mat.ValidateStatus = g_status_success
                            _mat.ValidateMessage = "ผ่านการตรวจสอบข้อมูลราคาเรียบร้อย"
                        End If
                    Next

                    Return Ok(New ActionResultModel(g_status_success, "success", Materials))

                End Using

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try


        End Function

    End Class
End Namespace