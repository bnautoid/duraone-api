﻿Imports System.Data.SqlClient
Imports System.IdentityModel.Tokens.Jwt
Imports System.Web.Http
Imports System.Web.Http.Cors
Imports System.Web.Http.Description
Imports System.Web.Script.Serialization
Imports Microsoft.IdentityModel.Tokens
Imports RestSharp

Namespace Controllers.DURAONE
    Public Class TrackingController
        Inherits ApiController

        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/TrackingUrlRedirect")>
        <EnableCors("*", "*", "*")>
        Public Function TrackingUrlRedirect(param As TrackingRequestDao) As IHttpActionResult
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(param.DeviceID, param.UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                If Not ModelState.IsValid Then
                    Return BadRequest(ModelState)
                End If

                'param.UserCode = "DV-02"
                'param.BranchSCGSoldTo = "5100017" 'customer_code
                'param.BranchSCGLocation = "1100013348" 'destination_code
                'param.SearchNumber = "BS2000035895" ' test ,2250036708,2250036709

                '-------- generate token JWT --------
                Dim encryptedJWT = Me.encryptJWT(param)

                '-------- get URL from API --------
                Dim TrackingUrl As String = ConfigurationManager.AppSettings("configTrackingUrl")
                Dim TrackingUrlDefaultLogin As String = ConfigurationManager.AppSettings("configTrackingDefaultLogin")

                Dim client = New RestClient(TrackingUrl)
                client.Timeout = -1
                Dim request = New RestRequest(Method.POST)
                request.AddHeader("Authorization", "Bearer " & encryptedJWT)
                Dim response As IRestResponse = client.Execute(request)
                'Console.WriteLine(response.Content)

                '--------- RESPORNSE ------------
                If response.IsSuccessful Then
                    Dim jsonText = response.Content
                    Dim jss As New JavaScriptSerializer()
                    jss.MaxJsonLength = Int32.MaxValue
                    Dim res As TrackingAPIResultDao = jss.Deserialize(Of TrackingAPIResultDao)(jsonText)

                    If res.returnCode = "200" Then
                        Return Ok(New ActionResultModel(g_status_success, res.message, res.data))

                    ElseIf res.returnCode = "400" Then
                        Return Ok(New ActionResultModel(g_status_fail, "CODE: " & res.returnCode & " " & res.message, TrackingUrlDefaultLogin))
                    Else
                        Return Ok(New ActionResultModel(g_status_fail, "CODE: " & res.returnCode & " " & res.message, TrackingUrlDefaultLogin))
                    End If

                Else
                    Return Ok(New ActionResultModel(g_status_fail, "cannot get url from API. Error message is " & response.ErrorMessage & vbNewLine & response.Content, TrackingUrlDefaultLogin))
                End If

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function

        Private Function encryptJWT(param As TrackingRequestDao) As String
            Try

                Dim TrackingPrivateKey As String = ConfigurationManager.AppSettings("configTrackingPrivateKey")

                Dim securityKey As New Microsoft.IdentityModel.Tokens.SymmetricSecurityKey(Encoding.UTF8.GetBytes(TrackingPrivateKey))
                Dim credentials As New Microsoft.IdentityModel.Tokens.SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256)

                Dim culture = New System.Globalization.CultureInfo("en-US")
                Dim dateTimeSring = Date.UtcNow.AddHours(7).ToString("yyyy-MM-dd'T'HH:mm:ss", culture)

                'dateTimeSring = "2025-01-08T16:23:47"

                Dim header = New JwtHeader(credentials)
                Dim payload = New JwtPayload From {
                                                        {"user_id", param.UserCode},
                                                        {"customer_code", param.BranchSCGSoldTo},
                                                        {"destination_code", param.BranchSCGLocation},
                                                        {"function", "track_customer"},
                                                        {"current_date", dateTimeSring},
                                                        {"searchNumber", param.SearchNumber}
                                                  }

                Dim secToken = New JwtSecurityToken(header, payload)
                Dim handler = New JwtSecurityTokenHandler()

                Dim tokenString = handler.WriteToken(secToken)
                'Dim token = handler.ReadJwtToken(tokenString)
                'Dim x = token.Payload.First().Value

                Return tokenString

            Catch ex As Exception
                Throw ex
            End Try
        End Function


        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/GetTrackingBranch")>
        <EnableCors("*", "*", "*")>
        Public Function GetTrackingBranch(DeviceID As String,
                                       UserID As Integer,
                                       BranchNameList As List(Of String)) As IHttpActionResult
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                If Not ModelState.IsValid Then
                    Return BadRequest(ModelState)
                End If

                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                               Pub_UserId, Pub_Password)

                    dbContext.Database.CommandTimeout = 180

                    'Dim objOrderTracking = (From ord In dbContext.OrderTracking
                    '                        Join br In dbContext.Branches On ord.Customer_name Equals br.BranchName
                    '                        Where If(br.IsActive, False) AndAlso ord.Ship_to IsNot Nothing AndAlso BranchNameList.Contains(ord.Customer_name)
                    '                        Group By br.BranchID, br_ship_to = br.ShipTo, br.BranchName, br.Region7_SM, br.CustCode, br.Address1, br.Province, tracking_Ship_to = ord.Ship_to, br.SCGLLocation, br.SCGLSoldTo
                    '                           Into Group
                    '                        Select New TrackingBranchDao With {
                    '                           .Region7SM = Region7_SM,
                    '                           .BranchCustCode = CustCode,
                    '                           .BranchID = BranchID,
                    '                           .BranchName = BranchName,
                    '                           .BranchShipTo = br_ship_to,
                    '                           .TrackingShipTo = tracking_Ship_to,
                    '                           .BranchAddress = Address1,
                    '                           .BranchProvince = Province,
                    '                           .BranchSCGSoldTo = SCGLSoldTo,
                    '                           .BranchSCGLocation = SCGLLocation,
                    '                           .CountPO = Group.Select(Function(x) x.ord.PO_number).Distinct().Count()
                    '                           }).ToList


                    '---------------------
                    ' Create a DataTable to hold your branch names
                    Dim branchTable As New DataTable()
                    branchTable.Columns.Add("BranchName", GetType(String))
                    For Each name As String In BranchNameList
                        branchTable.Rows.Add(name)
                    Next

                    ' Prepare the TVP parameter (make sure you've created a user-defined table type in SQL Server, e.g., dbo.BranchNameTableType)
                    Dim parameter = New SqlParameter("@BranchNames", branchTable)
                    parameter.TypeName = "dbo.BranchNameTableType"  ' This is a user-defined table type
                    parameter.SqlDbType = SqlDbType.Structured

                    ' Use a raw SQL query that joins with the TVP
                    Dim query As String = "
                                                SELECT COUNT(DISTINCT PO_number) AS CountPO,  
                                                                    br.BranchID AS BranchID, 
                                                                    br.ShipTo AS BranchShipTo, 
                                                                    br.BranchName AS BranchName, 
                                                                    br.Region7_SM AS Region7SM, 
                                                                    br.CustCode AS BranchCustCode, 
                                                                    br.Address1 AS BranchAddress, 
                                                                    br.Province AS BranchProvince, 
                                                                    ord.Ship_to AS TrackingShipTo, 
                                                                    br.SCGLLocation AS BranchSCGLocation, 
                                                                    br.SCGLSoldTo AS BranchSCGSoldTo
                                                FROM OrderTracking ord 
                                                INNER JOIN Branches br on ord.Customer_name = br.BranchName
                                                INNER JOIN @BranchNames bn ON ord.Customer_name = bn.BranchName
                                                WHERE br.IsActive = 1 AND ord.Ship_to IS NOT NULL
GROUP BY  br.BranchID, br.ShipTo, br.BranchName, br.Region7_SM, br.CustCode, br.Address1, br.Province, ord.Ship_to, br.SCGLLocation, br.SCGLSoldTo
                                                 "



                    Dim results = dbContext.Database.SqlQuery(Of TrackingBranchDao)(query, parameter).ToList()

                    '---------------------

                    Return Ok(New ActionResultModel(g_status_success, "success", results))

                End Using

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function


        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/GetTrackingPO")>
        <EnableCors("*", "*", "*")>
        Public Function GetTrackingPO(DeviceID As String,
                                 UserID As Integer,
                                 BranchName As String) As IHttpActionResult
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                If Not ModelState.IsValid Then
                    Return BadRequest(ModelState)
                End If

                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                               Pub_UserId, Pub_Password)

                    dbContext.Database.CommandTimeout = 180

                    Dim objOrderTracking = (From ord In dbContext.OrderTracking
                                            Where ord.Customer_name IsNot Nothing AndAlso ord.Customer_name = BranchName
                                            Group By ord.Customer_name, ord.PO_number
                                               Into Group
                                            Select New TrackingPOModel With {
                                               .BranchName = Customer_name,
                                               .PONumber = PO_number,
                                               .CountSO = Group.Select(Function(x) x.SO_number).Distinct().Count()
                                               }).ToList

                    Return Ok(New ActionResultModel(g_status_success, "success", objOrderTracking))

                End Using

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function


        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/GetTrackingSO")>
        <EnableCors("*", "*", "*")>
        Public Function GetTrackingSO(DeviceID As String,
                                 UserID As Integer,
                                 BranchName As String,
                                 PONumber As String) As IHttpActionResult
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                If Not ModelState.IsValid Then
                    Return BadRequest(ModelState)
                End If

                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                               Pub_UserId, Pub_Password)

                    dbContext.Database.CommandTimeout = 180

                    Dim objOrderTracking = (From ord In dbContext.OrderTracking
                                            Where ord.Customer_name IsNot Nothing AndAlso ord.Customer_name = BranchName And ord.PO_number = PONumber
                                            Group By ord.Customer_name, ord.PO_number, ord.SO_number
                                               Into Group
                                            Select New TrackingSOModel With {
                                                .BranchName = Customer_name,
                                               .PONumber = PO_number,
                                               .SONumber = SO_number,
                                               .CountItem = Group.Count
                                               }).ToList

                    Return Ok(New ActionResultModel(g_status_success, "success", objOrderTracking))

                End Using

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function


        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/GetTrackingItem")>
        <EnableCors("*", "*", "*")>
        Public Function GetTrackingItem(DeviceID As String,
                                 UserID As Integer,
                                 BranchName As String,
                                Optional PONumber As String = "",
                                Optional SONumber As String = "",
                                Optional DNNumber As String = "") As IHttpActionResult
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                'If Not ModelState.IsValid Then
                '    Return BadRequest(ModelState)
                'End If

                If PONumber = "" Then PONumber = Nothing
                If SONumber = "" Then SONumber = Nothing
                If DNNumber = "" Then DNNumber = Nothing

                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                               Pub_UserId, Pub_Password)

                    dbContext.Database.CommandTimeout = 180

                    Dim objOrderTracking = (From ord In dbContext.OrderTracking
                                            Where ord.Customer_name IsNot Nothing _
                                                AndAlso ord.Customer_name = BranchName _
                                                AndAlso (PONumber Is Nothing OrElse ord.PO_number = PONumber) _
                                                AndAlso (SONumber Is Nothing OrElse ord.SO_number = SONumber) _
                                                AndAlso (DNNumber Is Nothing OrElse ord.DN_number = DNNumber)
                                            Select New TrackingItemModel With {
                                                .BranchName = ord.Customer_name,
                                                .PONumber = ord.PO_number,
                                                .SONumber = ord.SO_number,
                                                .DNNumber = ord.DN_number,
                                                .MatCode = ord.Mat_code,
                                                .MatName = ord.Mat_name,
                                                .ProductUnit = ord.Product_unit,
                                                .OrderQty = ord.Order_quantity,
                                                .FillQty = ord.Fill_quantity,
                                                .BackQty = ord.Back_quantity
                                               }).ToList

                    Return Ok(New ActionResultModel(g_status_success, "success", objOrderTracking))

                End Using

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function

    End Class

    Public Class TrackingRequestDao
        Public Property DeviceID As String
        Public Property UserID As Integer
        Public Property UserCode As String
        Public Property BranchSCGSoldTo As String
        Public Property BranchSCGLocation As String
        Public Property SearchNumber As String
    End Class

    Public Class TrackingAPIResultDao
        Public Property returnCode As String
        Public Property success As Boolean
        Public Property data As String
        Public Property message As String
    End Class
End Namespace