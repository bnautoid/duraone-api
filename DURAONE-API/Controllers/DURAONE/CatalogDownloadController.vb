﻿Imports System.IO
Imports System.Net
Imports System.Web.Hosting
Imports System.Web.Http
Imports System.Web.Http.Cors
Imports System.Web.Http.Description
Imports System.Security.Cryptography
Imports System.Threading.Tasks
Imports System.Net.Http
Imports System.Web.Script.Serialization
Imports RestSharp
Imports System.Data.Objects


Namespace Controllers.DURAONE
    Public Class CatalogDownloadController
        Inherits ApiController

        Private Function getContent(contentType As String,
                                    userId As Integer,
                                    currentRegion As String,
                                    currentProvince As String,
                                    currentStoreId As Integer,
                                    currentBranchId As Integer) As List(Of ContentDownloadModel)
            Try


                Dim urlImage As String = ConfigurationManager.AppSettings("ContentImageFullUrl")

                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                           Pub_UserId, Pub_Password)

                    '--- check User branches
                    'Dim objUserBranches = (From elm In dbContext.UserBranch
                    '                       Where elm.UserID = userId
                    '                       Select elm.Branches).ToList

                    'If objUserBranches.Count = 0 Then
                    '    Throw New Exception("ไม่พบข้อมูลสาขาของรหัสผู้ใช้งานนี้")
                    'End If

                    'province list
                    'Dim provinceList = (From elm In objUserBranches Group By elm.Province Into Group Select Province).ToList
                    'store list
                    'Dim storeList = (From elm In objUserBranches Group By elm.StoreID Into Group Select StoreID).ToList
                    'branch list
                    'Dim branchList = (From elm In objUserBranches Group By elm.BranchID Into Group Select BranchID).ToList

                    '--- get content can access --
                    Dim contentIdList As New List(Of Decimal)

                    Dim objRolesByRegions = (From elm In dbContext.ContentRoles
                                             Where elm.Region = "ALL" OrElse elm.Region = currentRegion
                                             Select If(elm.ContentID, 0)).ToList

                    Dim objRolesByProvinces = (From elm In dbContext.ContentRoles
                                               Where elm.Province = "ALL" OrElse elm.Province = currentProvince
                                               Select If(elm.ContentID, 0)).ToList

                    Dim objRolesByStores As List(Of Decimal) = (From elm In dbContext.ContentRoles
                                                                Where elm.StoreID = 0 OrElse elm.StoreID = currentStoreId
                                                                Select If(elm.ContentID, 0)).ToList

                    Dim objRolesByBranches As List(Of Decimal) = (From elm In dbContext.ContentRoles
                                                                  Where elm.BranchID = 0 OrElse elm.BranchID = currentBranchId
                                                                  Select If(elm.ContentID, 0)).ToList

                    If objRolesByRegions IsNot Nothing Then contentIdList.AddRange(objRolesByRegions)
                    If objRolesByProvinces IsNot Nothing Then contentIdList.AddRange(objRolesByProvinces)
                    If objRolesByStores IsNot Nothing Then contentIdList.AddRange(objRolesByStores)
                    If objRolesByBranches IsNot Nothing Then contentIdList.AddRange(objRolesByBranches)

                    If contentIdList.Count = 0 Then
                        Throw New Exception("ไม่พบข้อมูลสำหรับการแสดง กรุณาตรวจสอบการตั้งค่าการเข้าถึงเนื้อหาของรหัสผู้ใช้งานนี้")
                    End If

                    '---- group if duplicate ----
                    contentIdList = (From elm In contentIdList Group By elm Into Group Select elm).ToList

                    Dim currentDate = Util.BkkNow.Date

                    Dim contents = (From elm In dbContext.ContentDownload
                                    Where elm.ContentType = contentType _
                                        And If(elm.IsActive, False) = True _
                                        And contentIdList.Contains(elm.ROWID) _
                                        And If(elm.ContentDate Is Nothing, True, currentDate >= elm.ContentDate)
                                    Select elm Order By elm.CreateDate Descending).ToList

                    Dim result = (From elm In contents
                                  Select New ContentDownloadModel With {
                                      .ROWID = elm.ROWID,
                                      .ContentType = elm.ContentType,
                                      .ContentSubject = elm.ContentSubject,
                                      .ContentCategory = elm.ContentCategory,
                                      .ProductDescription = elm.ProductDescription,
                                      .ContentTag = elm.ContentTag,
                                      .ContentDate = Util.DateToStringFormat(elm.ContentDate),
                                      .ContentDescription = elm.ContentDescription,
                                      .DisplayImageName = elm.DisplayImageName,
                                      .ImageUrl = urlImage & elm.DisplayImageName,
                                      .IsActive = If(elm.IsActive, False),
                                      .IsPinned = If(elm.IsPinned, False),
                                      .StartDate = Util.DateToStringFormat(elm.StartDate),
                                      .ExpireDate = Util.DateToStringFormat(elm.ExpireDate),
                                      .CreateDate = Util.DateToStringFormat(elm.CreateDate),
                                      .CreateBy = elm.CreateBy,
                                      .LastUpdateDate = Util.DateToStringFormat(elm.LastUpdateDate),
                                      .LastUpdateBy = elm.LastUpdateBy
                                      }).ToList

                    Return result
                End Using

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Function getFiles(contentId As Decimal) As List(Of ContentFilesModel)
            Try

                Dim urlImage As String = ConfigurationManager.AppSettings("ContentFileFullUrl")

                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                           Pub_UserId, Pub_Password)
                    Dim contentFiles = (From elm In dbContext.ContentFiles
                                        Where elm.Ref_ROWID = contentId _
                                            And If(elm.IsActive, False) = True
                                        Select elm).ToList

                    Dim result = (From elm In contentFiles
                                  Select New ContentFilesModel With {
                                      .ROWID = elm.ROWID,
                                      .Ref_ROWID = elm.Ref_ROWID,
                                      .FileName = elm.FileName,
                                      .FileType = elm.FileType,
                                      .Remark = elm.Remark,
                                      .IsActive = If(elm.IsActive, False),
                                      .CreateDate = Util.DateToStringFormat(elm.CreateDate),
                                      .CreateBy = elm.CreateBy,
                                      .LastUpdateDate = Util.DateToStringFormat(elm.LastUpdateDate),
                                      .LastUpdateBy = elm.LastUpdateBy,
                                      .FileUrl = urlImage & elm.FileName
                                      }).ToList

                    Return result
                End Using

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/getContentPromotions")>
        <EnableCors("*", "*", "*")>
        Function getContentPromotions(DeviceID As String, UserID As Integer,
                                    currentRegion As String,
                                    currentProvince As String,
                                    currentStoreId As Integer,
                                    currentBranchId As Integer) As IHttpActionResult
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                Dim contentType As String = ConfigurationManager.AppSettings("ContentTypePromotion")

                Dim result = getContent(contentType, UserID, currentRegion, currentProvince, currentStoreId, currentBranchId)

                '-- validate วันที่ expire --
                Dim currentDate = Util.BkkNow.Date

                Dim resultValidated = (From elm In result
                                       Where If(elm.ExpireDate Is Nothing, True, currentDate <= Util.convertStringToDate(elm.ExpireDate))
                                       Select elm).ToList


                Return Ok(New ActionResultModel(g_status_success, "Success", resultValidated))

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function


        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/getContentCatalogs")>
        <EnableCors("*", "*", "*")>
        Function getContentCatalogs(DeviceID As String, UserID As Integer,
                                     currentRegion As String,
                                    currentProvince As String,
                                    currentStoreId As Integer,
                                    currentBranchId As Integer) As IHttpActionResult
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                Dim contentType As String = ConfigurationManager.AppSettings("ContentTypeCatalog")

                Dim result = getContent(contentType, UserID, currentRegion, currentProvince, currentStoreId, currentBranchId)

                Return Ok(New ActionResultModel(g_status_success, "Success", result))

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function

        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/getContentInstallation")>
        <EnableCors("*", "*", "*")>
        Function getContentInstallation(DeviceID As String, UserID As Integer,
                                         currentRegion As String,
                                    currentProvince As String,
                                    currentStoreId As Integer,
                                    currentBranchId As Integer) As IHttpActionResult
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                Dim contentType As String = ConfigurationManager.AppSettings("ContentTypeInstallation")

                Dim result = getContent(contentType, UserID, currentRegion, currentProvince, currentStoreId, currentBranchId)

                Return Ok(New ActionResultModel(g_status_success, "Success", result))

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function


        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/getContentCertificate")>
        <EnableCors("*", "*", "*")>
        Function getContentCertificate(DeviceID As String, UserID As Integer,
                                        currentRegion As String,
                                    currentProvince As String,
                                    currentStoreId As Integer,
                                    currentBranchId As Integer) As IHttpActionResult
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                Dim contentType As String = ConfigurationManager.AppSettings("ContentTypeCertificate")

                Dim result = getContent(contentType, UserID, currentRegion, currentProvince, currentStoreId, currentBranchId)

                Return Ok(New ActionResultModel(g_status_success, "Success", result))

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function



        '--- get files 
        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/getContentFile")>
        <EnableCors("*", "*", "*")>
        Function getContentFile(DeviceID As String, UserID As Integer, ContentId As Decimal) As IHttpActionResult
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                Dim result = getFiles(ContentId)

                Return Ok(New ActionResultModel(g_status_success, "Success", result))

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function


    End Class
End Namespace