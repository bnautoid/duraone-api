﻿Imports System.Data.Entity
Imports System.IO
Imports System.Net
Imports System.Web.Http
Imports System.Web.Http.Cors
Imports System.Web.Http.Description
Imports Newtonsoft.Json

Namespace Controllers.DURAONE
    Public Class NotificationsController
        Inherits ApiController
        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/getNotification")>
        <EnableCors("*", "*", "*")>
        Public Function getNotification(DeviceID As String, UserID As Integer)
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                Dim _CurDate As Date = Util.BkkNow

                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                              Pub_UserId, Pub_Password)

                    Dim userBranches = (From e In dbContext.UserBranch
                                        Where e.UserID = UserID
                                        Select e.BranchID).ToList

                    Dim getNotifications = (From e In dbContext.Notifications
                                            Where If(If(e.NotifyToBranchID, 0) <> 0, userBranches.Contains(e.NotifyToBranchID), True) _
                                             And If(If(e.NotifyToUserID, 0) <> 0, e.NotifyToUserID = UserID, True)
                                            Order By e.CreateDate Descending
                                            Select e).Take(99).ToList

                    Dim result = (From e In getNotifications
                                  Select New NotificationModel With {
                    .ROWID = e.ROWID,
                    .NotifyDate = If(e.NotifyDate Is Nothing, Nothing, e.NotifyDate.Value.ToString("dd/MM/yyyy hh:mm", g_culture_en)),
                    .NotifyType = e.NotifyType,
                    .NotifySubject = e.NotifySubject,
                    .NotifyDescription = e.NotifyDescription,
                    .NotifyToUserID = If(e.NotifyToUserID, 0),
                    .NotifyToBranchID = If(e.NotifyToBranchID, 0),
                    .NotifyToStoreID = If(e.NotifyToStoreID, 0),
                    .NotifyNumber = If(e.NotifyNumber, 0),
                    .IsRead = If(e.IsRead, False),
                    .ReadDate = If(e.ReadDate Is Nothing, Nothing, e.ReadDate.Value.ToString("dd/MM/yyyy hh:mm", g_culture_en)),
                    .CreateDate = If(e.CreateDate Is Nothing, Nothing, e.CreateDate.Value.ToString("dd/MM/yyyy hh:mm", g_culture_en)),
                    .CreateBy = If(e.CreateBy, 0),
                    .LastUpdateDate = If(e.LastUpdateDate Is Nothing, Nothing, e.LastUpdateDate.Value.ToString("dd/MM/yyyy hh:mm", g_culture_en)),
                    .LastUpdateBy = If(e.LastUpdateBy, 0)
                    }).ToList

                    Return Ok(New ActionResultModel(g_status_success, "Success", result))
                End Using
            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function


        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/getCountNotification")>
        <EnableCors("*", "*", "*")>
        Public Function getCountNotification(DeviceID As String, UserID As Integer)
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                Dim _CurDate As Date = Util.BkkNow

                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                                  Pub_UserId, Pub_Password)

                    Dim getNotifications = (From e In dbContext.Notifications
                                            Where e.NotifyToUserID = UserID _
                                                    And (e.IsRead Is Nothing OrElse e.IsRead = False)
                                            Order By e.CreateDate Descending
                                            Select e).Take(99).ToList

                    Return Ok(New ActionResultModel(g_status_success, "Success", getNotifications.Count))

                End Using
            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function

        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/saveNotification")>
        <EnableCors("*", "*", "*")>
        Public Function saveNotification(DeviceID As String, UserID As Integer,
                                         notification As NotificationModel)
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                Dim _CurDate As Date = Util.BkkNow

                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                              Pub_UserId, Pub_Password)

                    If notification.NotifyDescription.Length > 1000 Then
                        notification.NotifyDescription = notification.NotifyDescription.Substring(0, 950) & vbNewLine & "(มีต่อ) ....."
                    End If


                    Dim objSaveNotify = (From elm In dbContext.Notifications
                                         Where (elm.IsRead Is Nothing OrElse elm.IsRead = False) _
                                           And elm.NotifyToUserID = notification.NotifyToUserID _
                                           And elm.NotifyType = notification.NotifyType _
                                           And elm.NotifySubject = notification.NotifySubject _
                                           And elm.NotifyDescription = notification.NotifyDescription
                                         Select elm).FirstOrDefault

                    If objSaveNotify IsNot Nothing Then
                        objSaveNotify.NotifyNumber = objSaveNotify.NotifyNumber.GetValueOrDefault(0) + notification.NotifyNumber
                        objSaveNotify.LastUpdateDate = _CurDate
                        objSaveNotify.LastUpdateBy = notification.LastUpdateBy
                    Else
                        objSaveNotify = New Notifications
                        With objSaveNotify
                            .NotifyDate = _CurDate.Date
                            .NotifyType = notification.NotifyType
                            .NotifySubject = notification.NotifySubject
                            .NotifyDescription = notification.NotifyDescription
                            .NotifyToUserID = If(notification.NotifyToUserID = 0, Nothing, notification.NotifyToUserID)
                            .NotifyToBranchID = If(notification.NotifyToBranchID = 0, Nothing, notification.NotifyToBranchID)
                            .NotifyToStoreID = If(notification.NotifyToStoreID = 0, Nothing, notification.NotifyToStoreID)
                            .NotifyNumber = If(notification.NotifyNumber = 0, Nothing, notification.NotifyNumber)
                            .IsRead = False
                            .ReadDate = Nothing
                            .CreateDate = _CurDate
                            .CreateBy = notification.CreateBy
                            .LastUpdateDate = _CurDate
                            .LastUpdateBy = notification.LastUpdateBy
                        End With

                        dbContext.Notifications.Add(objSaveNotify)
                    End If


                    dbContext.SaveChanges()

                    '-- push notify --
                    Try
                        Dim fromUserId As Integer = objSaveNotify.CreateBy.GetValueOrDefault(0)
                        Dim fromUserName As String = ""
                        Dim objFromUser = (From elm In dbContext.Users
                                           Where elm.UserID = fromUserId
                                           Select elm).FirstOrDefault

                        If objFromUser IsNot Nothing Then
                            fromUserName = If(objFromUser.FirstName, "") & " " & If(objFromUser.LastName, "")
                        End If

                        ' Dim t As New Thread(AddressOf FCMPushNotification)
                        Dim param As New FCMParameter

                        param.user_id = objSaveNotify.NotifyToUserID.GetValueOrDefault(0)
                        param.title_text = objSaveNotify.NotifySubject & " : " & fromUserName
                        param.body_text = objSaveNotify.NotifyDescription & " " & +objSaveNotify.NotifyNumber.GetValueOrDefault(0) & " รายการ"

                        Me.FCMPushNotification(param)
                        't.Start(param)

                    Catch ex As Exception

                    End Try
                    '-----------------

                    Return Ok(New ActionResultModel(g_status_success, "Success", objSaveNotify))
                End Using

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function

        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/updateNotifyRead")>
        <EnableCors("*", "*", "*")>
        Public Function updateNotifyRead(DeviceID As String, UserID As Integer, RowId As Int64)
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                Dim _CurDate As Date = Util.BkkNow

                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                              Pub_UserId, Pub_Password)

                    Dim updateNotify = (From elm In dbContext.Notifications
                                        Where elm.ROWID = RowId
                                        Select elm).FirstOrDefault
                    If updateNotify IsNot Nothing Then

                        updateNotify.IsRead = True
                        updateNotify.ReadDate = _CurDate
                        updateNotify.LastUpdateDate = _CurDate
                        updateNotify.LastUpdateBy = UserID

                        dbContext.SaveChanges()

                        Return Ok(New ActionResultModel(g_status_success, "Success", Nothing))
                    Else
                        Return Ok(New ActionResultModel(g_status_fail, "Fail! no data updates", Nothing))
                    End If

                End Using

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function

#Region "FCM Notification"
        Public Function FCMPushNotification(ByVal param As Object) As Boolean
            Try
                Dim _parameter = CType(param, FCMParameter)
                Dim user_id = _parameter.user_id
                Dim title_text = _parameter.title_text
                Dim body_text = _parameter.body_text

                Dim ClientToken As String = ""

                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                             Pub_UserId, Pub_Password)

                    Dim UserDevice = (From e In dbContext.Users
                                      Where e.UserID = user_id
                                      Select e).FirstOrDefault
                    If UserDevice IsNot Nothing Then
                        ClientToken = UserDevice.FCMToken
                    Else
                        Return False
                    End If

                End Using

                If ClientToken = "" OrElse ClientToken Is Nothing Then Return False

                Dim senderId = ConfigurationManager.AppSettings("configFCMSenderId")
                Dim serverKey = ConfigurationManager.AppSettings("configFCMServerKey")
                Dim fcmTokenTo = ClientToken
                Dim tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send")

                tRequest.Method = "post"
                tRequest.Headers.Add(String.Format("Authorization: key={0}", serverKey))
                tRequest.Headers.Add(String.Format("Sender: id={0}", senderId))
                tRequest.ContentType = "application/json"

                Dim payload = New FCMPayload With {
                                                             .to = fcmTokenTo,
                                                             .priority = "high",
                                                             .content_available = True,
                                                             .notification = New FCMPayloadNotification With {
                                                                             .title = title_text,
                                                                             .body = body_text,
                                                                             .sound = "default",
                                                                             .badge = 1
                                                                            },
                                                            .data = New FCMData With {
                                                                             .key1 = "Value1",
                                                                             .key2 = "Value2"
                                                                            }
                                                   }

                Dim postbody As String = JsonConvert.SerializeObject(payload).ToString()
                Dim byteArray As Byte() = Encoding.UTF8.GetBytes(postbody.ToString)
                tRequest.ContentLength = byteArray.Length
                Using dataStream As Stream = tRequest.GetRequestStream()
                    dataStream.Write(byteArray, 0, byteArray.Length)
                    Using tResponse As WebResponse = tRequest.GetResponse()
                        Using dataStreamResponse As Stream = tResponse.GetResponseStream()
                            If dataStreamResponse IsNot Nothing Then
                                Using tReader As New StreamReader(dataStreamResponse)
                                    Dim sResponseFromServer As String = tReader.ReadToEnd()

                                    Return True

                                End Using

                            End If

                        End Using
                    End Using
                End Using

                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function

        Class FCMParameter
            Public Property user_id As Integer
            Public Property title_text As String
            Public Property body_text As String
        End Class
        Class FCMPayload
            Public Property [to] As String
            Public Property priority As String
            Public Property content_available As Boolean
            Public Property notification As FCMPayloadNotification
            Public Property data As FCMData
        End Class
        Class FCMPayloadNotification
            Public Property title As String
            Public Property body As String
            Public Property sound As String
            Public Property badge As Integer
        End Class
        Class FCMData
            Public Property key1 As String
            Public Property key2 As String
        End Class
#End Region


    End Class
End Namespace