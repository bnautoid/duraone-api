﻿Imports System.IO
Imports System.Net
Imports System.Web.Hosting
Imports System.Web.Http
Imports System.Web.Http.Cors
Imports System.Web.Http.Description
Imports System.Security.Cryptography
Imports System.Data.Entity

Namespace Controllers.DURAONE
    Public Class UpdatePricingController
        Inherits ApiController

        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/UpdatePricingToCart")>
        <EnableCors("*", "*", "*")>
        Public Function UpdatePricingToCart(obj As SaveUpdatePricingModel) As IHttpActionResult
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(obj.DeviceID, obj.UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================
                If Not ModelState.IsValid Then
                    Return BadRequest(ModelState)
                End If

                Dim resultSaved = Me.savePromotionPriceToDB(obj)

                Return Ok(resultSaved)

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function

        Public Function savePromotionPriceToDB(obj As SaveUpdatePricingModel) As ActionResultModel
            Try

                Dim _g_culture_en As New System.Globalization.CultureInfo("en-US")
                Dim _TransactDate As Date? = Nothing
                Dim _NewDate As Date? = Nothing
                Dim _PromotionDate As Date? = Nothing
                Dim _CurDate As Date = Util.BkkNow

                '-----validate ----
                Dim resultValidated = Me.ValidatePricing(obj)
                If resultValidated.resultcode = g_status_fail Then
                    Return resultValidated
                End If

                '---- convert date ---
                If obj.TransactDate IsNot Nothing Then _TransactDate = Date.ParseExact(obj.TransactDate, "dd/MM/yyyy", _g_culture_en).Date
                If obj.NewDate IsNot Nothing Then _NewDate = Date.ParseExact(obj.NewDate, "dd/MM/yyyy", _g_culture_en).Date
                If obj.PromotionDate IsNot Nothing Then _PromotionDate = Date.ParseExact(obj.PromotionDate, "dd/MM/yyyy", _g_culture_en).Date

                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                               Pub_UserId, Pub_Password)

                    Dim BranchInfo = (From elm In dbContext.Branches
                                      Where elm.BranchID = obj.BranchID And elm.IsActive = True
                                      Select elm).SingleOrDefault

                    If BranchInfo Is Nothing Then
                        Return New ActionResultModel(g_status_fail, "Invalid Branch Information", Nothing)
                    End If

                    Dim IsProd = (From elm In dbContext.Competitors
                                  Where elm.StoreID = BranchInfo.StoreID AndAlso elm.ROWID = obj.CompetitorID AndAlso elm.IsActive = True
                                  Select elm).SingleOrDefault

                    If IsProd IsNot Nothing Then
                        Dim _NewPrice As New UpdatePricing
                        _NewPrice.BranchID = obj.BranchID
                        _NewPrice.TransactDate = _TransactDate
                        _NewPrice.CompetitorID = obj.CompetitorID
                        _NewPrice.NormalPrice = IsProd.NormalPrice.GetValueOrDefault(0)
                        _NewPrice.NewPrice = obj.NewPrice
                        _NewPrice.NewDate = _NewDate
                        _NewPrice.PromotionPrice = obj.PromotionPrice
                        _NewPrice.PromotionDate = _PromotionDate
                        _NewPrice.Remark = ""

                        '_NewPrice.IsApproved = True 'False
                        '_NewPrice.ApprovedDate = _CurDate ' Nothing 
                        '_NewPrice.ApprovedBy = g_system_user 'Nothing 

                        _NewPrice.IsApproved = False
                        _NewPrice.ApprovedDate = Nothing
                        _NewPrice.ApprovedBy = Nothing

                        _NewPrice.IsRejected = False
                        _NewPrice.RejectedDate = Nothing
                        _NewPrice.RejectedBy = Nothing
                        _NewPrice.IsActive = True
                        _NewPrice.CreateBy = obj.UserID
                        _NewPrice.CreateDate = _CurDate
                        _NewPrice.LastUpdateBy = obj.UserID
                        _NewPrice.LastUpdateDate = _CurDate

                        _NewPrice.ToleranceReason = obj.Reason

                        dbContext.UpdatePricing.Add(_NewPrice)
                        dbContext.SaveChanges()

                        Return New ActionResultModel(g_status_success, "success", _NewPrice)
                    Else
                        Return New ActionResultModel(g_status_fail, "Invalid Store and Product is not associated", Nothing)
                    End If

                End Using

            Catch ex As Exception
                Throw ex
            End Try

        End Function

        '<HttpPost()>
        '<ResponseType(GetType(IHttpActionResult))>
        '<Route("api/ValidatePricing")>
        '<EnableCors("*", "*", "*")>
        Private Function ValidatePricing(obj As SaveUpdatePricingModel) As ActionResultModel

            Dim _g_culture_en As New System.Globalization.CultureInfo("en-US")
            Dim _TransactDate As Date = Nothing
            Dim _NewDate As Date = Nothing
            Dim _PromotionDate As Date = Nothing
            Dim _CurDate As Date = Util.BkkNow
            Try

                If obj.BranchID = 0 Then
                    Return New ActionResultModel(g_status_fail, "BranchID is Required", Nothing)
                End If

                If obj.UserID = 0 Then
                    Return New ActionResultModel(g_status_fail, "UserID is Required", Nothing)
                End If

                Try
                    _NewDate = Date.ParseExact(obj.NewDate, "dd/MM/yyyy", _g_culture_en).Date
                Catch ex As Exception
                    Return New ActionResultModel(g_status_fail, "Unable to convert New Price format (dd/MM/yyyy)", Nothing)
                End Try

                If obj.PromotionPrice <= 0 AndAlso obj.NewPrice <= 0 Then
                    Return New ActionResultModel(g_status_fail, "Price is Valid. Please checking again!", Nothing)
                End If

                If obj.PromotionDate IsNot Nothing Then
                    Try
                        _PromotionDate = Date.ParseExact(obj.PromotionDate, "dd/MM/yyyy", _g_culture_en).Date
                    Catch ex As Exception
                        Return New ActionResultModel(g_status_fail, "Unable to convert PromotionDate format (dd/MM/yyyy)", Nothing)
                    End Try

                End If

                If obj.PromotionDate IsNot Nothing Then
                    Try
                        _TransactDate = Date.ParseExact(obj.TransactDate, "dd/MM/yyyy", _g_culture_en).Date
                    Catch ex As Exception
                        Return New ActionResultModel(g_status_fail, "Unable to convert TransactDate format (dd/MM/yyyy)", Nothing)
                        '_TransactDate = _CurDate.Date
                    End Try
                End If


                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                               Pub_UserId, Pub_Password)

                    Dim BranchInfo = (From elm In dbContext.Branches
                                      Where elm.BranchID = obj.BranchID And elm.IsActive = True
                                      Select elm).SingleOrDefault

                    If BranchInfo Is Nothing Then
                        Return New ActionResultModel(g_status_fail, "Invalid Branch Information", Nothing)
                    End If

                    Dim IsProd = (From elm In dbContext.Competitors
                                  Where elm.StoreID = BranchInfo.StoreID AndAlso elm.ROWID = obj.CompetitorID AndAlso elm.IsActive = True
                                  Select elm).SingleOrDefault


                    If IsProd Is Nothing Then
                        Return New ActionResultModel(g_status_fail, "Invalid Store and Product is not associated", Nothing)
                    End If

                    Dim IsPricing = (From elm In dbContext.UpdatePricing
                                     Where elm.BranchID = obj.BranchID AndAlso DbFunctions.TruncateTime(elm.TransactDate) = _TransactDate AndAlso
                                             elm.CompetitorID = obj.CompetitorID
                                     Select elm).FirstOrDefault

                    If IsPricing IsNot Nothing Then
                        Return New ActionResultModel(g_status_fail, "There are pricing has already updated", IsPricing)
                    Else
                        Return New ActionResultModel(g_status_success, "Success", Nothing)
                    End If

                End Using

            Catch ex As Exception
                Return New ActionResultModel(g_status_fail, ex.Message, Nothing)
            End Try
        End Function


        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/getNotifyItemCart")>
        <EnableCors("*", "*", "*")>
        Public Function getNotifyItemCart(DeviceID As String, UserID As Integer,
                                          BranchID As Integer, TransactDate As String) As IHttpActionResult
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                Dim _g_culture_en As New System.Globalization.CultureInfo("en-US")
                Dim _TransactDate As Date
                Try
                    _TransactDate = Date.ParseExact(TransactDate, "dd/MM/yyyy", _g_culture_en).Date
                Catch ex As Exception
                    Return Ok(New ActionResultModel(g_status_fail, "API Error: ", "API Error: " & ex.Message))
                End Try


                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                              Pub_UserId, Pub_Password)

                    Dim countItemCart As Integer? = (From elm In dbContext.UpdatePricing
                                                     Where elm.BranchID = BranchID _
                                          And elm.TransactDate = _TransactDate _
                                          And elm.CreateBy = UserID _
                                          And elm.IsActive = True _
                                           And (elm.IsSend = False OrElse elm.IsSend Is Nothing) _
                                          And (elm.IsApproved = False OrElse elm.IsApproved Is Nothing) _
                                          And (elm.IsRejected = False OrElse elm.IsRejected Is Nothing)
                                                     Select elm).ToList.Count
                    Dim result As Integer = countItemCart.GetValueOrDefault(0)
                    Return Ok(New ActionResultModel(g_status_success, "Success", result))

                End Using

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function

        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/getItemCart")>
        <EnableCors("*", "*", "*")>
        Public Function getItemCart(DeviceID As String, UserID As Integer, BranchID As Integer, TransactDate As String) As IHttpActionResult
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                Dim _g_culture_en As New System.Globalization.CultureInfo("en-US")
                Dim _TransactDate As Date
                Try
                    _TransactDate = Date.ParseExact(TransactDate, "dd/MM/yyyy", _g_culture_en).Date
                Catch ex As Exception
                    Return Ok(New ActionResultModel(g_status_fail, "API Error: ", "API Error: " & ex.Message))
                End Try


                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                              Pub_UserId, Pub_Password)

                    Dim countItemsCart = (From prc In dbContext.UpdatePricing
                                          Let cpt = (From c In dbContext.Competitors
                                                     Where c.ROWID = prc.CompetitorID
                                                     Select c).FirstOrDefault
                                          Where cpt IsNot Nothing _
                                              And prc.BranchID = BranchID _
                                              And prc.TransactDate = _TransactDate _
                                              And prc.CreateBy = UserID _
                                              And prc.IsActive = True _
                                              And (prc.IsSend = False OrElse prc.IsSend Is Nothing) _
                                              And (prc.IsApproved = False OrElse prc.IsApproved Is Nothing) _
                                              And (prc.IsRejected = False OrElse prc.IsRejected Is Nothing)
                                          Select prc, cpt).ToList


                    Dim result = (From e In countItemsCart
                                  Select New UpdatePricingModel With {
                                      .ROWID = e.prc.ROWID,
                                      .BranchID = e.prc.BranchID,
                                      .TransactDate = e.prc.TransactDate.ToString("dd/MM/yyyy", g_culture_en),
 _
                                      .CompetitorID = e.prc.CompetitorID,
                                      .ProductNo = e.cpt.ProductNo,
                                      .ProductName = e.cpt.ProductName,
                                      .UnitCode = e.cpt.UnitCode,
                                      .Barcode = e.cpt.Barcode,
 _
                                      .NormalPrice = e.prc.NormalPrice.GetValueOrDefault(0),
                                      .NewPrice = e.prc.NewPrice.GetValueOrDefault(0),
                                      .NewDate = If(e.prc.NewDate Is Nothing, Nothing, e.prc.NewDate.Value.ToString("dd/MM/yyyy", _g_culture_en)),
                                      .PromotionPrice = e.prc.PromotionPrice.GetValueOrDefault(0),
                                      .PromotionDate = If(e.prc.PromotionDate Is Nothing, Nothing, e.prc.PromotionDate.Value.ToString("dd/MM/yyyy", _g_culture_en)),
 _
                                      .Remark = e.prc.Remark,
                                      .IsApproved = e.prc.IsApproved.GetValueOrDefault(False),
                                      .ApprovedDate = If(e.prc.ApprovedDate Is Nothing, Nothing, e.prc.ApprovedDate.Value.ToString("dd/MM/yyyy", _g_culture_en)),
                                      .ApprovedBy = If(e.prc.ApprovedBy, 0),
                                      .IsRejected = e.prc.IsRejected.GetValueOrDefault(False),
                                      .RejectedDate = If(e.prc.RejectedDate Is Nothing, Nothing, e.prc.RejectedDate.Value.ToString("dd/MM/yyyy", _g_culture_en)),
                                      .RejectedBy = If(e.prc.RejectedBy, 0),
                                      .IsActive = e.prc.IsActive.GetValueOrDefault(False),
                                      .CreateBy = If(e.prc.CreateBy, 0),
                                      .ToleranceReason = e.prc.ToleranceReason
                                      }).ToList

                    Return Ok(New ActionResultModel(g_status_success, "Success", result))

                End Using

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function

        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/saveSendPricing")>
        <EnableCors("*", "*", "*")>
        Public Function saveSendPricing(DeviceID As String, UserID As Integer, RowIds As List(Of Int64)) As IHttpActionResult
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                Dim _CurDate As Date = Util.BkkNow
                Dim _sender As Users = Nothing
                Dim _aprover As Users = Nothing

                If RowIds Is Nothing OrElse RowIds.Count = 0 Then
                    Return Ok(New ActionResultModel(g_status_fail, "ไม่พบข้อมูลสำหรับการบันทึก", Nothing))
                End If


                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                              Pub_UserId, Pub_Password)

                    For Each _rowId As Int64 In RowIds
                        Dim pricingUpdate = (From elm In dbContext.UpdatePricing
                                             Where elm.ROWID = _rowId
                                             Select elm).FirstOrDefault
                        If pricingUpdate IsNot Nothing Then
                            pricingUpdate.IsSend = True
                            pricingUpdate.SendDate = _CurDate
                            pricingUpdate.SendBy = UserID

                            '--- auto approve ---
                            pricingUpdate.IsApproved = True
                            pricingUpdate.ApprovedDate = _CurDate
                            pricingUpdate.ApprovedBy = UserID
                            '--------------------

                            pricingUpdate.LastUpdateDate = _CurDate
                            pricingUpdate.LastUpdateBy = UserID
                        End If
                    Next

                    dbContext.SaveChanges()

                    '-- get sender/approver
                    _sender = (From elm In dbContext.Users
                               Where elm.UserID = UserID
                               Select elm).FirstOrDefault

                    Dim _userRelate = (From elm In dbContext.UserRelated
                                       Where elm.StaffUserID = UserID
                                       Select elm).FirstOrDefault
                    If _userRelate IsNot Nothing Then
                        _aprover = (From elm In dbContext.Users
                                    Where elm.UserID = _userRelate.SuperUserID
                                    Select elm).FirstOrDefault
                    End If

                End Using

                '================== NOTI ==================
                Dim objSaveNotify As New NotificationModel
                With objSaveNotify
                    .NotifyDate = _CurDate.Date
                    .NotifyType = g_notifyType_APPORVE_PRICING
                    '.NotifySubject = "Approve Pricing"
                    '.NotifyDescription = If(_sender Is Nothing, "", _sender.FirstName) & " ส่งข้อมูล " & RowIds.Count & " รายการ เพื่อรอยืนยันราคา."
                    '.NotifyDescription = "รอยืนยันราคา"
                    .NotifySubject = "ราคาโปรโมชั่น (ป้ายแดง)"
                    .NotifyDescription = "มีราคาใหม่" & If(_sender Is Nothing, "", " จาก : " & _sender.FirstName)
                    .NotifyNumber = RowIds.Count
                    .NotifyToUserID = If(_aprover Is Nothing, 0, _aprover.UserID)
                    .NotifyToBranchID = 0
                    .NotifyToStoreID = 0
                    .IsRead = False
                    .ReadDate = Nothing
                    .CreateDate = _CurDate
                    .CreateBy = UserID
                    .LastUpdateDate = _CurDate
                    .LastUpdateBy = UserID
                End With

                Dim noti As New NotificationsController
                noti.saveNotification(DeviceID, UserID, objSaveNotify)
                '=============================================

                Return Ok(New ActionResultModel(g_status_success, "Success", Nothing))

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function


        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/editPricing")>
        <EnableCors("*", "*", "*")>
        Public Function editPricing(DeviceID As String, UserID As Integer, RowId As Long, NewPrice As Decimal, PromotionDate As String) As IHttpActionResult
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                Dim _CurDate As Date = Util.BkkNow
                Dim _PromotionDate As Date
                Try
                    _PromotionDate = Date.ParseExact(PromotionDate, "dd/MM/yyyy", g_culture_en).Date
                Catch ex As Exception
                    Return Ok(New ActionResultModel(g_status_fail, "API Error: ", "API Error: " & ex.Message))
                End Try

                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                              Pub_UserId, Pub_Password)

                    Dim pricingUpdate = (From elm In dbContext.UpdatePricing
                                         Where elm.ROWID = RowId
                                         Select elm).FirstOrDefault
                    If pricingUpdate IsNot Nothing Then
                        pricingUpdate.PromotionPrice = NewPrice
                        pricingUpdate.NewPrice = NewPrice
                        pricingUpdate.PromotionDate = _PromotionDate

                        pricingUpdate.LastUpdateDate = _CurDate
                        pricingUpdate.LastUpdateBy = UserID
                    Else
                        Return Ok(New ActionResultModel(g_status_fail, "ไม่พบข้อมูลสำหรับการบันทึก", Nothing))
                    End If

                    dbContext.SaveChanges()

                End Using

                Return Ok(New ActionResultModel(g_status_success, "Success", Nothing))

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function

        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/deletePricing")>
        <EnableCors("*", "*", "*")>
        Public Function deletePricing(DeviceID As String, UserID As Integer, RowId As Long) As IHttpActionResult
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                              Pub_UserId, Pub_Password)

                    Dim pricingUpdate = (From elm In dbContext.UpdatePricing
                                         Where elm.ROWID = RowId
                                         Select elm).FirstOrDefault

                    If pricingUpdate IsNot Nothing Then

                        dbContext.UpdatePricing.Remove(pricingUpdate)

                        '--- reset notification --
                        Dim objNotificationPrice = (From elm In dbContext.UpdatePricingNotification
                                                    Where elm.ActionCode = Util.g_noti_action_promotion _
                                                            And elm.AddToCartRowId = pricingUpdate.ROWID
                                                    Select elm).FirstOrDefault
                        If objNotificationPrice IsNot Nothing Then
                            objNotificationPrice.AddToCartBy = Nothing
                            objNotificationPrice.AddToCartDate = Nothing
                            objNotificationPrice.IsAddToCart = Nothing
                            objNotificationPrice.AddToCartRowId = Nothing
                        End If
                        '-------------------------

                        dbContext.SaveChanges()
                    Else
                        Return Ok(New ActionResultModel(g_status_fail, "ไม่พบข้อมูลสำหรับการบันทึก", Nothing))
                    End If

                End Using

                Return Ok(New ActionResultModel(g_status_success, "Success", Nothing))

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function


        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/getUserListForApprove")>
        <EnableCors("*", "*", "*")>
        Public Function getUserListForApprove(DeviceID As String, UserID As Integer) As IHttpActionResult
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                              Pub_UserId, Pub_Password)

                    Dim _CurDate As Date = Util.BkkNow

                    '-- get user relate 
                    Dim _userRelate As List(Of Integer) = (From elm In dbContext.UserRelated
                                                           Where elm.SuperUserID = UserID
                                                           Select elm.StaffUserID).ToList

                    Dim getListForApprove = (From price In dbContext.UpdatePricing
                                             Let branch = (From br In dbContext.Branches Where br.BranchID = price.BranchID Select br).FirstOrDefault
                                             Where price.IsSend = True AndAlso _userRelate.Contains(price.SendBy) _
                                             And (price.SendDate IsNot Nothing AndAlso DbFunctions.TruncateTime(price.SendDate) = _CurDate.Date)
                                             Select price,
                                                 branch,
                                                 store = branch.Stores).ToList

                    'And (price.IsApproved Is Nothing OrElse price.IsApproved = False) _
                    'And (price.IsRejected Is Nothing OrElse price.IsRejected = False)

                    Dim groupByUser = (From elm In getListForApprove
                                       Let user = (From usr In dbContext.Users Where usr.UserID = elm.price.SendBy).FirstOrDefault
                                       Where user IsNot Nothing
                                       Group By _UserID = user.UserID,
                                           user.UserCode, user.FirstName, user.LastName, user.Title,
                                           _StoreID = elm.store.StoreID, elm.store.StoreName,
                                           elm.branch.BranchID, elm.branch.BranchName
                                           Into Group
                                       Select New ApproveListUserModel With {
                                           .UserID = _UserID,
                                           .UserCode = UserCode,
                                           .FirstName = FirstName,
                                           .LastName = LastName,
                                           .FullName = FirstName & " " & LastName,
                                           .Title = Title,
                                           .StoreID = _StoreID,
                                           .StoreName = StoreName,
                                           .BranchID = BranchID,
                                           .BrandName = BranchName,
                                           .TotalItems = Group.Count,
                                           .IsFlag = True
                                           }).ToList

                    '---- check Tolerance Flag ----
                    For Each usr In groupByUser
                        Dim check = (From elm In getListForApprove
                                     Where elm.price.SendBy = usr.UserID _
                                     And elm.price.ToleranceReason IsNot Nothing
                                     Select elm).FirstOrDefault
                        If check IsNot Nothing Then
                            usr.IsFlag = True
                        Else
                            usr.IsFlag = False
                        End If
                    Next


                    Return Ok(New ActionResultModel(g_status_success, "Success", groupByUser))
                End Using



            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function

        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/getProductsForApprove")>
        <EnableCors("*", "*", "*")>
        Public Function getProductsForApprove(DeviceID As String, ApproverID As Integer, UserID As Integer) As IHttpActionResult
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                              Pub_UserId, Pub_Password)

                    Dim _CurDate As Date = Util.BkkNow

                    '-- get user relate 
                    'Dim _userRelate As List(Of Integer) = (From elm In dbContext.UserRelated
                    '                                       Where elm.SuperUserID = UserID
                    '                                       Select elm.StaffUserID).ToList


                    Dim getListForApprove = (From prc In dbContext.UpdatePricing
                                             Let cpt = (From c In dbContext.Competitors
                                                        Where c.ROWID = prc.CompetitorID
                                                        Select c).FirstOrDefault
                                             Where prc.IsSend = True And prc.SendBy = ApproverID _
                                             And prc.IsSend = True _
                                             And (prc.SendDate IsNot Nothing AndAlso DbFunctions.TruncateTime(prc.SendDate) = _CurDate.Date)
                                             Select prc, cpt Order By prc.SendDate Descending).ToList

                    'And (prc.IsApproved Is Nothing OrElse prc.IsApproved = False) _
                    'And (prc.IsRejected Is Nothing OrElse prc.IsRejected = False)

                    Dim result = (From e In getListForApprove
                                  Select New UpdatePricingModel With {
                                      .ROWID = e.prc.ROWID,
                                      .BranchID = e.prc.BranchID,
                                      .TransactDate = e.prc.TransactDate.ToString("dd/MM/yyyy", g_culture_en),
                                                                                                              _
                                      .CompetitorID = e.prc.CompetitorID,
                                      .ProductNo = e.cpt.ProductNo,
                                      .ProductName = e.cpt.ProductName,
                                      .UnitCode = e.cpt.UnitCode,
                                      .Barcode = e.cpt.Barcode,
                                                               _
                                      .NormalPrice = e.cpt.NormalPrice.GetValueOrDefault(0),
                                      .NewPrice = e.prc.NewPrice.GetValueOrDefault(0),
                                      .NewDate = If(e.prc.NewDate Is Nothing, Nothing, e.prc.NewDate.Value.ToString("dd/MM/yyyy", g_culture_en)),
                                      .PromotionPrice = e.prc.PromotionPrice.GetValueOrDefault(0),
                                      .PromotionDate = If(e.prc.PromotionDate Is Nothing, Nothing, e.prc.PromotionDate.Value.ToString("dd/MM/yyyy", g_culture_en)),
                                                                                                                                                                   _
                                      .Remark = e.prc.Remark,
                                      .IsApproved = e.prc.IsApproved.GetValueOrDefault(False),
                                      .ApprovedDate = If(e.prc.ApprovedDate Is Nothing, Nothing, e.prc.ApprovedDate.Value.ToString("dd/MM/yyyy", g_culture_en)),
                                      .ApprovedBy = If(e.prc.ApprovedBy, 0),
                                      .IsRejected = e.prc.IsRejected.GetValueOrDefault(False),
                                      .RejectedDate = If(e.prc.RejectedDate Is Nothing, Nothing, e.prc.RejectedDate.Value.ToString("dd/MM/yyyy", g_culture_en)),
                                      .RejectedBy = If(e.prc.RejectedBy, 0),
                                      .IsActive = e.prc.IsActive.GetValueOrDefault(False),
                                      .CreateBy = If(e.prc.CreateBy, 0),
                                      .ToleranceReason = e.prc.ToleranceReason
                                      }).ToList

                        Return Ok(New ActionResultModel(g_status_success, "Success", result))
                End Using



            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function

        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/saveApprovePricing")>
        <EnableCors("*", "*", "*")>
        Public Function saveApprovePricing(DeviceID As String, UserID As Integer, RowIds As List(Of Int64)) As IHttpActionResult
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                Dim _CurDate As Date = Util.BkkNow
                Dim _UserTask As Integer = 0

                If RowIds Is Nothing OrElse RowIds.Count = 0 Then
                    Return Ok(New ActionResultModel(g_status_fail, "ไม่พบข้อมูลสำหรับการบันทึก", Nothing))
                End If


                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                              Pub_UserId, Pub_Password)

                    For Each _rowId As Int64 In RowIds
                        Dim pricingUpdate = (From elm In dbContext.UpdatePricing
                                             Where elm.ROWID = _rowId
                                             Select elm).FirstOrDefault
                        If pricingUpdate IsNot Nothing Then
                            pricingUpdate.IsApproved = True
                            pricingUpdate.ApprovedDate = _CurDate
                            pricingUpdate.ApprovedBy = UserID

                            pricingUpdate.LastUpdateDate = _CurDate
                            pricingUpdate.LastUpdateBy = UserID
                            _UserTask = pricingUpdate.CreateBy ' --- get user task
                        End If
                    Next

                    dbContext.SaveChanges()

                End Using

                '================== NOTI ==================
                Dim objSaveNotify As New NotificationModel
                With objSaveNotify
                    .NotifyDate = _CurDate.Date
                    .NotifyType = g_notifyType_PRICING
                    .NotifySubject = "Pricing"
                    .NotifyDescription = "ยืนยันราคาแล้ว"
                    .NotifyNumber = RowIds.Count
                    .NotifyToUserID = _UserTask
                    .NotifyToBranchID = 0
                    .NotifyToStoreID = 0
                    .IsRead = False
                    .ReadDate = Nothing
                    .CreateDate = _CurDate
                    .CreateBy = UserID
                    .LastUpdateDate = _CurDate
                    .LastUpdateBy = UserID
                End With

                Dim noti As New NotificationsController
                noti.saveNotification(DeviceID, UserID, objSaveNotify)
                '=============================================

                Return Ok(New ActionResultModel(g_status_success, "Success", Nothing))

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function

        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/rejectPricing")>
        <EnableCors("*", "*", "*")>
        Public Function rejectPricing(DeviceID As String, UserID As Integer, RowIds As List(Of Int64)) As IHttpActionResult
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                Dim _CurDate As Date = Util.BkkNow
                Dim _UserTask As Integer = 0
                Dim _ProductText As New StringBuilder

                If RowIds Is Nothing OrElse RowIds.Count = 0 Then
                    Return Ok(New ActionResultModel(g_status_fail, "ไม่พบข้อมูลสำหรับการบันทึก", Nothing))
                End If


                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                              Pub_UserId, Pub_Password)

                    For Each _rowId As Int64 In RowIds
                        Dim pricingUpdate = (From elm In dbContext.UpdatePricing
                                             Where elm.ROWID = _rowId
                                             Select elm).FirstOrDefault
                        If pricingUpdate IsNot Nothing Then
                            pricingUpdate.IsRejected = True
                            pricingUpdate.RejectedDate = _CurDate
                            pricingUpdate.RejectedBy = UserID

                            pricingUpdate.LastUpdateDate = _CurDate
                            pricingUpdate.LastUpdateBy = UserID

                            _UserTask = pricingUpdate.CreateBy ' --- get user task

                            '==== notify get product
                            Dim objProd = (From elm In dbContext.Competitors Where elm.ROWID = pricingUpdate.CompetitorID Select elm).FirstOrDefault
                            If objProd IsNot Nothing Then
                                _ProductText.Append("------------")
                                _ProductText.Append(vbNewLine)
                                _ProductText.Append(":  " & objProd.ProductName)
                                _ProductText.Append(vbNewLine)
                                _ProductText.Append(":  รหัส : " & objProd.ProductNo)
                                _ProductText.Append(vbNewLine)
                                _ProductText.Append(":  ราคาใหม่ : " & pricingUpdate.NewPrice.GetValueOrDefault.ToString("#,##0.####"))
                                _ProductText.Append(vbNewLine)
                                _ProductText.Append(":  สิ้นสุดใหม่ : " & pricingUpdate.NewDate.GetValueOrDefault.ToString("dd/MM/yyyy"))
                                _ProductText.Append(vbNewLine)
                            End If
                            '======================
                        End If
                    Next

                    dbContext.SaveChanges()

                End Using

                '================== NOTI ==================
                Dim objSaveNotify As New NotificationModel
                With objSaveNotify
                    .NotifyDate = _CurDate.Date
                    .NotifyType = g_notifyType_REJECT_PRICING
                    .NotifySubject = "Pricing"
                    .NotifyDescription = "ปฏิเสธราคา! " & RowIds.Count.ToString & " รายการ" & vbNewLine & _ProductText.ToString
                    .NotifyNumber = RowIds.Count
                    .NotifyToUserID = _UserTask
                    .NotifyToBranchID = 0
                    .NotifyToStoreID = 0
                    .IsRead = False
                    .ReadDate = Nothing
                    .CreateDate = _CurDate
                    .CreateBy = UserID
                    .LastUpdateDate = _CurDate
                    .LastUpdateBy = UserID
                End With

                Dim noti As New NotificationsController
                noti.saveNotification(DeviceID, UserID, objSaveNotify)
                '=============================================

                Return Ok(New ActionResultModel(g_status_success, "Success", Nothing))

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function

    End Class
End Namespace