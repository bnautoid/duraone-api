﻿Imports System.IO
Imports System.Net
Imports System.Web.Hosting
Imports System.Web.Http
Imports System.Web.Http.Cors
Imports System.Web.Http.Description
Imports System.Security.Cryptography
Imports System.Data.Entity
Imports System.Data.Entity.Core.Objects
Imports System.Diagnostics.Eventing.Reader
Imports RestSharp.Authenticators

Namespace Controllers.DURAONE
    Public Class SalesAndCommissionController
        Inherits ApiController

        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/GetSaleOutBySM")>
        <EnableCors("*", "*", "*")>
        Public Function GetSaleOutBySM(DeviceID As String,
                                         UserID As Integer,
                                         SMCode As String) As IHttpActionResult
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                If Not ModelState.IsValid Then
                    Return BadRequest(ModelState)
                End If

                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                               Pub_UserId, Pub_Password)

                    dbContext.Database.CommandTimeout = 180

                    Dim objSaleOut = (From elm In dbContext.SalesAndComission
                                      Where elm.Region7SM = SMCode
                                      Group By Region7SM = elm.Region7SM
                                               Into CurrentSalesOut = Sum(If(elm.CurrentSalesOut, 0)),
                                               FinalForecast = Sum(If(elm.FinalForecast, 0)),
                                               SaleOutEstActual = Sum(If(elm.EstimateSalesOutEndMonth, 0)),
                                               CommissionAmount = Sum(If(elm.ComissionAmount, 0))
                                      Select New SaleOutAndCommissionModel With {
                                               .Region7SM = Region7SM,
                                               .BranchCustCode = Nothing,
                                               .BranchID = 0,
                                               .BranchName = Nothing,
                                               .ProductGroup4 = Nothing,
                                               .FinalForecast = FinalForecast,
                                               .CurrentSalesOut = CurrentSalesOut,
                                               .SalesOutPercent = If(FinalForecast = 0, 0, (CurrentSalesOut / FinalForecast) * 100),
                                               .SaleOutDiff = CurrentSalesOut - FinalForecast,
                                               .SaleOutEstActual = SaleOutEstActual,
                                               .SaleOutEstDiff = SaleOutEstActual - FinalForecast,
                                               .CommissionRate = 0,
                                               .CommissionAmount = CommissionAmount
                                               }).FirstOrDefault

                    Return Ok(New ActionResultModel(g_status_success, "success", objSaleOut))
                End Using

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function

        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/GetSaleOutByBranches")>
        <EnableCors("*", "*", "*")>
        Public Function GetSaleOutByBranches(DeviceID As String,
                                         UserID As Integer,
                                         SMCode As String,
                                         BranchIds As List(Of Integer)) As IHttpActionResult
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                If Not ModelState.IsValid Then
                    Return BadRequest(ModelState)
                End If

                Dim result As New List(Of SaleOutAndCommissionModel)

                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                               Pub_UserId, Pub_Password)

                    dbContext.Database.CommandTimeout = 180

                    For Each BranchId In BranchIds

                        Dim _branchCustCode As String = Nothing
                        Dim _branchId As Integer = 0
                        Dim _branchName As String = Nothing

                        Dim objBrach = (From elm In dbContext.Branches
                                        Where elm.BranchID = BranchId
                                        Select elm).FirstOrDefault
                        If BranchId <> 0 And objBrach Is Nothing Then
                            Return Ok(New ActionResultModel(g_status_fail, "ไม่พบข้อมูลสาขา (ID: " & BranchId.ToString & ")", Nothing))
                        Else
                            _branchId = objBrach.BranchID
                            _branchName = objBrach.BranchName
                            _branchCustCode = objBrach.CustCode
                        End If

                        Dim objSaleOutAndCommission = (From elm In dbContext.SalesAndComission
                                                       Where elm.Region7SM = SMCode And elm.CustomerCode = _branchCustCode
                                                       Group By elm.Region7SM, elm.CustomerCode
                                               Into CurrentSalesOut = Sum(If(elm.CurrentSalesOut, 0)),
                                               FinalForecast = Sum(If(elm.FinalForecast, 0)),
                                               SaleOutEstActual = Sum(If(elm.EstimateSalesOutEndMonth, 0)),
                                               CommissionAmount = Sum(If(elm.ComissionAmount, 0))
                                                       Select New SaleOutAndCommissionModel With {
                                               .Region7SM = Region7SM,
                                               .BranchCustCode = _branchCustCode,
                                               .BranchID = _branchId,
                                               .BranchName = _branchName,
                                               .ProductGroup4 = Nothing,
                                               .FinalForecast = FinalForecast,
                                               .CurrentSalesOut = CurrentSalesOut,
                                               .SalesOutPercent = If(FinalForecast = 0, 0, (CurrentSalesOut / FinalForecast) * 100),
                                               .SaleOutDiff = CurrentSalesOut - FinalForecast,
                                               .SaleOutEstActual = SaleOutEstActual,
                                               .SaleOutEstDiff = SaleOutEstActual - FinalForecast,
                                               .CommissionRate = 0,
                                               .CommissionAmount = CommissionAmount
                                               }).FirstOrDefault

                        If objSaleOutAndCommission IsNot Nothing Then
                            result.Add(objSaleOutAndCommission)
                        End If

                    Next

                    Return Ok(New ActionResultModel(g_status_success, "success", result))

                End Using

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function

        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/GetSaleOutByBranch")>
        <EnableCors("*", "*", "*")>
        Public Function GetSaleOutByBranch(DeviceID As String,
                                         UserID As Integer,
                                         SMCode As String,
                                         BranchId As Integer) As IHttpActionResult
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                If Not ModelState.IsValid Then
                    Return BadRequest(ModelState)
                End If

                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                               Pub_UserId, Pub_Password)

                    dbContext.Database.CommandTimeout = 180

                    Dim _branchCustCode As String = Nothing
                    Dim _branchId As Integer = 0
                    Dim _branchName As String = Nothing

                    Dim objBrach = (From elm In dbContext.Branches
                                    Where elm.BranchID = BranchId
                                    Select elm).FirstOrDefault
                    If BranchId <> 0 And objBrach Is Nothing Then
                        Return Ok(New ActionResultModel(g_status_fail, "ไม่พบข้อมูลสาขา (ID: " & BranchId.ToString & ")", Nothing))
                    Else
                        _branchId = objBrach.BranchID
                        _branchName = objBrach.BranchName
                        _branchCustCode = objBrach.CustCode
                    End If


                    Dim objSaleOut = (From elm In dbContext.SalesAndComission
                                      Where elm.Region7SM = SMCode _
                                               And elm.CustomerCode = _branchCustCode
                                      Group By elm.Region7SM, elm.CustomerCode
                                               Into CurrentSalesOut = Sum(If(elm.CurrentSalesOut, 0)),
                                               FinalForecast = Sum(If(elm.FinalForecast, 0)),
                                               SaleOutEstActual = Sum(If(elm.EstimateSalesOutEndMonth, 0)),
                                               CommissionAmount = Sum(If(elm.ComissionAmount, 0))
                                      Select New SaleOutAndCommissionModel With {
                                               .Region7SM = Region7SM,
                                               .BranchCustCode = _branchCustCode,
                                               .BranchID = _branchId,
                                               .BranchName = _branchName,
                                               .ProductGroup4 = Nothing,
                                               .FinalForecast = FinalForecast,
                                               .CurrentSalesOut = CurrentSalesOut,
                                               .SalesOutPercent = If(FinalForecast = 0, 0, (CurrentSalesOut / FinalForecast) * 100),
                                               .SaleOutDiff = CurrentSalesOut - FinalForecast,
                                               .SaleOutEstActual = SaleOutEstActual,
                                               .SaleOutEstDiff = SaleOutEstActual - FinalForecast,
                                               .CommissionRate = 0,
                                               .CommissionAmount = CommissionAmount
                                               }).FirstOrDefault

                    Return Ok(New ActionResultModel(g_status_success, "success", objSaleOut))
                End Using

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function

        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/GetSaleOutByGroup4")>
        <EnableCors("*", "*", "*")>
        Public Function GetSaleOutByGroup4(DeviceID As String,
                                       UserID As Integer,
                                       SMCode As String,
                                       BranchId As Integer,
                                       Group4SubCate As String) As IHttpActionResult
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                If Not ModelState.IsValid Then
                    Return BadRequest(ModelState)
                End If

                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                               Pub_UserId, Pub_Password)

                    dbContext.Database.CommandTimeout = 180

                    Dim _branchCustCode As String = Nothing
                    Dim _branchId As Integer = 0
                    Dim _branchName As String = Nothing

                    If BranchId <> 0 Then
                        Dim objBrach = (From elm In dbContext.Branches
                                        Where elm.BranchID = BranchId
                                        Select elm).FirstOrDefault
                        If objBrach Is Nothing Then
                            Return Ok(New ActionResultModel(g_status_fail, "ไม่พบข้อมูลสาขา (ID: " & BranchId.ToString & ")", Nothing))
                        Else
                            _branchId = objBrach.BranchID
                            _branchName = objBrach.BranchName
                            _branchCustCode = objBrach.CustCode
                        End If
                    End If



                    Dim objSaleOut = (From elm In dbContext.SalesAndComission
                                      Where elm.Region7SM = SMCode _
                                               And If(_branchCustCode Is Nothing, True, elm.CustomerCode = _branchCustCode) _
                                               And elm.Group4Group = Group4SubCate
                                      Group By elm.Region7SM, elm.CustomerCode, elm.Group4Group
                                               Into CurrentSalesOut = Sum(If(elm.CurrentSalesOut, 0)),
                                               FinalForecast = Sum(If(elm.FinalForecast, 0)),
                                               SaleOutEstActual = Sum(If(elm.EstimateSalesOutEndMonth, 0)),
                                               CommissionAmount = Sum(If(elm.ComissionAmount, 0))
                                      Select New SaleOutAndCommissionModel With {
                                               .Region7SM = Region7SM,
                                               .BranchCustCode = _branchCustCode,
                                               .BranchID = _branchId,
                                               .BranchName = _branchName,
                                               .ProductGroup4 = Nothing,
                                               .FinalForecast = FinalForecast,
                                               .CurrentSalesOut = CurrentSalesOut,
                                               .SalesOutPercent = If(FinalForecast = 0, 0, (CurrentSalesOut / FinalForecast) * 100),
                                               .SaleOutDiff = CurrentSalesOut - FinalForecast,
                                               .SaleOutEstActual = SaleOutEstActual,
                                               .SaleOutEstDiff = SaleOutEstActual - FinalForecast,
                                               .CommissionRate = 0,
                                               .CommissionAmount = CommissionAmount
                                               }).FirstOrDefault

                    Return Ok(New ActionResultModel(g_status_success, "success", objSaleOut))
                End Using

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function


        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/GetCommissionByBranch")>
        <EnableCors("*", "*", "*")>
        Public Function GetCommissionByBranch(DeviceID As String,
                                         UserID As Integer,
                                         SMCode As String,
                                         BranchId As Integer) As IHttpActionResult
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                If Not ModelState.IsValid Then
                    Return BadRequest(ModelState)
                End If

                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                               Pub_UserId, Pub_Password)

                    dbContext.Database.CommandTimeout = 180

                    Dim _branchCustCode As String = Nothing
                    Dim _branchId As Integer = 0
                    Dim _branchName As String = Nothing

                    Dim objBrach = (From elm In dbContext.Branches
                                    Where elm.BranchID = BranchId
                                    Select elm).FirstOrDefault
                    If BranchId <> 0 And objBrach Is Nothing Then
                        Return Ok(New ActionResultModel(g_status_fail, "ไม่พบข้อมูลสาขา (ID: " & BranchId.ToString & ")", Nothing))
                    Else
                        _branchId = objBrach.BranchID
                        _branchName = objBrach.BranchName
                        _branchCustCode = objBrach.CustCode
                    End If

                    'Let cat = (From c In dbContext.Categories Where c.CategoryCode = elm.Group4Group Select c).FirstOrDefault
                    'elm.Group4Group, Group4Name = If(cat Is Nothing, "-", cat.CategoryName),

                    'Where elm.Region7SM = SMCode And elm.CustomerCode = _branchCustCode And If(elm.ComissionAmount, 0) > 0

                    Dim objSaleOut = (From elm In dbContext.SalesAndComission
                                      Where elm.Region7SM = SMCode And elm.CustomerCode = _branchCustCode
                                      Group By elm.Region7SM, elm.CustomerCode,
                                          elm.Group4Group,
                                          commissionRate = If(elm.Commission, 0)
                                               Into CurrentSalesOut = Sum(If(elm.CurrentSalesOut, 0)),
                                               FinalForecast = Sum(If(elm.FinalForecast, 0)),
                                               SaleOutEstActual = Sum(If(elm.EstimateSalesOutEndMonth, 0)),
                                               CommissionAmount = Sum(If(elm.ComissionAmount, 0))
                                      Select New SaleOutAndCommissionModel With {
                                               .Region7SM = Region7SM,
                                               .BranchCustCode = _branchCustCode,
                                               .BranchID = _branchId,
                                               .BranchName = _branchName,
                                               .ProductGroup4 = Group4Group,
                                               .ProductGroup4Name = Nothing,
                                               .FinalForecast = FinalForecast,
                                               .CurrentSalesOut = CurrentSalesOut,
                                               .SalesOutPercent = If(FinalForecast = 0, 0, (CurrentSalesOut / FinalForecast) * 100),
                                               .SaleOutDiff = CurrentSalesOut - FinalForecast,
                                               .SaleOutEstActual = SaleOutEstActual,
                                               .SaleOutEstDiff = SaleOutEstActual - FinalForecast,
                                               .CommissionRate = commissionRate,
                                               .CommissionAmount = CommissionAmount
                                               }).ToList

                    objSaleOut = objSaleOut.OrderBy(Function(e) e.ProductGroup4).ToList

                    Return Ok(New ActionResultModel(g_status_success, "success", objSaleOut))
                End Using

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function

        '<HttpPost()>
        '<ResponseType(GetType(IHttpActionResult))>
        '<Route("api/GetCommissionByGroup3")>
        '<EnableCors("*", "*", "*")>
        'Public Function GetCommissionByGroup3(DeviceID As String,
        '                            UserID As Integer,
        '                            SMCode As String,
        '                            BranchId As Integer,
        '                            Group3 As String) As IHttpActionResult
        '    Try
        '        '======== CHECK USE DEVICE ===========
        '        Dim msgChecked = Util.CheckUserDevice(DeviceID, UserID)
        '        If msgChecked.resultcode = g_status_userdevice_fail Then
        '            Return Ok(msgChecked)
        '        End If
        '        '=====================================

        '        If Not ModelState.IsValid Then
        '            Return BadRequest(ModelState)
        '        End If

        '        Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
        '                                                       Pub_UserId, Pub_Password)

        '            Dim _branchCustCode As String = Nothing
        '            Dim _branchId As Integer = 0
        '            Dim _branchName As String = Nothing

        '            If BranchId <> 0 Then
        '                Dim objBrach = (From elm In dbContext.Branches
        '                                Where elm.BranchID = BranchId
        '                                Select elm).FirstOrDefault
        '                If objBrach Is Nothing Then
        '                    Return Ok(New ActionResultModel(g_status_fail, "ไม่พบข้อมูลสาขา (ID: " & BranchId.ToString & ")", Nothing))
        '                Else
        '                    _branchId = objBrach.BranchID
        '                    _branchName = objBrach.BranchName
        '                    _branchCustCode = objBrach.CustCode
        '                End If
        '            End If

        '            Dim objSaleOut = (From elm In dbContext.SalesAndComission
        '                              Where elm.Region7SM = SMCode _
        '                                       And If(_branchCustCode Is Nothing, True, elm.CustomerCode = _branchCustCode) _
        '                                       And elm.Group3Group = Group3
        '                              Group By elm.Region7SM, elm.CustomerCode, elm.Group3Group, Commission = If(elm.Commission, 0)
        '                                       Into CurrentSalesOut = Sum(If(elm.CurrentSalesOut, 0)),
        '                                       FinalForecast = Sum(If(elm.FinalForecast, 0)),
        '                                       SaleOutEstActual = Sum(If(elm.EstimateSalesOutEndMonth, 0)),
        '                                       CommissionAmount = Sum(If(elm.ComissionAmount, 0))
        '                              Select New SaleOutAndCommissionModel With {
        '                                       .Region7SM = Region7SM,
        '                                       .BranchCustCode = _branchCustCode,
        '                                       .BranchID = _branchId,
        '                                       .BranchName = _branchName,
        '                                       .ProductGroup3 = Group3,
        '                                       .FinalForecast = FinalForecast,
        '                                       .CurrentSalesOut = CurrentSalesOut,
        '                                       .SalesOutPercent = If(FinalForecast = 0, 0, (CurrentSalesOut / FinalForecast) * 100),
        '                                       .SaleOutDiff = FinalForecast - CurrentSalesOut,
        '                                       .SaleOutEstActual = SaleOutEstActual,
        '                                       .SaleOutEstDiff = SaleOutEstActual - FinalForecast,
        '                                       .CommissionRate = Commission,
        '                                       .CommissionAmount = CommissionAmount
        '                                       }).FirstOrDefault

        '            Return Ok(New ActionResultModel(g_status_success, "success", objSaleOut))
        '        End Using

        '    Catch ex As Exception
        '        Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
        '    End Try
        'End Function


    End Class
End Namespace