﻿Imports System.IO
Imports System.Net
Imports System.Web.Hosting
Imports System.Web.Http
Imports System.Web.Http.Cors
Imports System.Web.Http.Description
Imports System.Security.Cryptography

Namespace Controllers
    Public Class LoginController
        Inherits ApiController

        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/GetUserAuthorize")>
        <EnableCors("*", "*", "*")>
        Public Function GetUserAuthorize(DeviceID As String,
                                         DeviceName As String,
                                         usercode As String,
                                         password As String) As IHttpActionResult
            Try

                If Not ModelState.IsValid Then
                    Return BadRequest(ModelState)
                End If

                If String.IsNullOrEmpty(DeviceID) Then
                    Return Ok(New ActionResultModel(g_status_fail, "device Id is Required", Nothing))
                End If

                If String.IsNullOrEmpty(usercode) Then
                    Return Ok(New ActionResultModel(g_status_fail, "User Code is Required", Nothing))
                End If



                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                               Pub_UserId, Pub_Password)


                    Dim UsersInfo = (From elm In dbContext.Users
                                     Where elm.IsActive = True _
                                         And elm.UserCode = usercode
                                     Select elm).FirstOrDefault

                    If UsersInfo Is Nothing Then
                        Return Ok(New ActionResultModel(g_status_fail, "User Not found", Nothing))
                    Else

                        If Not Me.ValidatePlanTextPassword(UsersInfo.MD5Password, password) Then
                            Return Ok(New ActionResultModel(g_status_fail, "Password not match", Nothing))
                        End If

                        '====== CONVERT TO DATA MODEL
                        Dim userResult = Me.userToDataModel(UsersInfo)


                        '======== CHECK USE DEVICE ===========
                        Dim msgChecked = Util.CheckUserDevice(DeviceID, UsersInfo.UserID)
                        If msgChecked.resultcode = g_status_userdevice_fail Then
                            Return Ok(New ActionResultModel(msgChecked.resultcode, msgChecked.message, userResult))
                        End If
                        '=====================================

                        '------ update Device Id ---
                        If UsersInfo.DeviceID Is Nothing Then
                            UsersInfo.DeviceID = DeviceID
                            UsersInfo.DeviceName = DeviceName
                            dbContext.SaveChanges()
                        End If


                        Return Ok(New ActionResultModel(g_status_success, "success", userResult))
                    End If

                End Using

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function


        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/LoginUpdateUserDevice")>
        <EnableCors("*", "*", "*")>
        Public Function LoginUpdateUserDevice(UserID As Integer,
                                         DeviceID As String,
                                         DeviceName As String) As IHttpActionResult
            Try

                If Not ModelState.IsValid Then
                    Return BadRequest(ModelState)
                End If

                Dim _userId = UserID
                Dim _deviceId = DeviceID
                Dim _deviceName = DeviceName


                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                               Pub_UserId, Pub_Password)


                    Dim UsersInfo = (From elm In dbContext.Users
                                     Where elm.UserID = _userId
                                     Select elm).FirstOrDefault
                    If UsersInfo IsNot Nothing Then
                        UsersInfo.DeviceID = _deviceId
                        UsersInfo.DeviceName = _deviceName

                        dbContext.SaveChanges()
                    End If

                    '====== CONVERT TO DATA MODEL
                    Dim userResult = Me.userToDataModel(UsersInfo)

                    Return Ok(New ActionResultModel(g_status_success, "success", userResult))


                End Using

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function

        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/LogoutUserDevice")>
        <EnableCors("*", "*", "*")>
        Public Function LogoutUserDevice(UserID As Integer) As IHttpActionResult
            Try

                If Not ModelState.IsValid Then
                    Return BadRequest(ModelState)
                End If

                Dim _userId = UserID


                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                               Pub_UserId, Pub_Password)


                    Dim UsersInfo = (From elm In dbContext.Users
                                     Where elm.UserID = _userId
                                     Select elm).FirstOrDefault
                    If UsersInfo IsNot Nothing Then
                        UsersInfo.DeviceID = Nothing
                        UsersInfo.DeviceName = Nothing

                        dbContext.SaveChanges()
                    End If


                    Return Ok(New ActionResultModel(g_status_success, "success", Nothing))


                End Using

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function

        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/UpdateFCMToken")>
        <EnableCors("*", "*", "*")>
        Public Function UpdateFCMToken(UserID As Integer, FCMToken As String) As IHttpActionResult
            Try

                If Not ModelState.IsValid Then
                    Return BadRequest(ModelState)
                End If

                Dim _userId = UserID
                Dim _fcm_token = FCMToken

                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                               Pub_UserId, Pub_Password)

                    '-- clear fcm other user if login multiple devices
                    Dim UsersInfo = (From elm In dbContext.Users
                                     Where elm.FCMToken = _fcm_token
                                     Select elm).ToList
                    For Each us In UsersInfo
                        us.FCMToken = Nothing
                        dbContext.SaveChanges()
                    Next

                    '-- update fcm token
                    Dim UpdateUser = (From elm In dbContext.Users
                                      Where elm.UserID = _userId
                                      Select elm).FirstOrDefault
                    If UpdateUser IsNot Nothing Then
                        UpdateUser.FCMToken = _fcm_token
                        dbContext.SaveChanges()
                    End If

                    Return Ok(New ActionResultModel(g_status_success, "success", Nothing))
                End Using

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function

        Private Function userToDataModel(UsersInfo As Users) As UserLogonModel
            Try
                Using dbContext = ConnectionHelper.CreateDURAConnection(Pub_metaData, Pub_dataSource, Pub_initialCatalog,
                                                               Pub_UserId, Pub_Password)

                    Dim userBranches = (From bran In dbContext.UserBranch
                                        Where bran.UserID = UsersInfo.UserID
                                        Order By bran.Branches.BranchName
                                        Select New BranchModel With {
                                                                             .StoreID = bran.Branches.StoreID,
                                                                             .BranchID = bran.BranchID,
                                                                             .BranchName = bran.Branches.BranchName,
                                                                             .Address = bran.Branches.Address1,
                                                                             .Region = bran.Branches.Region,
                                                                             .Province = bran.Branches.Province,
                                                                            .SCGLSoldTo = bran.Branches.SCGLSoldTo,
                                                                            .SCGLLocation = bran.Branches.SCGLLocation,
                                                                            .CustCode = bran.Branches.CustCode,
                                                                            .Region7SM = bran.Branches.Region7_SM,
                                                                            .ShipTo = bran.Branches.ShipTo
                                                                         }).ToList

                    Dim userRoles = (From role In dbContext.UserRoles
                                     Let appMenu = (From app In dbContext.AppMenu Where app.MenuID = role.MenuID Select app).FirstOrDefault
                                     Where role.RoleID = UsersInfo.RoleID And role.IsVisibled = True
                                     Select New UserRoleModel With {
                                                                             .RoleID = role.RoleID,
                                                                             .MenuID = role.MenuID,
                                                                             .MenuName = If(appMenu Is Nothing, "", appMenu.MenuName),
                                                                             .IsVisibled = If(role.IsVisibled, False)
                                                                         }).ToList

                    Dim userResult As New UserLogonModel With {
                                                .UserID = UsersInfo.UserID,
                                                .UserCode = UsersInfo.UserCode,
                                                .MD5Password = UsersInfo.MD5Password,
                                                .PlainTextPassword = "",
                                                .FirstName = UsersInfo.FirstName,
                                                .LastName = UsersInfo.LastName,
                                                .FullName = UsersInfo.FirstName & " " & UsersInfo.LastName,
                                                .Title = UsersInfo.Title,
                                                .FileName = UsersInfo.FirstName,
                                                .RoleCode = UsersInfo.Roles.RoleCode,
                                                .Email = UsersInfo.Email,
                                                .Tel = UsersInfo.Tel,
                                                .RoleID = UsersInfo.RoleID,
                                                .RoleName = UsersInfo.Roles.RoleName,
                                                .ByteImage = Nothing,
                                                .BranchReponsibility = userBranches,
                                                .UserRole = userRoles
                                                   }

                    If Not String.IsNullOrWhiteSpace(UsersInfo.ImagePath) Then
                        userResult.ByteImage = Me.GetUserImage(userResult.FileName)
                    End If


                    'commented 
                    'Dim lastUpdateDate = UsersInfo.LastUpdateDate.GetValueOrDefault(UsersInfo.CreateDate.GetValueOrDefault(BkkNow())).Date
                    'Dim diffDay = DateDiff(DateInterval.Day, lastUpdateDate, BkkNow())
                    'If diffDay > 90 Then
                    '    userResult.IsExpired = True
                    'Else
                    '    userResult.IsExpired = False
                    'End If

                    Return userResult
                End Using

            Catch ex As Exception
                Throw ex
            End Try
        End Function

#Region "Private Class"
        Private Function ValidatePlanTextPassword(HashPassword As String, PlanTextPassword As String) As Boolean
            Dim bOK As Boolean = False
            Try
                Dim source As String = PlanTextPassword
                Dim _md5 As New MD5Hash.MD5Hashing

                Using md5Hash As MD5 = MD5.Create()
                    Dim hash As String = _md5.GetMd5Hash(md5Hash, source)

                    If _md5.VerifyMd5Hash(md5Hash, source, HashPassword) Then
                        bOK = True
                    End If

                End Using

                Return bOK
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetUserImage(FileName As String) As Byte()
            Try
                Dim rootPath = HostingEnvironment.MapPath("~/images/usr/")
                Dim ImageObj As System.Drawing.Image
                Dim filepath = Path.Combine(rootPath, FileName)

                If Not File.Exists(filepath) Then
                    Return Nothing
                End If

                ImageObj = System.Drawing.Image.FromFile(filepath)

                'create the actual thumbnail image
                Dim thumbnailImage As System.Drawing.Image
                thumbnailImage = ImageObj.GetThumbnailImage(100, 100,
                                                            New System.Drawing.Image.GetThumbnailImageAbort(AddressOf ThumbnailCallback),
                                                            IntPtr.Zero)

                'make a memory stream to work with the image bytes
                Dim imageStream As New MemoryStream

                'put the image into the memory stream
                thumbnailImage.Save(imageStream, System.Drawing.Imaging.ImageFormat.Jpeg)
                'thumbnailImage.Save(rootPath & "test.jpg")

                'make byte array the same size as the image
                Dim imageContent As Byte() = New Byte(imageStream.Length - 1) {}

                'rewind the memory stream
                imageStream.Position = 0

                'load the byte array with the image
                imageStream.Read(imageContent, 0, imageStream.Length)

                Dim filedata = imageContent     'File.ReadAllBytes(filepath)

                If filedata Is Nothing Then
                    Return Nothing
                End If

                Return filedata

            Catch ex As Exception
                Return Nothing
            End Try
        End Function
        Public Function GetAsset100pxImage(FileName As String) As Byte()
            Try
                Dim rootPath = HostingEnvironment.MapPath("~/images/asset/")
                Dim ImageObj As System.Drawing.Image
                Dim filepath = Path.Combine(rootPath, FileName)

                If Not File.Exists(filepath) Then
                    Return Nothing
                End If

                ImageObj = System.Drawing.Image.FromFile(filepath)

                'create the actual thumbnail image
                Dim thumbnailImage As System.Drawing.Image
                thumbnailImage = ImageObj.GetThumbnailImage(100, 100,
                                                            New System.Drawing.Image.GetThumbnailImageAbort(AddressOf ThumbnailCallback),
                                                            IntPtr.Zero)

                'make a memory stream to work with the image bytes
                Dim imageStream As New MemoryStream

                'put the image into the memory stream
                thumbnailImage.Save(imageStream, System.Drawing.Imaging.ImageFormat.Jpeg)
                'thumbnailImage.Save(rootPath & "test.jpg")

                'make byte array the same size as the image
                Dim imageContent As Byte() = New Byte(imageStream.Length - 1) {}

                'rewind the memory stream
                imageStream.Position = 0

                'load the byte array with the image
                imageStream.Read(imageContent, 0, imageStream.Length)

                Dim filedata = imageContent     'File.ReadAllBytes(filepath)

                If filedata Is Nothing Then
                    Return Nothing
                End If

                Return filedata

            Catch ex As Exception
                Return Nothing
            End Try
        End Function
        Public Function GetAssetFullImage(FileName As String) As Byte()
            Try
                Dim rootPath = HostingEnvironment.MapPath("~/images/asset/")
                Dim ImageObj As System.Drawing.Image
                Dim filepath = Path.Combine(rootPath, FileName)

                If Not File.Exists(filepath) Then
                    Return Nothing
                End If

                ImageObj = System.Drawing.Image.FromFile(filepath)

                ''create the actual thumbnail image
                'Dim thumbnailImage As System.Drawing.Image
                'thumbnailImage = ImageObj.GetThumbnailImage(100, 100,
                '                                            New System.Drawing.Image.GetThumbnailImageAbort(AddressOf ThumbnailCallback),
                '                                            IntPtr.Zero)

                'make a memory stream to work with the image bytes
                Dim imageStream As New MemoryStream

                'put the image into the memory stream
                ImageObj.Save(imageStream, System.Drawing.Imaging.ImageFormat.Jpeg)
                'thumbnailImage.Save(rootPath & "test.jpg")

                'make byte array the same size as the image
                Dim imageContent As Byte() = New Byte(imageStream.Length - 1) {}

                'rewind the memory stream
                imageStream.Position = 0

                'load the byte array with the image
                imageStream.Read(imageContent, 0, imageStream.Length)

                Dim filedata = imageContent     'File.ReadAllBytes(filepath)

                If filedata Is Nothing Then
                    Return Nothing
                End If

                Return filedata

            Catch ex As Exception
                Return Nothing
            End Try
        End Function
        Public Function GetSignatureFullImage(FileName As String) As Byte()
            Try
                Dim rootPath = HostingEnvironment.MapPath("~/images/signature/")
                Dim ImageObj As System.Drawing.Image
                Dim filepath = Path.Combine(rootPath, FileName)

                If Not File.Exists(filepath) Then
                    Return Nothing
                End If

                ImageObj = System.Drawing.Image.FromFile(filepath)

                'make a memory stream to work with the image bytes
                Dim imageStream As New MemoryStream

                'put the image into the memory stream
                ImageObj.Save(imageStream, System.Drawing.Imaging.ImageFormat.Jpeg)
                'thumbnailImage.Save(rootPath & "test.jpg")

                'make byte array the same size as the image
                Dim imageContent As Byte() = New Byte(imageStream.Length - 1) {}

                'rewind the memory stream
                imageStream.Position = 0

                'load the byte array with the image
                imageStream.Read(imageContent, 0, imageStream.Length)

                Dim filedata = imageContent     'File.ReadAllBytes(filepath)

                If filedata Is Nothing Then
                    Return Nothing
                End If

                Return filedata

            Catch ex As Exception
                Return Nothing
            End Try
        End Function
        Public Function ThumbnailCallback() As Boolean
            Try
                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function

#End Region

    End Class

End Namespace