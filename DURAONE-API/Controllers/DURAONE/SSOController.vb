﻿Imports System.IO
Imports System.Net
Imports System.Web.Hosting
Imports System.Web.Http
Imports System.Web.Http.Cors
Imports System.Web.Http.Description
Imports System.Security.Cryptography
Imports System.Threading.Tasks
Imports System.Net.Http
Imports System.Web.Script.Serialization
Imports RestSharp
Imports Microsoft.IdentityModel.Tokens
Imports System.IdentityModel.Tokens.Jwt

Namespace Controllers.DURAONE
    Public Class SSOController
        Inherits ApiController

        <HttpPost()>
        <ResponseType(GetType(IHttpActionResult))>
        <Route("api/SSOHRUrlRedirect")>
        <EnableCors("*", "*", "*")>
        Public Function SSOHRUrlRedirect(param As SSORequestDao) As IHttpActionResult
            Try
                '======== CHECK USE DEVICE ===========
                Dim msgChecked = Util.CheckUserDevice(param.DeviceID, param.UserID)
                If msgChecked.resultcode = g_status_userdevice_fail Then
                    Return Ok(msgChecked)
                End If
                '=====================================

                If Not ModelState.IsValid Then
                    Return BadRequest(ModelState)
                End If

                Dim SSOHRUrl As String = ConfigurationManager.AppSettings("configSSOHRUrl")
                Dim SSOTenant As String = ConfigurationManager.AppSettings("configSSOTenant")
                Dim SSOPrivateKey As String = ConfigurationManager.AppSettings("configSSOPrivateKey")


                Dim securityKey As New Microsoft.IdentityModel.Tokens.SymmetricSecurityKey(Encoding.UTF8.GetBytes(SSOPrivateKey))
                Dim credentials As New Microsoft.IdentityModel.Tokens.SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256)

                Dim header = New JwtHeader(credentials)
                Dim payload = New JwtPayload From {
                                                        {"id", param.UserCode},
                                                        {"name", param.UserName},
                                                        {"email", param.Email}
                                                  }

                Dim secToken = New JwtSecurityToken(header, payload)
                Dim handler = New JwtSecurityTokenHandler()

                Dim tokenString = handler.WriteToken(secToken)
                'Dim token = handler.ReadJwtToken(tokenString)
                'Dim x = token.Payload.First().Value

                '--- url for redirect
                Dim hr_url = SSOHRUrl & "/" & SSOTenant & "/" & tokenString

                Return Ok(New ActionResultModel(g_status_success, "Success", hr_url))

            Catch ex As Exception
                Return Ok(New ActionResultModel(g_status_fail, ex.Message, Nothing))
            End Try
        End Function

        'https://uat.tks.co.th/empeo/handle-custom-sso/01BBDCD0-2A02-4151-8CEE-5503868DE85C/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjAwMzAtMDIyNjE1IiwibmFtZSI6IiIsImVtYWlsIjoicHV1d2FuYXQueUBteWVtcGVvLmNvbSJ9.8V8E4iwHzdQ9c1cPYvX9y5iACk6OeQyB3rM5BmgIkmc

    End Class

    Public Class SSORequestDao
        Public Property DeviceID As String
        Public Property UserID As Integer
        Public Property UserCode As String
        Public Property UserName As String
        Public Property Email As String
    End Class
End Namespace