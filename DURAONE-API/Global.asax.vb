﻿Imports System.Web.Http

Public Class WebApiApplication
    Inherits System.Web.HttpApplication

    Protected Sub Application_Start()
        GlobalConfiguration.Configure(AddressOf WebApiConfig.Register)

        GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
        GlobalConfiguration.Configuration.Formatters.Remove(GlobalConfiguration.Configuration.Formatters.XmlFormatter)


    End Sub

    Protected Sub Application_PreSendRequestHeaders(sender As Object, e As EventArgs)
        HttpContext.Current.Response.Headers.Remove("Server")
        HttpContext.Current.Response.Headers.Remove("X-AspNet-Version")
        HttpContext.Current.Response.Headers.Remove("X-AspNetMvc-Version")
    End Sub

    Protected Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)

        HttpContext.Current.Response.AddHeader("x-frame-options", "DENY")
        'HttpContext.Current.Response.AddHeader("x-frame-options", "SAMEORIGIN")

        If HttpContext.Current.Request.IsSecureConnection.Equals(False) AndAlso HttpContext.Current.Request.IsLocal.Equals(False) Then
            Response.Redirect("https://" & Request.ServerVariables("HTTP_HOST") + HttpContext.Current.Request.RawUrl)
        End If
    End Sub

End Class
