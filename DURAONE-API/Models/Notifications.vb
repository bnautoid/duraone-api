'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated from a template.
'
'     Manual changes to this file may cause unexpected behavior in your application.
'     Manual changes to this file will be overwritten if the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Collections.Generic

Partial Public Class Notifications
    Public Property ROWID As Long
    Public Property NotifyDate As Nullable(Of Date)
    Public Property NotifyType As String
    Public Property NotifySubject As String
    Public Property NotifyDescription As String
    Public Property NotifyToUserID As Nullable(Of Integer)
    Public Property NotifyToBranchID As Nullable(Of Integer)
    Public Property NotifyToStoreID As Nullable(Of Integer)
    Public Property NotifyNumber As Nullable(Of Integer)
    Public Property IsRead As Nullable(Of Boolean)
    Public Property ReadDate As Nullable(Of Date)
    Public Property CreateDate As Nullable(Of Date)
    Public Property CreateBy As Nullable(Of Integer)
    Public Property LastUpdateDate As Nullable(Of Date)
    Public Property LastUpdateBy As Nullable(Of Integer)

End Class
