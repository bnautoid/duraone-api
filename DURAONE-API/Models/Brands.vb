'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated from a template.
'
'     Manual changes to this file may cause unexpected behavior in your application.
'     Manual changes to this file will be overwritten if the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Collections.Generic

Partial Public Class Brands
    Public Property BrandID As Integer
    Public Property BrandCode As String
    Public Property BrandName As String
    Public Property BrandImageFileName As String
    Public Property IsActive As Nullable(Of Boolean)
    Public Property Remark As String
    Public Property CreateDate As Nullable(Of Date)
    Public Property CreateBy As Nullable(Of Integer)
    Public Property LastUpdateDate As Nullable(Of Date)
    Public Property LastUpdateBy As Nullable(Of Integer)

    Public Overridable Property Competitors As ICollection(Of Competitors) = New HashSet(Of Competitors)
    Public Overridable Property Products As ICollection(Of Products) = New HashSet(Of Products)

End Class
