'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated from a template.
'
'     Manual changes to this file may cause unexpected behavior in your application.
'     Manual changes to this file will be overwritten if the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Collections.Generic

Partial Public Class CalRelateMain2
    Public Property ROWID As Integer
    Public Property SubProjectID As Nullable(Of Integer)
    Public Property MatGroupID As Nullable(Of Integer)
    Public Property CreatedDate As Nullable(Of Date)
    Public Property CreatedBy As Nullable(Of Integer)
    Public Property LastUpdatedDate As Nullable(Of Date)
    Public Property LastUpdatedBy As Nullable(Of Integer)

    Public Overridable Property CalMaterialGroup As CalMaterialGroup
    Public Overridable Property CalSubProject As CalSubProject

End Class
