'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated from a template.
'
'     Manual changes to this file may cause unexpected behavior in your application.
'     Manual changes to this file will be overwritten if the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Collections.Generic

Partial Public Class CalProjectType
    Public Property ProjectID As Integer
    Public Property ProjectName As String
    Public Property ImageUrl As String
    Public Property IsActive As Nullable(Of Boolean)
    Public Property Remark As String
    Public Property CreatedDate As Nullable(Of Date)
    Public Property CreatedBy As Nullable(Of Integer)
    Public Property LastUpdatedDate As Nullable(Of Date)
    Public Property LastUpdatedBy As Nullable(Of Integer)

    Public Overridable Property CalSubProject As ICollection(Of CalSubProject) = New HashSet(Of CalSubProject)

End Class
