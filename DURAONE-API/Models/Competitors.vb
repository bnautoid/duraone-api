'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated from a template.
'
'     Manual changes to this file may cause unexpected behavior in your application.
'     Manual changes to this file will be overwritten if the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Collections.Generic

Partial Public Class Competitors
    Public Property ROWID As Long
    Public Property Province As String
    Public Property StoreID As Integer
    Public Property ProductNo As String
    Public Property ProductName As String
    Public Property UnitCode As String
    Public Property Barcode As String
    Public Property MatCategoryID As Nullable(Of Integer)
    Public Property BrandID As Nullable(Of Integer)
    Public Property Model As String
    Public Property NormalPrice As Nullable(Of Decimal)
    Public Property AdjustTolerance As Nullable(Of Decimal)
    Public Property FaultTolerance As Nullable(Of Decimal)
    Public Property IsActive As Nullable(Of Boolean)
    Public Property IsOurProduct As Nullable(Of Boolean)
    Public Property CompareToProduct As Nullable(Of Long)
    Public Property Remark As String
    Public Property CreateDate As Nullable(Of Date)
    Public Property CreateBy As Nullable(Of Integer)
    Public Property LastUpdateDate As Nullable(Of Date)
    Public Property LastUpdateBy As Nullable(Of Integer)

    Public Overridable Property Brands As Brands
    Public Overridable Property Categories As Categories
    Public Overridable Property Stores As Stores
    Public Overridable Property ProductRelated As ICollection(Of ProductRelated) = New HashSet(Of ProductRelated)

End Class
