'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated from a template.
'
'     Manual changes to this file may cause unexpected behavior in your application.
'     Manual changes to this file will be overwritten if the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Collections.Generic

Partial Public Class ContentFiles
    Public Property ROWID As Decimal
    Public Property Ref_ROWID As Nullable(Of Decimal)
    Public Property FileName As String
    Public Property FileType As String
    Public Property FileSize As Nullable(Of Decimal)
    Public Property Remark As String
    Public Property IsActive As Nullable(Of Boolean)
    Public Property CreateDate As Nullable(Of Date)
    Public Property CreateBy As String
    Public Property LastUpdateDate As Nullable(Of Date)
    Public Property LastUpdateBy As String

    Public Overridable Property ContentDownload As ContentDownload

End Class
